<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <!-- Basic Page Needs
  ================================================== -->
    <title>@yield('title')</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Cooking HTML5 Template - v1.0">
    <meta name="author" content="">
    <!-- Mobile Specific Meta
    ================================================== -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" href="{{ asset('front-end/assets/images/icon.png') }}" type="image/x-icon" />
    <!-- All Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/bootstrap.min.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/font-awesome.min.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/icofont.css')}}" media="screen">
    <!--Owl Carousel-->
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/owl.carousel.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/owl.theme.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/owl.transitions.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/jquery.fancybox.min.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/datepicker.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/odometer-theme-minimal.css')}}" media="screen">

    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/style.css')}}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{asset('front-end/assets/css/custom.css')}}" media="screen">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Mina" rel="stylesheet"> 
    @yield('custom-css')

</head>

<body class="body-innerwrapper">
<!--Pre Loader-->
<div id="pre_loader"></div>


<section id="top-bar" class="v2 light animate-in fade-in animated">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 topbar-block">
                <ul class="loc_ph">
                    <li><a href="tel:01846426884"><i class="fa fa-phone" aria-hidden="true"></i> 01846426884</a> </li>
                </ul>

                <div class="btn-group">
                    <button type="button" id="forms" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-user" aria-hidden="true"></i> Login
                    </button>

                </div>

            </div>
            <div class="col-sm-6 text-right topbar-block">
                <ul class="socials">
                    <li><a href="https://www.facebook.com/ doctor.co/"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                    <li><a href="javaScript:void(0)"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                    <li><a href="javaScript:void(0)"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                </ul>

            </div>
        </div>
    </div>
</section>

<header class="v2 light animate-in fade-in animated">
    <nav class="navbar affix-top" data-spy="affix" data-offset-top="60" id="slide-nav">
        <div class="container">
            <div class="navbar-header col-sm-3">
                <button type="button" class="navbar-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
              <a href="{{route('homeindex')}}" class="logo"><img src="{{asset('front-end/assets/images/logo-footer.png')}}" alt="Columba"></a>
            </div>
            <!--Nav links-->

            <div class=" navbar-collapse col-sm-12 col-md-9 no-padding" id="menu_nav">
                <a href="javaScript:void(0)" class="closs"><i class="icofont icofont-close-line"></i></a>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="{{route('homeindex')}}">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="javaScript:void(0)">Doctor</a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('specialitysearch',['sp'=>'Cardiology'])}}">Cardiology</a></li>
                            <li><a href="{{route('specialitysearch',['sp'=>'Dental'])}}">Dental</a></li>
                            <li><a href="{{route('specialitysearch',['sp'=>'Dermatology'])}}">Dermatology</a></li>
                            <li><a href="{{route('specialitysearch',['sp'=>'Gastroenterology'])}}">Gastroenterology</a></li>
                            <li><a href="{{route('specialitysearch',['sp'=>'Medicine'])}}">Medicine</a></li>
                            <li><a href="{{route('specialitysearch',['sp'=>'Orthopaedics'])}}">Orthopaedics</a></li>
                            <li><a href="{{route('specialitysearch',['sp'=>'Psychology'])}}">Psychology</a></li>
                        </ul>
                    </li>

                     <li class="dropdown">
                        <a href="{{ url('hospitals') }}">Hospital</a>

                    </li> 
                  <li><a href="{{ url('diagnostics') }}">Diagnostics </a></li>




                    <li><a href="{{route('contact')}}">Contact</a></li> <li><a href="{{route('contact')}}"> </a></li> <li><a href="{{route('contact')}}"> </a></li> <li><a href="{{route('contact')}}"> </a></li> <li><a href="{{route('contact')}}"></a></li>  <li><a href="{{route('contact')}}"></a></li><li><a href="{{route('contact')}}"></a></li>
                </ul>
            </div>

        </div>
    </nav>
</header>

<h3 style="color:red;text-align: center">
    <?php
    $exception = Session::get('exception');
    if($exception){
        echo $exception;
        Session::put('exception',null);
    }
    ?>
</h3>
<h3 style="color:green;text-align: center">
    <?php
    $message = Session::get('message');
    if($message){
        echo $message;
        Session::put('message',null);
    }
    ?>
</h3>


@section('content')
    @yield('show')

@show

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">X</button>
                <h4 class="modal-title" id="myModalLabel">Please Sign in </h4>
            </div>
            <div class="modal-body">
                        <div class="tabs">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#doctor" aria-expanded="false">Doctor</a></li>
                                <li class=""><a data-toggle="tab" href="#user" aria-expanded="false">Patient</a></li>
                                <li class=""><a data-toggle="tab" href="#diagnostic" aria-expanded="false">Diagnostic</a></li>
                                <li class=""><a data-toggle="tab" href="#hospital" aria-expanded="false">Hospital</a></li>
                            </ul>
                    <div class="tab-content">
                        <div id="doctor" class="tab-pane in fade active" style="width: 100%">
                            {!! Form::open(['url' => 'doctorjoinlogin','enctype'=>'multipart/form-data','method'=>'post']) !!}

                                <div class="form-group">
                                    <label>Username or email address</label>
                                    <input type="text" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-small">Login</button>
                                    <a href="#" class="simple"></a>
                                </div>
                            {!! Form::close() !!}
                            <h3> <a href="{{route('addnewdoctor')}}">Join as a doctor</a></h3>

                        </div>








                        <div id="user" class="tab-pane fade" style="width: 100%">
                            {!! Form::open(['url' => 'patientjoinlogins','enctype'=>'multipart/form-data','method'=>'post']) !!}
                                <div class="form-group">
                                    <label>Username or email address</label>
                                    <input type="text" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-small">Login</button>
                                    <a href="javaScript:void(0)" class="simple"></a>
                                </div>
                                {!! Form::close() !!}
                            <h3><a href="{{route('addnewuser')}}">Join as a patientss</a></h3>

                        </div>
                        <div id="diagnostic" class="tab-pane fade" style="width: 100%">
                            {!! Form::open(['url' => 'diagnosticjoinlogins','enctype'=>'multipart/form-data','method'=>'post']) !!}
                                <div class="form-group">
                                    <label>Username or email address</label>
                                    <input type="text" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-small">Login</button>
                                    <a href="javaScript:void(0)" class="simple"></a>
                                </div>
                                {!! Form::close() !!}
                            <h3><a href="{{route('addnewdiagnostic')}}">Join as a Diagnostic center</a></h3>

                        </div>





                        <div id="hospital" class="tab-pane fade" style="width: 100%">
                            {!! Form::open(['url' => 'hospitaljoinlogins','enctype'=>'multipart/form-data','method'=>'post']) !!}
                                <div class="form-group">
                                    <label>Username or email address</label>
                                    <input type="text" class="form-control" name="email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-small">Login</button>
                                    <a href="javaScript:void(0)" class="simple"></a>
                                </div>
                            {!! Form::close() !!}
                         <h3> <a href="{{route('addnewhospital')}}">Join as a Hospital</a></h3>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer-->
<footer class="v1">
    <section id="footer-upper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 footer-base no-padding">
                    <div class="col-sm-6 footer-block">
                        <a href="{{url('/')}}"><img src="{{asset('front-end/assets/images/logo-footer.png')}}" alt="Logo"></a>
                        <p>DoctorInformation is a healthcare discovery platform for doctor’s appointment (including video conference), diagnostic home service, blood donor service, doctor’s blog, updated news for health industry and a global facilitator for medical tourism.  doctor.com is a trusted advisor for healthcare and operates on a principle of absolute transparency with all stakeholders</p>
                        <ul class="address">
                            <li>Phone : <a href="tel:01846426884">01846426884</a> </li>
                            <li>Email : <a href="mailto:info@ doctor.com">info@gmail.com</a> </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 footer-block">
                        <div class="row">
                            <div class="col-sm-12">
                                <h5 class="pull-right">Connect with Socal Media</h5>
                            </div>
                            <div  class="col-sm-12">
                                <ul class="social-network pull-right social-circle">
                                    <li><a href="javaScript:void(0)" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="javaScript:void(0)" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="javaScript:void(0)" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="javaScript:void(0)" class="icoYoutube" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                                    <li><a href="javaScript:void(0)" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                            <div  class="col-sm-12">
                                <div class="company pull-right">
                                    <h3>Developed by</h3>

                                     <h3>Md Kamrul Hassan</h3>
                                     <h3>Md Rakib Hassan</h3>
                                     <h3>Md Rabiul Auwal Masum</h3>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="copy_rights">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center right-block">
                    <p>© Copyright Doctorinformation 2017 .All Rights Reserved. </p>
                </div>
            </div>
        </div>
    </section>
</footer>
<!-- Scripts -->
<script type="text/javascript" src="{{asset('front-end/assets/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0YyDTa0qqOjIerob2VTIwo_XVMhrruxo"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/moment.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="https://use.fontawesome.com/e18447245b.js"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/owl.carousel.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/appear.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/jquery.fancybox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/odometer.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/steller.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/jquery.bootstrap-touchspin.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/jquery.elevateZoom-3.0.8.min.js')}}"></script>
<script type="text/javascript" src="{{asset('front-end/assets/js/custom.js')}}"></script>



@yield('script')

</body>
</html>
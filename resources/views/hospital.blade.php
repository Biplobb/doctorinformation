@extends('layouts.master')
@section('title', 'Deshidoctor | Home')
@section('content')
    <section id="breadcrumb" class="light-overlay" data-stellar-background-ratio="0.4" style="background-position: 50% 0%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in move-up animated">
                    <h2>All Indian Hospitals</h2>
                    <p>Affordable Treatments, Honest &amp; Experienced Dentists.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="single-service" class="space">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 service-block">
                    <img src="assets/images/single-service.jpg" alt="Services">
                    <h2>X- Ray</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum odio urna, vitae ultrices mi malesuada ut. Praesent lacus erat, ultricies ut risus nec, pellentesque interdum purus. In mi justo, consectetur tincidunt sapien eget, venenatis volutpat risus. Maecenas eget pretium eros. Integer tincidunt aliquet ligula in vulputate. Ut ut justo facilisis, vulputate augue vel, vestibulum tortor. Nullam varius lacus non porttitor sodales. Vivamus ultricies est vitae pharetra convallis. Sed suscipit, nisi sit amet tempus mollis, mauris ante tempor risus, eu imperdiet ante felis vitae risus. Cras massa mi, condimentum at dignissim id, auctor ut arcu. Phasellus vitae pellentesque quam, sed blandit eros. Quisque malesuada arcu vel diam cursus posuere. Pellentesque faucibus risus libero.</p>
                    <h3>Important Things to Know</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum odio urna, vitae ultrices mi malesuada ut. Praesent lacus erat, ultricies ut risus nec, pellentesque interdum purus. In mi justo, consectetur tincidunt sapien eget, venenatis volutpat risus. Maecenas eget pretium eros. Integer tincidunt aliquet ligula in vulputate. </p>
                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                        <li>Nam ultrices elit augue, at euismod ipsum bibendum at.</li>
                        <li>Nulla semper consectetur ex,</li>
                        <li>Integer nisl nisi, scelerisque in metus nec</li>
                        <li>Aenean suscipit ullamcorper aliquam.</li>
                        <li>Aenean suscipit ullamcorper aliquam.</li>
                    </ul>
                    <h3>Get the Care You Deserve</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rutrum odio urna, vitae ultrices mi malesuada ut. Praesent lacus erat, ultricies ut risus nec, pellentesque interdum purus. In mi justo, consectetur tincidunt sapien eget, venenatis volutpat risus. Maecenas eget pretium eros. Integer tincidunt aliquet ligula in vulputate. </p>
                    <div class="panel-group accordion" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse1" aria-expanded="false">
                                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Why choose us ?</a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                            </div>
                        </div>
                        <div class="panel panel-default active">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" class="">
                                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Our Mission</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse in" aria-expanded="true" style="">
                                <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse3" aria-expanded="false">
                                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Our Vision</a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse4" aria-expanded="false">
                                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Our Value</a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 no-padding book-appointment">
                        <a href="#" class="btn"><span>BOOK APPOINTMENT</span>Experienced Dentists You Can Trust</a>
                    </div>
                </div>
                <aside class="col-sm-3">
                    <div class="widget services">
                        <h5>Our Services</h5>
                        <ul>
                            <li><a href="#">X -Ray</a> </li>
                            <li><a href="#">Traumatology</a> </li>
                            <li><a href="#">Pregnancy</a> </li>
                            <li><a href="#">Births</a> </li>
                            <li><a href="#">Dental</a> </li>
                        </ul>
                    </div>
                    <div class="widget contact">
                        <h5>How can we help you?</h5>
                        <p>Contact our WordPress gurus via our support channel</p>
                        <a href="#" class="btn">Contact Us</a>
                    </div>
                    <div class="widget testimonials">
                        <div class="inner">
                            <div class="icon">“</div>
                            <p>Before selecting a Financial Adviser we had discussed our finances with a few other advisers, but from our first meeting with Financial solutions it was apparent that their depth of knowledge far exceeded that of others we had spoken with.</p>
                        </div>
                        <div class="name"><span>Jeff P</span> - Ceo envato, May 2013</div>
                    </div>
                </aside>
            </div>
        </div>
    </section>
@endsection
{{--@include('admin.layouts.header')--}}
@extends('diagnostic-admin.layouts.master')
@section('content')
    <section class="content">

        <h1>Tests Table</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Tests_Id</th>
                <th scope="col">Diagnostic_center_Id</th>
                <th scope="col">price</th>
                <th scope="col">Discount percentage</th>
                <th scope="col">Discount price</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($diagnostic_center_tests as $diagnostic_center)
                <tr>
                    <th scope="row">{{ $diagnostic_center->id }}</th>
                    <td>{{ $diagnostic_center->Tests_id }}</td>
                    <td>{{ $diagnostic_center->Diagnostic_Center_id }}</td>
                    <td>{{ $diagnostic_center->previous }}</td>
                    <td>{{ $diagnostic_center->discount }}</td>
                    <td>{{ $diagnostic_center->discountprice }}</td>

                    <td>

                        <a href="{{ url('admin/diagnostic/diagnostic-center-test/edit/'.$diagnostic_center->id) }}">Edit</a>
                        <a href="{{ url('/admin/diagnostic/diagnostic-center-test/destroy/'.$diagnostic_center->id) }}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

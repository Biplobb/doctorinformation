{{--@include('admin.layouts.header')--}}
@extends('diagnostic-admin.layouts.master')
@section('content')
    <section class="content">

        <h1>Tests Table</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>

                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tests as $tests)
                <tr>
                    <th scope="row">{{ $tests->id }}</th>
                    <td>{{ $tests->name }}</td>


                    <td>

                        <a href="{{ url('admin/diagnostic/tests/edit/'.$tests->id) }}">Edit</a>
                        <a href="{{ url('/admin/diagnostic/tests/destroy/'.$tests->id) }}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


@extends('diagnostic-admin.layouts.master')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Add Test and price </h2>
            <small class="text-muted">Test information submit form</small>
        </div>
        <div class="row clearfix">
            {!! Form::open(['url' => 'diagnostic/tests/testsstore/'.$re->id,'enctype'=>'multipart/form-data','method'=>'post']) !!}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Test Information</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Test Name" name="name">
                                    </div>
                                </div>
                            </div>


                        </div>

                        {{--<div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Price" name="price">
                                    </div>
                                </div>
                            </div>
                           <div>--}}

                            {{--<div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Discount price" name="discountprice">
                                        </div>
                                    </div>
                                </div>


                            </div>--}}



                            <div class="col-sm-6 col-xs-12">

                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


    </div>

</section>
@endsection

{{--@include('admin.layouts.footer')--}}


@extends('layouts.master')
@section('title', 'Deshidoctor | Contact')
@section('content')
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4" style="background-position: 50% 0%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Contact Us</h2>
                    <p>A lot Of Style To Customize</p>
                </div>
            </div>
        </div>
    </section>


    <section id="contact-us" class="space v1 action-8">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <form id="contact-form">
                        <div class="row">
                           <div class="form-group col-sm-6">
                                <input type="text" class="form-control" name="name" id="name" required="" placeholder="Name">
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="text" class="form-control" name="mail" id="mail" required="" placeholder="Mobile Number">
                            </div> 
                        </div>
                         <div class="row">
                            <div class="form-group col-sm-6">
                                <input type="email" class="form-control" name="website" id="website" placeholder="Email Address">
                            </div>
                           <div class="form-group col-sm-6">
                                <input type="text" class="form-control" name="website" id="website" placeholder="Website">
                            </div>
                        </div>
                        <div class="row">
                             <div class="form-group col-sm-12 ">
                                <textarea class="form-control" name="comment" id="comment" placeholder="Message" autocomplete="off"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 form-group button">
                                <input type="submit" id="submit_contact" class="btn" value="Send">
                            </div>
                        </div>
                        
                        
                        <div id="msg" class="message"></div>
                    </form>
                </div>
                <div class="col-sm-5">
                    <div class="action-block col-sm-12">
                    <div class="inner">
                        <div class="action-item">
                            <div class="icon">
                                <img src="{{ asset('front-end/assets/images/topbar-icon-1.png') }}" alt="Columba">
                            </div>
                            <script>
                                document.getElementById('dt').max= new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
                            </script>
                            Date: <input type="date" min='1899-01-01' id="dt" />
                            <div class="action-text">
                                <h5>Monday - Friday : 08:00-19:00</h5>
                                <p>Saturday and Sunday - Closed</p>
                            </div>
                        </div>
                        <div class="action-item">
                            <div class="icon">
                                <img src="{{ asset('front-end/assets/images/topbar-icon-2.png') }}" alt="Columba">
                            </div>
                            <div class="action-text">
                                <h5>1-800-700-6200</h5>
                                <p>themesfoundry@gmail.com</p>
                            </div>
                        </div>
                        <div class="action-item">
                            <div class="icon">
                                <img src="{{ asset('front-end/assets/images/topbar-icon-3.png') }}" alt="Columba">
                            </div>
                            <div class="action-text">
                                <h5>70/3 East Tejgaon Farmgate</h5>
                                <p>Dhaka-1215 Bangladesh</p>
                            </div>
                        </div>
                    </div>
                </div>
                    
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="map-area">
                    <div id="googleMap" style="width:100%;height:400px;"></div>
                </div>
    </section>
    

    @section('script')
    
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        function initialize() {
          var mapOptions = {
            zoom: 14,
            scrollwheel: false,
            center: new google.maps.LatLng(23.7629435,90.3843439)
          };

          var map = new google.maps.Map(document.getElementById('googleMap'),
              mapOptions);


          var marker = new google.maps.Marker({
            position: map.getCenter(),
            animation:google.maps.Animation.BOUNCE,
            icon: '{{ asset('front-end/assets/images/map-icon.png') }}',
            map: map
          });

        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    @endsection()

@endsection
@extends('layouts.master')
@section('title', 'Doctor information | Hospitals')
@section('content')
    <section id="breadcrumb" class="light-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-bott">

                        {{--  <div class="titless" style="overflow:hidden">
                              <div class="title" align="center"  >
                                  {!! Form::open(['route' => 'searchPDF','method'=>'post','class'=>'class_name']) !!}
                                  {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}

                                  <button class="clickable" id="clickable">.        search    .</button>

                                  --}}{{--{!! Form::submit('search') !!}--}}{{--
                                  {!! Form::close() !!}
                              </div>
                          </div>--}}
                        {!! Form::open(['route' => 'searchPDFmain','method'=>'post','id','=',"search"]) !!}
                        {{--<form id="search" method="post" onsubmit="return validdata()">--}}
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-2">
                                <div class="gutter">
                                    <select name="area" id="area" class="form-control" onchange="enable_lookingfor()">
                                        <option value="">Search Area</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Narayonganj">Narayganj</option>
                                        <option value="Savar">Savar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="gutter">
                                    <select name="lookingfor" id="lookingfor" class="form-control" onchange="enable_keyword()">
                                        <option value="">Looking for</option>
                                        <option value="doctor">Doctor</option>
                                        <option value="hospital">Hospital</option>
                                        <option value="diagnostic">Diagnostics</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="gutter">
                                    <input id="keyword" class="form-control" name="keyword" type="text" placeholder="Type Keyword"  autocomplete="off">
                                    <input type="hidden" name="idnt" id="idnt">
                                    <div class="suggestion">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="gutter">
                                    <div class="search-icon">
                                        <button id="dosearch"> <span class="fa fa-search" ></span></button>
                                        {{--<button id="dosearch"> <span class="fa fa-search" ></span></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <h2>Your Health We Care</h2>
                    </div>
                </div>
            </div>
        </section>
    <section id="blog" class="space v1">
        <div class="container">
            <div class="row">
              
                <div class="col-sm-12 no-padding blog-base">

                    @foreach($Hospital as $hospital_service)

                    <div class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img  src="{{asset('image/hospital-photo/'.$hospital_service->image)}}" alt="Columba"  />
                            <a class="hover" href="{{ url('hospitalshow/'.$hospital_service->id)  }}">
                                <div class="inner">
                                    <h4>{{ $hospital_service->name }}</h4>
                                    <div class="date">{{ $hospital_service->location }}</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <article class="col-sm-12 text-center load-more animate-in move-up">
                    <a href="{{ url('allindex') }}" class="btn">Load more</a>
                </article>
            </div>
        </div>
    </section>
@endsection

@section('script')


    <script>
        var token='{{\Illuminate\Support\Facades\Session::token()}}';
    </script>
    <script src="{{asset('front-end/assets/js/myjs.js')}}"></script>

@endsection
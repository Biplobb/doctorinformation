@extends('layouts.master')
@section('title', 'Deshidoctor | News')
@section('content')
    <section id="blog" class="space v3">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9 no-padding">
                    <div class="col-sm-12 no-padding blog-base">

                        @foreach($news as $news)
                            <article class="col-sm-12 blog-block animate-in move-up">
                                <div class="inner">
                                    <img src="{{asset('image/news-photo/'.$news->image)}}" width="400px",height="400px">
                                    <h3><a href="#">{{ $news->news_title }}</a> </h3>
                                    <ul class="meta">
                                        <li><a href="#">{{ $news->author }}</a> </li>
                                        <li><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $news->created_at }}</a> </li>

                                    </ul>




                                    <p style="display: inline; text-align: justify">

                                        @php
                                            $limit=450;
                                                if (strlen($news->news_post) > $limit){
                                               echo $stringCut = substr($news->news_post, 0, $limit);
                                               // echo substr($stringCut, 0, strrpos($stringCut, ''));
                                                }
                                                else
                                                {
                                                echo $news->news_post;
                                                }
                                        @endphp
                                    </p>
                                    <a href="{{url('newsdetails/'.$news->id)}}"> [Continue Reading ...]</a>

                                </div>



                            </article>

                        @endforeach


                    </div>


                    <div class="col-sm-12 co-pagination animate-in move-up">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <aside class="col-sm-4 col-md-3">
                    <div class="widget category animate-in move-up animated">
                        <h5>Categories</h5>
                        <ul>
                            <li><a href="#">Photoshop<span>(5)</span></a> </li>
                            <li><a href="#">Photography<span>(5)</span></a> </li>
                            <li><a href="#">Design<span>(5)</span></a> </li>
                            <li><a href="#">Development<span>(5)</span></a> </li>
                            <li><a href="#">Illustrator<span>(5)</span></a> </li>
                        </ul>
                    </div>
                    <div class="widget recent-post animate-in move-up animated">
                        <h5>Recent Posts</h5>
                        <ul>
                            <li><a href="#">Smarter Grids With Sass And ...</a> </li>
                            <li><a href="#">Quantity Ordering With CSS</a> </li>
                            <li><a href="#">Gallery Post</a> </li>
                            <li><a href="#">Video Post</a> </li>
                            <li><a href="#">Image Post</a> </li>
                        </ul>
                    </div>
                    <div class="widget tags animate-in move-up">
                        <h5>Tag</h5>
                        <ul>
                            <li><a href="#">Design</a> </li>
                            <li><a href="#">Branding</a> </li>
                            <li><a href="#">Concept</a> </li>
                            <li><a href="#">Website</a> </li>
                            <li><a href="#">App</a> </li>
                            <li><a href="#">Photography</a> </li>
                        </ul>
                    </div>
                    {{--<div class="widget instagram animate-in move-up">--}}
                        {{--<h5>Instagram</h5>--}}
                        {{--<ul>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-1.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-2.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-3.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-4.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-5.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-6.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-7.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-8.jpg')}}" alt="Columba"></a> </li>--}}
                            {{--<li><a href="#"><img src="{{asset('front-end/assets/images/insta-9.jpg')}}" alt="Columba"></a> </li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                    <div class="widget popular-post animate-in move-up">
                        <h5>popular posts</h5>
                        <div class="posts">
                            <div class="post-block">
                                <img src="{{asset('front-end/assets/images/popular-post-1.jpg')}}" alt="Columba">
                                <div class="post-text">
                                    <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                    <div class="date">December 9th, 2016</div>
                                </div>
                            </div>
                            <div class="post-block">
                                <img src="{{asset('front-end/assets/images/popular-post-2.jpg')}}" alt="Columba">
                                <div class="post-text">
                                    <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                    <div class="date">December 9th, 2016</div>
                                </div>
                            </div>
                            <div class="post-block">
                                <img src="{{asset('front-end/assets/images/popular-post-3.jpg')}}" alt="Columba">
                                <div class="post-text">
                                    <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                    <div class="date">December 9th, 2016</div>
                                </div>
                            </div>
                            <div class="post-block">
                                <img src="{{asset('front-end/assets/images/popular-post-4.jpg')}}" alt="Columba">
                                <div class="post-text">
                                    <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                    <div class="date">December 9th, 2016</div>
                                </div>
                            </div>
                            <div class="post-block">
                                <img src="{{asset('front-end/assets/images/popular-post-5.jpg')}}" alt="Columba">
                                <div class="post-text">
                                    <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                    <div class="date">December 9th, 2016</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget feature-post animate-in move-up">
                        <div class="owl-theme owl-carousel feature-post-slider" style="opacity: 1; display: block;">
                            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1578px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 263px;"><div class="item">
                                            <div class="feature-block">
                                                <img src="{{asset('front-end/assets/images/feature-post.jpg')}}" alt="Columba">
                                                <div class="hover">
                                                    <div class="inner">
                                                        <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                                        <div class="date">December 9th, 2016</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item" style="width: 263px;"><div class="item">
                                            <div class="feature-block">
                                                <img src="{{asset('front-end/assets/images/feature-post.jpg')}}" alt="Columba">
                                                <div class="hover">
                                                    <div class="inner">
                                                        <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                                        <div class="date">December 9th, 2016</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item" style="width: 263px;"><div class="item">
                                            <div class="feature-block">
                                                <img src="{{asset('front-end/assets/images/feature-post.jpg')}}" alt="Columba">
                                                <div class="hover">
                                                    <div class="inner">
                                                        <h6><a href="#">Melancholy Middletons Yet Understood Decisively</a> </h6>
                                                        <div class="date">December 9th, 2016</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-controls">
                                <div class="owl-pagination">
                                    <div class="owl-page active">
                                        <span class="">

                                        </span>
                                    </div>
                                    <div class="owl-page">
                                        <span class="">

                                        </span>
                                    </div>
                                    <div class="owl-page">
                                        <span class="">

                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<div class="widget twitter animate-in move-up">--}}
                        {{--<h5>Twitter recents</h5>--}}
                        {{--<div class="twitter-feed">--}}
                            {{--<div class="twitter-block col-sm-12 no-padding">--}}
                                {{--<div class="icon">--}}
                                    {{--<i class="fa fa-twitter" aria-hidden="true"></i>--}}
                                {{--</div>--}}
                                {{--<div class="feed-text">--}}
                                    {{--<p>Ut commodo felis tellus, eu placerat mi luctus ac. Nam egestas velit ut leo eat suscipit, libero libero </p>--}}
                                    {{--<a href="#">http://t.co/hVtABj5tZo</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="twitter-block col-sm-12 no-padding">--}}
                                {{--<div class="icon">--}}
                                    {{--<i class="fa fa-twitter" aria-hidden="true"></i>--}}
                                {{--</div>--}}
                                {{--<div class="feed-text">--}}
                                    {{--<p>Ut commodo felis tellus, eu placerat mi luctus ac. Nam egestas velit ut leo eat suscipit, libero libero </p>--}}
                                    {{--<a href="#">http://t.co/hVtABj5tZo</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="widget newsletter text-center animate-in move-up">
                        <h5>subscribe</h5>
                        <p>Subscribe to get the latest news</p>
                        <form action="#" method="post">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email address">
                            </div>
                            <button type="submit" class="btn">Sign up</button>
                        </form>
                    </div>
                </aside>
            </div>
        </div>
    </section>
@endsection
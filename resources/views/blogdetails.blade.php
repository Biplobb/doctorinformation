@extends('layouts.master')
@section('title', 'Deshidoctor | Blog')
@section('content')
    <section id="blog" class="space v3">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9 no-padding">
                    <div class="col-sm-12 no-padding blog-base">


                            <article class="col-sm-12 blog-block animate-in move-up">
                                <div class="inner">
                                    <img src="{{asset('image/blog-photo/'.$blogs->image)}}" width="400px", height="400px">
                                    <h3><a href="#">{{ $blogs->title }}</a> </h3>
                                    <ul class="meta">
                                        <li><a href="#">{{ $blogs->author }}</a> </li>
                                        <li><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blogs->created_at }}</a> </li>
                                        <li><a href="#"><i class="fa fa-tags" aria-hidden="true"></i> {{ $blogs->type }}</a> </li>
                                        {{--<li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> 3 comment’s</a> </li>git--}}
                                    </ul>
                                    <p>{!! $blogs->blog_post !!} </p>

                                    <br>
                                    <br>


                                </div>
                            </article>

                    </div>



                    @foreach($comment_view as $comment)



                        <br>
                        <br>

                        {{$comment->name}}
                        <br>
                        {{$comment->email}}
                        <br>
                        {{$comment->comments}}


                        <br>
                        <br>


                    @endforeach



                    <div class="row clearfix">

                        <br>
                        <br>

                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                <strong>{{ session('message') }}</strong>
                            </div>

                        @endif

                        <div class="col-sm-4 col-xs-12">

                            <br>
                            <br>

                            <form action="{{route('blogcomment')}}" method="post" enctype="multipart/form-data">

                                {{csrf_field()}}
                                <div class="form-group">
                                <div class="form-line">
                                    <input type="hidden" value="{{ $blogs->id }}" name="blog_id">

                                    <input type="text" class="form-control" placeholder=" Name" name="name" required> <br>

                                    <input type="text" class="form-control" placeholder="Email" name="email" > <br>

                                    <textarea class="form-control" placeholder="Type Your Comment" name="comments" required></textarea> <br>

                                    <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>

                                </div>
                            </div>
                            </form>
                        </div>


                                <div class="col-sm-12 co-pagination animate-in move-up">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
               
            </div>
                 <aside class="col-sm-4 col-md-3">

                    <div class="widget recent-post animate-in move-up animated">
                        <h5>Recent Posts</h5>
                        <ul>
                            @foreach($recent_posts as $post)
                                <li><a href="{{url('blogdetails/'.$post->id)}}">{{$post->title}}</a> </li>
                            @endforeach

                        </ul>
                    </div>


                </aside>
        </div>
    </section>
@endsection
@extends('layouts.master')
@section('title', 'Deshidoctor | Blood')
@section('content')
<section id="breadcrumb" class="light-overlay">
            <div class="container">
                <div class="row">
                     <div class="col-md-8 col-md-offset-2 custom-search">
                        <form id="search" method="post" onsubmit="return validdata()">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="gutter">
                                        <select name="area" id="area" class="form-control" onchange="enable_lookingfor()">
                                            <option value="">Search Area</option>
                                            <option value="Dhaka">Dhaka</option>
                                            <option value="Narayonganj">Narayganj</option>
                                            <option value="Savar">Savar</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="gutter">
                                        <select name="lookingfor" id="lookingfor" class="form-control" onchange="enable_keyword()">
                                            <option value="">I am looking</option>
                                            <option value="doctor">Doctor</option>
                                            <option value="blood">Blood</option>
                                            <option value="hospital">Hospital</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="gutter">
                                        <input id="keyword" class="form-control" name="keyword" type="text" placeholder="Type Keyword"  autocomplete="off">
                                        <input type="hidden" name="idnt" id="idnt">
                                        <div class="suggestion">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="gutter">
                                        <div class="search-icon">
                                            <button id="dosearch"> <span class="fa fa-search" ></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
<section id="blog" class="space v2">
<div class="container">
<div class="row">

<aside class="col-sm-4 col-md-3">
    <div class="widget category animate-in move-up animated">
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-tint" aria-hidden="true" style="color: red;"></i>
            Donate Your Blood</button>

        <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Blood Donor Registration</h4>
        </div>

    <div class="modal-footer">
        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                </div>
                <div class="row clearfix">
                    {!!   Form::open(['url'=>'/blood/store','method'=>'POST','class'=>'form-horizontal'])!!}
                    <div class="col-md-12">
                        <div class="card">

        <div class="body">
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        <input type="text" name="name" class="form-control" id="" required  placeholder="Full Name">
                    </div>
                </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="address" class="form-control" id="" required  placeholder=" Donor Address">
                        </div>
                    </div>
                </div>
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <div class="form-line">
                    <input type="text" name="phone" class="form-control" id="" required  placeholder="Phone Number">
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="form-group">
                <div class="form-line">
                    <input type="text" name="date"  class="form-control" id="" required  placeholder=" Last Donate Date">
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <select  class="form-control" name="blood_group" id="" required >
                    <option value="" checked> Donar Group</option>
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>
                    <option value="AB+">AB+</option>
                    <option value="AB-">AB-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <input type="email" name="email" id="" required class="form-control" placeholder="Your Email">
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <select class="form-control" name="area" id="" required>
                    <option value="" checked>Donar District</option>
                    @foreach ($district as $district)
                        {
                        <option value="{{ $district->name}}">{{ $district->name }}</option>
                        }
                    @endforeach
                </select>
            </div>
        </div>
    </div>
            <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                    <div class="form-line">
                        {{-- <select class="form-control" name="area" id="" required>--}}

                        <select class="form-control" name="upozila" id="" required>
                            <option value="" checked> Donar Upazila</option>
                            @foreach ($upozila as $upozila)
                                {
                                <option value="{{ $upozila->name }}">{{ $upozila->name }}</option>
                                }
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        <input type="hidden" name="status" value="0">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
            <button type="button"
                    class="btn btn-raised"
                    data-dismiss="modal">Close</button>
            </div>
    {!!  Form::close()!!}
   </div>
      </div>
        </div>
         </div>
          </div>
          </section>
            </div>
              </div>
                   </div>
                     </div>

                      <br><br>

        <h5>Blood Group</h5>

        <ul class="pull-left">
            @foreach($counters as $c)
            <li><a href="{{url('/blood/'.$c->blood_group)}}">{{$c->blood_group}} <span> ( {{$c->total}} ) </span></a> </li>
            @endforeach
        </ul>
    </div>

    </aside>

<div class="col-md-9">
    <div id="cart" class="space-bottom">
        <div class="cart-product no-padding">
        <table class="table">
        <p class="text-align:center text-success">{{Session::get('message')}}</p>
        <br>
        <br>
        <div class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>Address</th>

            <th>Blood Group</th>

            <th>Email</th>

            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @if(sizeof($donors)==0)
            <h2>No data found</h2>
            @else
            @foreach($donors as $donor)

                <tr>
                    <td>{{ $donor->name }}</td>
                    <td>{{ $donor->address }}</td>
                    <td>{{ $donor->blood_group }}</td>

                    <td>{{ $donor->email }}</td>
                    <td>
                        <a href="javaScript:void(0)"
                                class="btn green btn2x radius-2x btn-small"
                           id="dnridno"
                                data-toggle="modal"
                                data-target="#favoritesModal"
                                data-id = "{{ $donor->id }}"
                                >
                            Request
                        </a>
                    </td>
                </tr>

               </tbody>

            @endforeach
            @endif
    <div class="modal fade" id="favoritesModal"
          tabindex="-1" role="dialog"
          aria-labelledby="favoritesModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="favoritesModalLabel">Submit Your Request</h4>
                </div>
                <div class="modal-body">

   <div class="container-fluid">
     <div class="block-header">
        <p class="text-align:center text-success">{{Session::get('message')}}</p>
      </div>
        <div class="row clearfix">
        {!!   Form::open(['url'=>'blood/seakerstore','method'=>'POST','class'=>'form-horizontal'])!!}
         <div class="col-md-12">
        <div class="card">
            <div class="body">
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="hidden" id="donor_id" name="donor_id" class="form-control">
                            <input type="text" name="name" class="form-control" id="" required  placeholder="Full Name">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" name="hname" class="form-control" id="" required  placeholder="Hospital Name" id="" required>
                        </div>
                    </div>
                </div>
    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <input type="text" name="haddress" class="form-control" id="" required  placeholder="Hospital Address">
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <input type="text" name="relationship" class="form-control" id="" required  placeholder="Relationship">
            </div>
        </div>
    </div>

    <div class="col-sm-6 col-xs-12">
        <div class="form-group">
            <div class="form-line">
                <input type="text" name="phone" maxlength="11" class="form-control" id="" required  placeholder="Phone Number">
            </div>
        </div>
    </div>
                <div class="col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="email" name="email" id="" required   class="form-control" placeholder="Your Email Address">
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    </div>
    </div>
   <div class="modal-footer">
     <button type="button"
         class="btn green btn2x radius-2x btn-small"
           data-dismiss="modal">Close</button>
         <button type="submit" class="btn green btn2x radius-2x btn-small pull-right">
        Submit
        </button>
        {!!  Form::close()!!}
            </div>
        </div>
     </div>
      </div>
     </div>
        </div>
        </table>
    <ul class="pagination">
        <div class="col-sm-12 co-pagination animate-in move-up animated">
        {{$donors->links()}}
        </div>
    </ul>
      </div>
   </div>
     </div>
        </div>
       </div>
    </section>

    @endsection

    @section('script')

        <script>
            $(document).on('click','#dnridno',function () {
                $('#donor_id').val($(this).data('id'));
            });


            var token='{{\Illuminate\Support\Facades\Session::token()}}';
        </script>

        <script src="{{asset('front-end/assets/js/myjs.js')}}"></script>

    @endsection


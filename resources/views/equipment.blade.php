@extends('layouts.master')
@section('title', 'Deshidoctor | Equipment')
@section('content')
    <section id="shop" class="space">
        <div class="container">
            <div class="row">
                <aside class="col-sm-3">
                    <div class="widget category animate-in move-up animated">
                        <h5>Categories</h5>
                        <ul>
                            <li><a href="#">Patient Care<span>(5)</span></a> </li>
                            <li><a href="#">Wound Care <span>(5)</span></a> </li>
                            <li><a href="#">Bandage/Plaster <span>(5)</span></a> </li>
                            <li><a href="#">Sterile Supplies<span>(5)</span></a> </li>
                            <li><a href="#">Medical Applicators <span>(5)</span></a> </li>
                        </ul>
                    </div>
                    <div class="widget category animate-in move-up">
                        <h5>Brands</h5>
                        <ul>
                            <li><a href="#">Medline<span>(5)</span></a> </li>
                            <li><a href="#">Drive Medical <span>(5)</span></a> </li>
                            <li><a href="#">Medquip <span>(5)</span></a> </li>
                            <li><a href="#">First Aid Only<span>(5)</span></a> </li>
                            <li><a href="#">Med Pride <span>(5)</span></a> </li>
                        </ul>
                    </div>
                    <div class="widget category animate-in move-up">
                        <h5>price</h5>
                        <ul>
                            <li><a href="#">Under $30 <span>(5)</span></a> </li>
                            <li><a href="#">$30 - $50<span>(5)</span></a> </li>
                            <li><a href="#">$50 - $100<span>(5)</span></a> </li>
                            <li><a href="#">$100 - $500 <span>(5)</span></a> </li>
                            <li><a href="#">$500 - $1000<span>(5)</span></a> </li>
                        </ul>
                    </div>
                </aside>
                <div class="col-sm-9 shop-base no-padding">
                    <header class="com-sm-12 shop-header no-padding animate-in move-up animated">
                        <div class="inner">
                            <div class="col-sm-6 col-sm-offset-6 col-xs-12 text-right header-block">
                                <ul>
                                    <li><a href="#"><i class="fa fa-bars" aria-hidden="true"></i></a> </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                            <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                            <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>items 1 - 15 of 28 total</li>
                                </ul>
                            </div>
                        </div>
                    </header>
                    <div class="col-sm-12 no-padding products">
                        <div class="col-sm-12 no-padding product-base">
                            <div class="col-sm-4 product-block animate-in move-up animated">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment1.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up animated">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment2.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up animated">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment3.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment4.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment5.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment6.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment7.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment8.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                            <div class="col-sm-4 product-block animate-in move-up">
                                <div class="product-image center">
                                    <img src="{{asset('front-end/assets/images/equipment1.jpg')}}" alt="Columba Products">
                                    <a href="#" class="btn">Add to Cart</a>
                                </div>
                                <div class="product-description">
                                    <h5><a href="#">Advance Vinyl Clear Multi-Purpose Powder-free Gloves (Case of 1,000)</a> </h5>
                                    <div class="price">$340.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 co-pagination animate-in move-up">
                            <ul class="pagination">
                                <li><a href="#">Prev</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"> Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
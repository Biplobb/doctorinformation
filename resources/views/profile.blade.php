@extends('layouts.master')
@section('custom-css')
    <style>
        .TimeSchedule{
            display: none;
        }
    </style>
@endsection
@section('title', 'Doctor Information | Profile')
@section('content')
    <section id="breadcrumb" class="light">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center ">
                    <div class="inner">
                        <img src="{{asset('image/doctor-photo/'.$doctor->image)}}" alt="Columba" class="img-circle" width="120px" height="120px">
                        <div class="name"><h2>{{$doctor->name}}</h2></div>
                        <div class="experience">
                            <h4>{{$doctor->experience}} years experience</h4>
                            <h4>{{$doctor->degree}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="choose" class="space v2 bg-color">
        <div class="container">
            <div class="row">

                @if(session('wornginfor'))
                    <span style="color: #f44242;">{{session('wornginfor')}}</span>
                @elseif(session('alreadydone'))
                    <span style="color: #f44242;">{{session('alreadydone')}}</span>
                @endif

                    <div class="col-sm-6 col-md-offset-3 error-block text-center">
                        @if(session('oldperson'))
                            <h2>{{session('oldperson')}}</h2>
                            <hr>
                        @elseif(session('newperson'))
                            <h2>{{session('appintment')}}</h2>
                            <h2>{{session('newperson')}}</h2>
                            <h3>{{session('message')}}</h3>
                            <hr>
                        @endif
                    </div>

                <div class="col-sm-6">
                        <div class="panel panel-default" >
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a  class="collapsed active" data-parent="#accordion"  aria-expanded="true">

                                        <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> Specialist in</a>
                                </h4>
                            </div>

                            <div>
                                <div class="panel-body">
                                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi nemo reprehenderit vero voluptatum.
                                    Accusantium aliquid cupiditate debitis odit voluptas. Ad blanditiis deleniti eligendi in nulla odit officia tempore, totam vel!
                                </div>
                            </div>

                        </div>


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#">
                                    <i class="fa fa-arrow-circle-o-down" aria-hidden="true"></i> Chambers</a>
                            </h4>
                        </div>
                        <div>

                            <div class="panel-body">
                                @foreach($data as $d)
                                    <p>{{$d->hospitalname}}</p>
                                    Location : Dhanmondi

                                    @if($d->first_fees!='')
                                        <p>First visit : {{$d->first_fees}} Bdt.</p>
                                    @endif
                                    @if($d->second_fees!='')
                                        <p>Second visit : {{$d->second_fees}} Bdt.</p>
                                    @endif
                                    
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>



                <div class="col-sm-6 about-block no-padding" id="schedulearea">

                    <div class="panel panel-default" >
                        <div class="panel-heading">
                            <h3>Book Appointment</h3>
                        </div>

                        <div class="panel-body">

                            <div class="row">

                                <span class="label label-danger" id="warning"></span>
                                <div class="col-sm-6">

                                    <label for="">Select Chamber/Hospital <span style="color: red">*</span></label>
                                    <select class="form-control " name="chamber" id="chamber">
                                        <option value="">Select hospital</option>
                                        @foreach($data as $d)
                                            <option value=""   data-id1="{{$d->hospitalid}}" data-id2="{{$d->doctorid}}">{{$d->hospitalname}}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <form action="{{route('bookappointment')}}" method="post" name="appmntime" onsubmit="return validateAppointmentForm()" >
                                    {{csrf_field()}}

                                    <div class="col-sm-6">
                                        <div id="datetime"  class="form-group">
                                            <label for="">Select date :<span style='color: red'>  *</span></label>
                                            <input type="text" id="disabledtime" class="form-control" disabled="disabled" placeholder="Check available date">
                                        </div>
                                    </div>


                                    <div class="col-sm-6">
                                        <div id="timelist" class="TimeSchedule"></div>
                                        <div id="timeset" class="form-group">
                                            <label for="">Select time : <span style='color: red'>  *</span></label>
                                            <select name="" id="" class="form-control" disabled>
                                                <option value="">Select time</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="col-sm-6">

                                        <div id="apptype" class="apptype"> </div>

                                        <div id="apmnttype" class="form-group">
                                            <label for="">Appointment type :<span style='color: red'>  *</span></label>
                                            <input type="text"  class="form-control" disabled="disabled" placeholder="Select appointment type">
                                        </div>
                                    </div>


                                    <div class="col-sm-12">
                                        <div id="appointmentform"></div>
                                    </div>

                                </form>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            </div>
        </div>

    </section>
@endsection

@section('script')


    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function () {
            $('#TimeSchedule').hide();
        });

        var dayNameId=[];
        var doctor_id;
        var hospital_id;
        var dayOfWeek='';
        var time;
        var weekday=new Array();
        weekday['Mon']="Monday";
        weekday['Tue']="Tuesday";
        weekday['Wed']="Wednesday";
        weekday['Thu']="Thursday";
        weekday['Fri']="Friday";
        weekday['Sat']="Saturday";
        weekday['Sun']="Sunday";


        $('#chamber').on('change', function (e) {
            $("#appointmentform").empty();
            $("#timelist").empty();
            $("#timeset").show();

            dayNameId = [];

            hospital_id = $(this).find(':selected').data('id1');
            doctor_id = $(this).find(':selected').data('id2');

            $.ajax({
            method: "post",
            url: '{{route("getscheduledata")}}',
            data: {
            hospital_id: hospital_id,
            doctor_id: doctor_id
            },

            success: function (data) {
                for (i = 0; i < data[0].length; i++) {
                    dayNameId[i] = data[0][i]["value"];
                }

                $("#datetime").html("<label>Select date : <span style='color: red'>*<span> </label><input type='text'  id='txtDate' name='apptDate' class='form-control' style='border-color: green' placeholder='Check available date'>");
                $("#timelist").append(data[1]);
                $("#timelist").hide();
                $(function () {
                    $("#txtDate").datepicker({
                        beforeShowDay: function (dt) {
                            dmy = dt.getDay();
                            if ($.inArray(dmy, dayNameId) == -1) {
                                return [false, "Unavailable"];
                            } else {
                                return [true, "Available"];
                            }
                        },
                        minDate: 0,
                        maxDate: "+20d",
                        showAnim: "clip",
                        dateFormat: "yy-mm-dd",

                        onSelect: function (inst) {
                            var seldate = $(this).datepicker("getDate");
                            seldate = seldate.toDateString();
                            seldate = seldate.split(" ");
                            dayOfWeek = weekday[seldate[0]];
                            $("#disabledtime").remove();

                            $("#timeset").hide();

                            $("#timelist").show();
                            $("#appointmentform").empty();

                            $("#Monday").hide();
                            $("#Wednesday").hide();
                            $("#Sunday").hide();
                            $("#Saturday").hide();
                            $("#Tuesday").hide();
                            $("#Thursday").hide();
                            $("#Friday").hide();
                            $("#" + dayOfWeek).show();
                        }
                    });
               })
            }

            });
        });


        $(document).on("change",".times",function () {
            var thistime=document.getElementById(dayOfWeek).value;
            $("#apmnttype").remove();
            $("#apptype").html(
                "<input type='hidden' name='hospital_id' id='hospital_id' value='"+hospital_id+"'>"+
                "<input type='hidden' name='doctor_id' id='doctor_id' value='"+doctor_id+"'>"+
                "<input type='hidden' name='dayOfWeek' id='dayOfWeek' value='"+dayOfWeek+"'>"+
                "<input type='hidden' name='times' id='tiimes' value='"+thistime+"'>"+
                    "<div class='form-group'>"+
                        "<label>Appointment type <span style='color: red;'>*</span></label>"+
                        "<select class='form-control' id='apttype' name='apttype'>"+
                        "<option value=''>Select appointment type</option>"+
                        "<option value='chamber'>On chamber</option>"+
                        "<option value='video' disabled>On video call</option>"+
                        "</select>"+
                    "</div>");
        });

        $(document).on("change","#apttype",function () {

            $("#appointmentform").html("<div class='row'>"+
                "<div class='col-sm-6'>" +
                    "<div class='form-group'>"+
                    "<label>Name <span style='color: red'>*</span></label>"+
                    "<input type='text' name='name' required class='form-control'>"+
                    "</div>"+
                "</div>"+
                "<div class='col-sm-6'>" +
                    "<div class='form-group '>"+
                    "<label>Phone Number <span style='color: red'>*</span></label>"+
                    "<input type='text' name='phone' required class='form-control'>"+
                    "</div>"+
                "</div>"+

                "<div class='col-sm-6'>" +
                    "<div class='form-group '>"+
                    "<label>Email </label>"+
                    "<input type='email' name='email'  class='form-control'>"+
                    "</div>"+
                "</div>"+
                "<div class='col-sm-6'>" +
                    "<div class='form-group '>"+
                    "<label>Address <span style='color: red'>*</span></label>"+
                    "<input type='text' name='address' required class='form-control'>"+
                    "</div>"+
                    "<div class='button  no-padding text-center'>"+
                    "<button type='submit' class='btn black-border'>Book appointment</button>"+
                    "</div>"+
                "</div>"+
            "</div>");

        });

        function validateAppointmentForm() {
            var apptDate=document.forms["appmntime"]["apptDate"].value;
            var dayOfWeek=document.forms["appmntime"]["dayOfWeek"].value;
            var name=document.forms["appmntime"]["name"].value;
            var phone=document.forms["appmntime"]["phone"].value;
            var address=document.forms["appmntime"]["address"].value;
            var times=document.forms["appmntime"]["times"].value;
            var apttype=document.forms["appmntime"]["apttype"].value;


            if(times==''|| times!=times){
                $('#warning').text('Select valid time !!');
                return false;
            }else if(apptDate==''){
                $('#warning').text('Valid date required!!');
                return false;
            }else if(apttype==''){
                $('#warning').text('Valid type required!!');
                return false;
            }else if(dayOfWeek==''){
                $('#warning').text('Invalid request!!');
                return false;
            }else if(name==''){
                $('#warning').text('Name required!!');
                return false;
            }else if(phone=='' || isNaN(phone)){
                $('#warning').text('Valid phone number required !!');
                return false;
            }else if(address==''){
                $('#warning').text('Address required!!');
                return false;
            }else if(doctor_id!=document.forms["appmntime"]["doctor_id"].value){
                $('#warning').text('Invalid request!!');
                return false;
            }else if(hospital_id!=document.forms["appmntime"]["hospital_id"].value){
                $('#warning').text('Invalid request!!');
                return false;
            }else if(dayOfWeek!=document.forms["appmntime"]["dayOfWeek"].value){
                $('#warning').text('Invalid request!!');
                return false;
            }else{
                return true;
            }
        }
    </script>
@endsection


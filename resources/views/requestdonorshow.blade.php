        @extends('layouts.master')
        @section('title', 'Deshidoctor | Search')
        @section('content')
        <section id="blog" class=" v2">
        <div class="container">
        <div class="row">
        <div class="col-sm-9 col-sm-offset-3 search-page">
        <div class="search-box text-center">
            <form>
                <select>
                    <option>Dhaka</option>
                    <option>Gazipur</option>
                    <option>Rangpur</option>
                    <option>Rajshahi</option>
                </select>
                <select>
                    <option>Dhaka</option>
                    <option>Gazipur</option>
                    <option>Rangpur</option>
                    <option>Rajshahi</option>
                </select>
                <input type="text" name="">
                <button type="submit" name=""><i class="fa fa-search"></i></button>
            </form>
        </div>
        </div>
        </div>
        <div class="row">
        <aside class="col-sm-4 col-md-3">
        <div class="widget category animate-in move-up animated">
            <h5>Categories</h5>

            <ul class="pull-left">
                @foreach($counters as $c)
                    <li><a href="{{url('/blood/search/'.$c->blood_group)}}">{{$c->blood_group}} <span> ( {{$c->total}} ) </span></a> </li>
                @endforeach

            </ul>
        </div>
        <div class="widget recent-post animate-in move-up animated">
            <h5>Recent Posts</h5>
            <ul>
                <li><a href="#">Smarter Grids With Sass And ...</a> </li>
                <li><a href="#">Quantity Ordering With CSS</a> </li>
                <li><a href="#">Gallery Post</a> </li>
                <li><a href="#">Video Post</a> </li>
                <li><a href="#">Image Post</a> </li>
            </ul>
        </div>
        </aside>
        <div class="col-md-9">
        <div id="cart" class="space-bottom">
            <div class="cart-product no-padding">
                <table class="table">

                    <p class="text-align:center text-success">{{Session::get('message')}}</p>
                    <br>
                    <br>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Blood Group</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Area</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($seakers as $seaker)
                        <tr>
                            <th scope="row"> {{ $seakers->id }}</th>
                            <td>{{ $seakers->name }}</td>
                            <td>{{ $seakers->address }}</td>
                            <td>{{ $seakers->hname }}</td>
                            <td>{{ $seakers->haddress }}</td>
                            <td>{{ $seakers->relationship }}</td>
                            <td>{{ $seakers->phone }}</td>
                            <td>{{ $seakers->area }}</td>
                            <td>{{ $seakers->donor_id }}</td>
                            <td>
                                <button
                                        type="submit"
                                        class="btn green btn2x radius-2x"
                                        data-toggle="modal"
                                        data-target="#favoritesModal">
                                    Request
                                </button>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        </div>
        </div>
        </div>
        </section>
        @endsection
@extends('layouts.master')
@section('title', 'Doctorinformation | Join')
@section('content')
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Join as a patient</h2>
                    <p>Welcome massage goes to here</p>
                </div>
            </div>
        </div>
    </section>
    <section id="about-us" class="space-top bg-color v2">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 no-padding">

                    @foreach ($errors->all() as $message)
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @endforeach

                    @if(session('success'))
                        <div class="alert alert-success">
                            {{session('success')}}
                        </div>
                    @endif
                    <form action="{{route('joinuser')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="col-sm-12">
                            <div class="form-group col-sm-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Your email" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Password</label>
                                <input type="password" class="form-control" name="password" id="email" placeholder="password" required>
                            </div>



                            <div class="form-group col-sm-6">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender" required>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control"  name="phone" id="phone" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="location">present Address/Location</label>
                                <input type="text" class="form-control"  name="present" id="location" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="permanent">permanent Address/Location</label>
                                <input type="text" class="form-control"  name="permanent" id="location" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="Occupation">Occupation</label>
                                <input type="text" class="form-control"  name="occupation" id="Occupation" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="reg_no">contact</label>
                                <input type="text" class="form-control"  name="contact" id="degree" required >
                            </div>




                            <div class="form-group col-sm-6">
                                <label for="image">Your Image</label>
                                <input type="file" class="form-control"  name="image" id="image" required>
                            </div>
                        </div>


                        <div class="form-group col-sm-12">
                            <h4>Terms and conditions</h4>
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur delectus,
                                dolore dolores facere laboriosam laudantium maiores
                                possimus reiciendis sint soluta tenetur vero. Aperiam debitis hic illum laboriosam maiores minus rem!</p>
                        </div>


                        <div class="form-group col-sm-12">
                            <div class="button-item pull-right">
                                <label class="checkbox-inline"><input type="checkbox" id="termsaccept" name="termsaccept" value="">I accept the term and conditions.</label>
                                <button class="btn radius-4x" id="submitbtn">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-sm-4 col-sm-offset-1 about-block text-right">
                    <img src="{{ asset('front-end/assets/images/patient1.png') }}" alt="Columba">
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#submitbtn").attr("disabled", "disabled");
        });

        $(document).on('click','#termsaccept',function () {
            if ($('#termsaccept').is(':checked')) {
                $('#termsaccept').val("yes");
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#termsaccept').val("not");
                $("#submitbtn").attr("disabled", true);
            }
        })
    </script>

@endsection
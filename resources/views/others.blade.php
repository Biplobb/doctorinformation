@extends('layouts.master')
@section('title', 'Deshidoctor | Contact')
@section('content')
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4" style="background-position: 50% 0%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Others Services</h2>
                    <p>A lot Of Style To Customize</p>
                </div>
            </div>
        </div>
    </section>

    <section id="features" class="space v4">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 feature-block text-center help-center animate-in move-up">
                    <div class="inner center">
                        <div class="inner-center">
                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 293.759 293.759" style="enable-background:new 0 0 293.759 293.759;" xml:space="preserve">
                                <g>
                                    <path d="M293.759,120.087c0-63.113-51.346-114.459-114.459-114.459c-35.435,0-67.158,16.187-88.169,41.552
								c-5.494-3.323-11.688-5.323-18.21-5.791c-21.62-1.901-42.049,7.976-55.691,25.064C1.848,85.72-3.579,111.723,2.343,137.792
								c10.534,46.157,33.328,88.664,65.923,122.958c16.582,17.41,38.868,27.382,61.166,27.382c24.082,0,46.668-11.604,58.211-32.804
								c3.755-6.641,5.533-14.064,5.264-21.594C249.629,226.984,293.759,178.596,293.759,120.087z M161.425,240.741
								c-5.933,11.135-18.182,17.391-31.993,17.391c-13.092,0-27.588-5.626-39.421-18.05c-28.886-30.392-49.083-68.056-58.413-108.935
								c-7.81-34.383,14.506-62.225,38.969-59.849c6.26,0.341,11.05,6.015,11.235,12.603c0.301,11.444,1.705,22.725,4.139,33.706
								c1.392,6.325-1.941,12.818-7.936,14.653c-2.721,0.797-5.441,1.593-8.162,2.39c7.695,26.276,20.786,50.689,38.414,71.639
								c2.169-1.825,4.338-3.651,6.507-5.476c2.101-1.725,4.654-2.548,7.239-2.548c3.378,0,6.81,1.406,9.358,4.046
								c7.8,8.104,16.419,15.515,25.785,22.099C162.53,228.208,164.605,235.338,161.425,240.741z M179.968,204.529
								c-1.706-1.683-3.552-3.238-5.531-4.634l-0.021-0.014l-0.02-0.014c-7.696-5.41-14.904-11.587-21.422-18.359
								c-8.134-8.427-19.412-13.242-30.971-13.242c-1.448,0-2.891,0.076-4.321,0.225c-4.202-6.652-7.918-13.582-11.134-20.763
								c1.764-2.146,3.33-4.486,4.669-7c4.805-9.017,6.234-19.517,4.022-29.568c-2.044-9.221-3.201-18.642-3.448-28.046l-0.001-0.026
								l-0.001-0.025c-0.107-3.818-0.711-7.58-1.774-11.191c15.277-21.886,40.629-36.243,69.284-36.243
								c46.571,0,84.459,37.888,84.459,84.459C263.759,166.434,226.232,204.167,179.968,204.529z"></path>
                                    <path d="M159.394,131.558c4.473-4.106,7.493-7.179,9.232-9.398c2.621-3.336,4.571-6.593,5.794-9.679
								c1.247-3.145,1.88-6.498,1.88-9.967c0-6.224-2.254-11.508-6.699-15.705c-4.416-4.164-10.495-6.274-18.071-6.274
								c-6.884,0-12.731,1.802-17.377,5.356c-2.803,2.146-4.961,5.119-6.415,8.839c-0.982,2.513-0.728,5.388,0.68,7.689
								c1.408,2.302,3.852,3.836,6.537,4.105c0.299,0.03,0.597,0.045,0.891,0.045c3.779,0,7.149-2.403,8.387-5.979
								c0.388-1.121,0.901-2.012,1.527-2.65c1.318-1.344,3.093-1.997,5.428-1.997c2.373,0,4.146,0.618,5.418,1.889
								c1.269,1.269,1.886,3.12,1.886,5.66c0,2.359-0.843,4.819-2.505,7.314c-0.891,1.305-3.554,4.36-11.108,11.398
								c-7.912,7.341-13.089,13.113-15.823,17.643c-1.93,3.195-3.338,6.573-4.187,10.04c-0.399,1.632-0.032,3.326,1.007,4.649
								c1.038,1.321,2.596,2.079,4.276,2.079h37.758c4.626,0,8.39-3.764,8.39-8.39c0-4.626-3.764-8.39-8.39-8.39h-17.051
								c0.139-0.164,0.282-0.328,0.428-0.493C152.401,138.089,155.129,135.47,159.394,131.558z"></path>
                                    <path d="M226.77,126.076h-0.865V85.971c0-2.997-2.439-5.436-5.436-5.436h-6.307c-1.797,0-3.475,0.886-4.489,2.37l-29.168,42.698
								c-0.851,1.246-1.301,2.703-1.301,4.212v6.919c0,2.997,2.439,5.436,5.436,5.436h23.945v5.787c0,4.775,3.885,8.66,8.66,8.66
								c4.775,0,8.66-3.885,8.66-8.66v-5.787h0.865c4.437,0,8.047-3.61,8.047-8.047C234.817,129.686,231.207,126.076,226.77,126.076z
								 M208.585,126.076h-10.748l10.748-15.978V126.076z"></path>
                                </g>
                            </svg>
                            <p>Need help, call us</p>
                            <a href="tel:1-800-700-6200" class="phone">1-800-700-6200</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 feature-block text-center box animate-in move-up animated">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/features-1.png')}}" alt="Columba">
                        <h4>Our Research</h4>
                        <p>Lorem ipsum dolor sit amet,
                            <br> consectetur adipiscing elit. Morbi
                            <br> sollicitudin justo non odio </p>
                    </div>
                </div>
                <div class="col-sm-4 feature-block text-center box animate-in move-up animated">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/features-2.png')}}" alt="Columba">
                        <h4>Our Transport</h4>
                        <p>Lorem ipsum dolor sit amet,
                            <br> consectetur adipiscing elit. Morbi
                            <br> sollicitudin justo non odio </p>
                    </div>
                </div>
                <div class="col-sm-4 feature-block animate-in move-up animated">
                    <div class="inner">
                        <h3>Medical Services That You Can Trust</h3>
                        <p>At Columba, we pride ourselves in premium yet affordable dental care. We’re a warm family of 14 offices situated (as of April of 2016) throughout the Southern Ontario region in Canada. </p>
                        <a href="#" class="btn">View our Services</a>
                    </div>
                </div>
                <div class="col-sm-4 feature-block text-center box animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/features-3.png')}}" alt="Columba">
                        <h4>Our Doctor</h4>
                        <p>Lorem ipsum dolor sit amet,
                            <br> consectetur adipiscing elit. Morbi
                            <br> sollicitudin justo non odio </p>
                    </div>
                </div>
                <div class="col-sm-4 feature-block text-center box animate-in move-up">
                    <div class="inner">
                        <img src="{{asset('front-end/assets/images/features-4.png')}}" alt="Columba">
                        <h4>Our Care</h4>
                        <p>Lorem ipsum dolor sit amet,
                            <br> consectetur adipiscing elit. Morbi
                            <br> sollicitudin justo non odio </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
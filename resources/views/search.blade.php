@extends('layouts.master')
@section('title', 'Doctor Information | Search')
@section('content')
    <section id="breadcrumb" class="light-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2" style="margin-bott">

                        {{--  <div class="titless" style="overflow:hidden">
                              <div class="title" align="center"  >
                                  {!! Form::open(['route' => 'searchPDF','method'=>'post','class'=>'class_name']) !!}
                                  {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}

                                  <button class="clickable" id="clickable">.        search    .</button>

                                  --}}{{--{!! Form::submit('search') !!}--}}{{--
                                  {!! Form::close() !!}
                              </div>
                          </div>--}}
                        {!! Form::open(['route' => 'searchPDFmain','method'=>'post','id','=',"search"]) !!}
                        {{--<form id="search" method="post" onsubmit="return validdata()">--}}
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-2">
                                <div class="gutter">
                                    <select name="area" id="area" class="form-control" onchange="enable_lookingfor()">
                                        <option value="">Search Area</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Narayonganj">Narayganj</option>
                                        <option value="Savar">Savar</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="gutter">
                                    <select name="lookingfor" id="lookingfor" class="form-control" onchange="enable_keyword()">
                                        <option value="">Looking for</option>
                                        <option value="doctor">Doctor</option>
                                        <option value="hospital">Hospital</option>
                                        <option value="diagnostic">Diagnostics</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="gutter">
                                    <input id="keyword" class="form-control" name="keyword" type="text" placeholder="Type Keyword"  autocomplete="off">
                                    <input type="hidden" name="idnt" id="idnt">
                                    <div class="suggestion">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="gutter">
                                    <div class="search-icon">
                                        <button id="dosearch"> <span class="fa fa-search" ></span></button>
                                        {{--<button id="dosearch"> <span class="fa fa-search" ></span></button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <h2>Your Health We Care</h2>
                    </div>
                </div>
            </div>
        </section>
    <section id="blog" class=" bg-color space v2">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                    @foreach($data as $d)
                        <div class="list-items">
                               <div class="col-md-3">
                                   <img src="{{asset('image/doctor-photo/'.$d->dphoto)}}" alt="Columba">
                               </div>
                               <div class="col-md-9">
                                   <div class="choose-text">
                                       <h4>
                                           <a href="#">{{$d->dname}}</a>
                                       </h4>
                                       <p>{{$d->degree}}</p>
                                   </div>
                                   <p> <i class="fa fa-map-marker"></i> Chamber : {{$d->hname}}  
                                       
                                       <br />
                                      
                                   <a href="{{route('doctor_profile',['id'=>$d->did])}}" class="btn green radius-2x"> <i class="fa fa-eye"></i> View Profile</a>
                               </div>
                           </div>

                    @endforeach
                        {{$data->links()}}
                    </div>
                </div>
                <aside class="col-sm-4 col-md-3">
                    <div class="widget tags doctor-cat animate-in move-up animated">
                        <h5>Categories</h5>
                        <ul>
                            <li>
                                <i class="icofont icofont-dna" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Cardiology'])}}">Cardiology</a>
                            </li>
                            <li>
                                 <i class="icofont icofont-tooth" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Dental'])}}">Dental</a>
                            </li>
                            <li>
                                 <i class="icofont icofont-laboratory" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Dermatology'])}}">Dermatology</a>
                            </li>
                            <li>
                                 <i class="icofont  icofont-pills" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Gastroenterology'])}}">Gastroenterology</a>
                            </li>
                            <li>
                                 <i class="icofont icofont-drug" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Medicine'])}}">Medicine</a>
                            </li>
                            <li>
                                 <i class="icofont icofont-xray" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Orthopaedics'])}}">Orthopaedics</a>
                            </li>
                            <li>
                                 <i class="icofont icofont-doctor" aria-hidden="true"></i>
                                <a href="{{route('specialitysearch',['sp'=>'Psychology'])}}">Psychology</a>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        var token='{{\Illuminate\Support\Facades\Session::token()}}';

    </script>

    <script src="{{asset('front-end/assets/js/myjs.js')}}"></script>
@endsection
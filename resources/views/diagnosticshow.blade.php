@extends('layouts.master')
@section('title', 'Doctorinformation | Home')
@section('content')

    @foreach($diagnostic_center_tests as $diagnostic_center_tests)

    <section id="breadcrumb" class="light-overlay" data-stellar-background-ratio="0.4" style="background-position: 50% 0%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in move-up animated">
                    <h2>{{ $diagnostic_center_tests->diagnosticcentername }}</h2>
                    <p>{{ $diagnostic_center_tests->diagnosticcenterlocation }}</p>

                </div>
            </div>
        </div>

    </section>
    <section id="single-service" class="space">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 service-block">

                    <img class="img-responsive" src="{{ asset('image/diagnostic-photo/'.$diagnostic_center_tests->diagnosticphoto) }}" alt="Columba">
                    <h2>About:</h2>
                    <p>{{ $diagnostic_center_tests->diagnosticcenterabout }}</p><br>
                    <div>
                        <h2>Our Test list:</h2>

                    </div>

                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>

                                <th>Sl</th>
                                <th>Test Name</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Discount Price</th>

                            </tr>
                        </thead>
                        @php
                        $s=1;
                        @endphp
                        @foreach($testss as $tests)
                        <tbody>
                               <tr>
                                   <td>{{ $s++ }}</td>
                                   <td>{{ $tests->testname }}</td>
                                   <td>{{ $tests->diagnosticcentertestsprevious }}</td>
                                   <td>{{ $tests->diagnosticcentertestsdiscount }}%</td>
                                   <td>{{ $tests->diagnosticcentertestsdiscountprice }}</td>



                               </tr>

                        </tbody>
                            <input type="hidden" name="Diagnostic_Center_id" value="{{ $diagnostic_center_tests->diagnosticcenterid }}">
                            @endforeach
                    </table>


                    <div class="row" id="digForm">

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name="name" type="text" class="form-control">

                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input name="phone" type="text" class="form-control">
                                </div>
                            </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Address</label>
                                <input name="address" type="text" class="form-control">
                            </div>
                        </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>prescription photo</label>
                                    <input name="photo" type="file" class="form-control">
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-success">submit</button>
                                </div>
                            </div>
                        {!! Form::close() !!}

                    </div>

                    <h3>Other diagnostic center</h3>
                    <section class="v5 bg-color small-space">
                        <div class="row">


                                @foreach($diagnostics as $diagnostic)

                                <a  href="{{ url('singlediagnosticshow/'.$diagnostic->id)  }}">
                                <div class="col-sm-4 team-block">
                                    <div class="inner">
                                        <div class="user-img">
                                            <img   src="{{ asset('image/diagnostic-photo/'.$diagnostic->image) }}" alt="Columba">
                                        </div>
                                        <div class="team-text no-padding">
                                            <div class="name">{{   $diagnostic->name }} </div>
                                            <div class="experience">{{ $diagnostic->name }}</div>

                                        </div>
                                    </div>
                                </div>
                                    @endforeach
                            </a>
                        </div>
                    </section>
                    <div class="col-sm-12 text-center load-more animate-in move-up animated">
                        <a href="{{ url('diagnostics') }}" class="btn">More Blog</a>
                    </div>
                </div>
                <aside class="col-sm-3">
                    <div class="widget contact">
                        <h1> up to {{ max(array($tests->diagnosticcentertestsdiscount))  }}</h1>
                        <p>Get Discount through Deshidoctor</p>
                        <a href="#" class="btn">Contact Us</a>
                    </div>
                    <div class="widget testimonials">
                        <div class="inner">
                            <div class="icon">“</div>
                            <h1>About</h1>
                            <p>Before selecting a Financial Adviser we had discussed our finances with a few other advisers, but from our first meeting with Financial solutions it was apparent that their depth of knowledge far exceeded that of others we had spoken with.</p>
                        </div>

                    </div>
                </aside>
            </div>
        </div>
                </div>

                </div>
                    @endforeach
            </div>

    </section>
@endsection

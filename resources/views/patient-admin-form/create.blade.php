
@extends('patient-admin-form.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Patient </h2>
                <small class="text-muted">Patient information submit form</small>
                <h3 style="color:red;text-align: center">
                    <?php
                    $exception = Session::get('exception');
                    if($exception){
                        echo $exception;
                        Session::put('exception',null);
                    }
                    ?>
                </h3>
                <h3 style="color:green;text-align: center">
                    <?php
                    $message = Session::get('message');
                    if($message){
                        echo $message;
                        Session::put('message',null);
                    }
                    ?>
                </h3>
            </div>
            <div class="row clearfix">
                {!! Form::open(['url' => '/admin/patientinformation/store','enctype'=>'multipart/form-data','method'=>'post']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Patient Information</h2>
                        </div>
                        <div class="body">

                            <h1>Must be anyone selected otherwise not working</h1>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <h3>prescription write:(Optional)</h3>
                                            <textarea name="prescriptionwrite" rows="20" cols="140"></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <input type="hidden" name="user_id" value="{{ $p }}">
                            <h3>prescription photo:(Optional)</h3>

                            <input type="file" name="prescription" ><br>

                            <h3>Report photo:(Optional)</h3>

                            <input type="file" name="report" ><br>






                            <div class="col-sm-6 col-xs-12">

                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


@extends('layouts.master')
@section('title', 'Deshidoctor | Interview')
@section('content')
    <section id="services" class="space v4">
        <div class="container">
            <div class="row">
                <div class="service-block col-sm-4 animate-in move-up animated">
                    <div class="inner">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RewqLyhwK28?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>
                            Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus
                        </p>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up animated">
                    <div class="inner">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RewqLyhwK28?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>
                            Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus
                        </p>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up animated">
                    <div class="inner">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RewqLyhwK28?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>
                            Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus
                        </p>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up animated">
                    <div class="inner">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RewqLyhwK28?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>
                            Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus
                        </p>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up animated">
                    <div class="inner">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RewqLyhwK28?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>
                            Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus
                        </p>
                    </div>
                </div>
                <div class="service-block col-sm-4 animate-in move-up animated">
                    <div class="inner">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/RewqLyhwK28?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                        </div>
                        <h4><a href="#">X- Ray</a> </h4>
                        <p>
                            Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="action-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 ">
                    <div class="col-sm-8 animate-in move-up">
                        <h2>Please join our Youtube Chanel</h2>
                    </div>
                    <div class="col-sm-4 animate-in move-up">
                        <a href="#" class="btn red"> <i class="fa fa-youtube-play" aria-hidden="true"></i> Subscribe</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endsection
@extends('layouts.master')

@section('content')

    <!--Bread Crumb-->
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block">
                    <h2>Frequently Asked Questions</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
                <div class="col-sm-6 bread-block text-right">
                    <a href="#" class="btn">Book appointment</a>
                </div>
            </div>
        </div>
    </section>
    <!--FAQ-->
    <section id="faq" class="space">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 no-padding faq-block">
                    <div class="col-sm-12 menu">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#about">About Columba</a></li>
                            <li><a data-toggle="tab" href="#eligibility">Columba eligibility</a></li>
                            <li><a data-toggle="tab" href="#plan">choosing a Medicare plan</a></li>
                            <li><a data-toggle="tab" href="#payments">columba costs and payments</a></li>
                        </ul>
                    </div>
                    <div class="tab-content col-sm-12 no-padding">
                        <div id="about" class="tab-pane fade in active">
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion11">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion11" href="#collapse1">
                                                    <i class="fa fa-check-circle-o"></i>How does Medicare work?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse1" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default active">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion11" href="#collapse2">
                                                    <i class="fa fa-check-circle-o"></i>Which doctors accept Medicare assignment?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse2" class="panel-collapse collapse  in">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion11" href="#collapse3">
                                                    <i class="fa fa-check-circle-o"></i>Do I need Medicare Part A or Part B if I am still working?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse3" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion11" href="#collapse4">
                                                    <i class="fa fa-check-circle-o"></i>What is the "Welcome to Medicare" physical exam?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse4" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion12" href="#collapse_1_1">
                                                    <i class="fa fa-check-circle-o"></i>How do I change my address through Social Security?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_1_1" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion12" href="#collapse_1_2">
                                                    <i class="fa fa-check-circle-o"></i>How do I report disability fraud?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_1_2" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion12" href="#collapse_1_3">
                                                    <i class="fa fa-check-circle-o"></i>That's the difference between home health care and
                                                    nursing home care?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_1_3" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion12" href="#collapse_1_4">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover funeral costs?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_1_4" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion12" href="#collapse_1_5">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover overseas travel emergencies?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse_1_5" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="eligibility" class="tab-pane fade">
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion21">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion21" href="#collapse21">
                                                    <i class="fa fa-check-circle-o"></i>How does Medicare work?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse21" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default active">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion21" href="#collapse22">
                                                    <i class="fa fa-check-circle-o"></i>Which doctors accept Medicare assignment?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse22" class="panel-collapse collapse  in">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion21" href="#collapse23">
                                                    <i class="fa fa-check-circle-o"></i>Do I need Medicare Part A or Part B if I am still working?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse23" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion21" href="#collapse24">
                                                    <i class="fa fa-check-circle-o"></i>What is the "Welcome to Medicare" physical exam?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse24" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion22">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion22" href="#collapse25">
                                                    <i class="fa fa-check-circle-o"></i>How do I change my address through Social Security?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse25" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion22" href="#collapse26">
                                                    <i class="fa fa-check-circle-o"></i>How do I report disability fraud?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse26" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion22" href="#collapse27">
                                                    <i class="fa fa-check-circle-o"></i>That's the difference between home health care and
                                                    nursing home care?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse27" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion22" href="#collapse28">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover funeral costs?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse28" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion22" href="#collapse29">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover overseas travel emergencies?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse29" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="plan" class="tab-pane fade">
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion31">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion31" href="#collapse1">
                                                    <i class="fa fa-check-circle-o"></i>How does Medicare work?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse31" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default active">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion31" href="#collapse32">
                                                    <i class="fa fa-check-circle-o"></i>Which doctors accept Medicare assignment?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse32" class="panel-collapse collapse  in">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion31" href="#collapse33">
                                                    <i class="fa fa-check-circle-o"></i>Do I need Medicare Part A or Part B if I am still working?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse33" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion31" href="#collapse34">
                                                    <i class="fa fa-check-circle-o"></i>What is the "Welcome to Medicare" physical exam?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse34" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion32">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion32" href="#collapse35">
                                                    <i class="fa fa-check-circle-o"></i>How do I change my address through Social Security?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse35" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion32" href="#collapse36">
                                                    <i class="fa fa-check-circle-o"></i>How do I report disability fraud?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse36" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion32" href="#collapse37">
                                                    <i class="fa fa-check-circle-o"></i>That's the difference between home health care and
                                                    nursing home care?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse37" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion32" href="#collapse38">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover funeral costs?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse38" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion32" href="#collapse39">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover overseas travel emergencies?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse39" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="payments" class="tab-pane fade">
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion41">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion41" href="#collapse41">
                                                    <i class="fa fa-check-circle-o"></i>How does Medicare work?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse41" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default active">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion41" href="#collapse42">
                                                    <i class="fa fa-check-circle-o"></i>Which doctors accept Medicare assignment?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse42" class="panel-collapse collapse  in">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion41" href="#collapse43">
                                                    <i class="fa fa-check-circle-o"></i>Do I need Medicare Part A or Part B if I am still working?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse43" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion41" href="#collapse44">
                                                    <i class="fa fa-check-circle-o"></i>What is the "Welcome to Medicare" physical exam?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse44" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 faq-item">
                                <div class="panel-group accordion" id="accordion42">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion42" href="#collapse45">
                                                    <i class="fa fa-check-circle-o"></i>How do I change my address through Social Security?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse45" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion42" href="#collapse46">
                                                    <i class="fa fa-check-circle-o"></i>How do I report disability fraud?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse46" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion42" href="#collapse47">
                                                    <i class="fa fa-check-circle-o"></i>That's the difference between home health care and
                                                    nursing home care?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse47" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion42" href="#collapse48">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover funeral costs?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse48" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="collapsed" data-parent="#accordion42" href="#collapse49">
                                                    <i class="fa fa-check-circle-o"></i>Does Medicare cover overseas travel emergencies?</a>
                                            </h4>
                                        </div>
                                        <div id="collapse49" class="panel-collapse collapse">
                                            <div class="panel-body">Cras nibh diam, semper eu bibendum id, interdum ut neque. Nunc sed lacinia nisl, molestie pulvinar nunc. Suspendisse vel orci eu mauris consectetur viverra.Mauris id egestas tortor, non blandit dolor. Phasellus euismod ligula non neque volutpat, sit amet feugiat elit pellentesque. Nulla facilisi. Integer posuere bibendum nisl, </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection
@extends('layouts.master')
@section('title', 'Deshidoctor | Blog')
@section('content')
    <section id="blog" class="space v3">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-9 no-padding">
                    <div class="col-sm-12 no-padding blog-base">

                        @foreach($blog as $blogs)
                        <article class="col-sm-12 blog-block animate-in move-up">
                            <div class="inner">
                                <img src="{{asset('image/blog-photo/'.$blogs->image)}}" width="100px",height="100x">
                                <h3><a href="#">{{ $blogs->title }}</a> </h3>
                                <ul class="meta">
                                    <li><a href="#">{{ $blogs->author }}</a> </li>
                                    <li><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $blogs->created_at->diffForHumans() }}</a> </li>
                                    <li><a href="#"><i class="fa fa-tags" aria-hidden="true"></i> {{ $blogs->type===1?'featured':'published' }}</a> </li>
                                    {{--<li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i> 3 comment’s</a> </li>--}}

                                </ul>




                                <p style="display: inline; text-align: justify">

                                @php
                                $limit=1000;
                                    if (strlen($blogs->blog_post) > $limit){
                                   echo $stringCut = substr($blogs->blog_post, 0, $limit);
                                   // echo substr($stringCut, 0, strrpos($stringCut, ''));
                                    }
                                    else
                                    {
                                    echo $blogs->blog_post;
                                    }
                             @endphp
                               </p>
                                <a href="{{url('blogdetails/'.$blogs->id)}}"> [Continue Reading ...]</a>

                            </div>



                        </article>

                        @endforeach


                    </div>


                    <div class="col-sm-12 co-pagination animate-in move-up">
                        <ul class="pagination">
                            <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <aside class="col-sm-4 col-md-3">
                        <div class="widget category animate-in move-up animated">
                            <h5>Categories</h5>
                            <ul>
                                @if(count($categories)>0)
                                    @foreach($categories as $category)
                                <li><a href="{{url('/blog/category/'.$category->id)}}">{{$category->name}}</a><span>{{$category->name}}</span> </li>
                                    @endforeach
                                    @else
                                    <li><a href="#">N/A</a> </li>
                                    @endif
                            </ul>
                        </div>
                        <div class="widget recent-post animate-in move-up animated">
                            <h5>Recent Posts</h5>
                            <ul>
                                @foreach($recent_posts as $post)
                                    <li><a href="{{url('blogdetails/'.$post->id)}}">{{$post->title}}</a> </li>
                                @endforeach

                            </ul>
                        </div>

                </aside>
            </div>
        </div>
    </section>
@endsection
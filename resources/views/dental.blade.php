@extends('layouts.master')
@section('title', 'Deshidoctor | Dental hospitals')
@section('content')
    <section id="breadcrumb" class="light-overlay" data-stellar-background-ratio="0.4" style="background-position: 50% 0%;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in move-up animated">
                    <h2>All Dental Hospitals</h2>
                    <p>Affordable Treatments, Honest &amp; Experienced Dentists.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="blog" class="space v1">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 no-padding blog-base">
                    <article class="col-sm-3 blog-block animate-in move-up animated">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>Aehi Advanced Eye Hospital and Institute</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up animated">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital2.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>Apollo Spectra</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up animated">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>Columbia Asia Global</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital3.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>Give Your Kids a Jumpstart on Oral Health Care!</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                     <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital4.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>What To Expect At The Dentist… When You’re Expecting</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital5.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>What Your Dentist Can Tell By Looking In Your Mouth</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital6.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>Give Your Kids a Jumpstart on Oral Health Care!</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>What To Expect At The Dentist… When You’re Expecting</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital2.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>What Your Dentist Can Tell By Looking In Your Mouth</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital3.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>Give Your Kids a Jumpstart on Oral Health Care!</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <article class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital4.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>What To Expect At The Dentist… When You’re Expecting</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </article>
                    <div class="col-sm-3 blog-block animate-in move-up">
                        <div class="inner">
                            <img src="{{asset('front-end/assets/images/hospital5.jpg')}}" alt="Columba">
                            <a class="hover" href="#">
                                <div class="inner">
                                    <h4>What Your Dentist Can Tell By Looking In Your Mouth</h4>
                                    <div class="date">25th February 2016</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <article class="col-sm-12 text-center load-more animate-in move-up">
                    <a href="#" class="btn">Load more</a>
                </article>
            </div>
        </div>
    </section>
@endsection
@extends('doctor.layout.master')
@section('content')

<section class="content">
    <div class="container-fluid">
    <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Your Chambers</h2>
            </div>
            <div class="body">
                <div class="col-sm-6 col-md-3 col-lg-12">
                    @if(session('updated'))
                        <div class="alert alert-success">
                            <strong> {{session('updated')}} !!</strong>
                        </div>
                    @endif

                    @foreach($data as $d)
                        <div class="thumbnail">
                            <div class="caption">
                                <h3>Chamber at :{{$d->chamber}}</h3>
                                <p>
                                    {{$d->location}}
                                </p>
                                <p><strong>{{$d->day_name}} Starting time :<span class="badge bg-cyan">{{Carbon\Carbon::parse($d->start)->format('h.i a')}} </span> Ending time <span class="badge bg-cyan">{{Carbon\Carbon::parse($d->end)->format('h.i a')}} </span></strong></p>
                            </div>
                            <a href="javascript:void(0);" data-toggle="modal" data-target="#defaultModal" data-id1="{{$d->hospitalid}}" data-id2="{{$d->day_id}}"  data-id3="{{$d->day_name}}" data-id4="{{$d->start}}" data-id5="{{$d->end}}" data-id6="{{$d->scheduleid}}" data-id7="{{$d->available}}"   class="btn btn-raised g-bg-green btn-primary waves-effect btn-sm" role="button" id="editbutton">Edit info</a>
                        </div>
                    @endforeach

                </div>

            </div>
        </div>
    </div>
    </div>
</section>

@endsection

<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
            </div>

            <form action="{{route('updateschedulebydoctor')}}" method="post">
                <input type="hidden" id="hospitalid" name="hospitalid">
                <input type="hidden" id="scheduleid" name="scheduleid">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="col-md-3">
                                <label for="">Day</label>
                                <select class="form-control show-tick" name="day" id="day">
                                    <option value="">Select day</option>
                                    <option value="1">Saturday</option>
                                    <option value="2">Sunday</option>
                                    <option value="3">Monday</option>
                                    <option value="4">Tuesday</option>
                                    <option value="5">Wednesday</option>
                                    <option value="6">Thursday</option>
                                    <option value="7">Friday</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="">Start time</label>
                                <select class="form-control show-tick" name="start" id="start">
                                    <option value="">Start Time</option>
                                    <option value="10:00:00">10 am</option>
                                    <option value="11:00:00">11 am</option>
                                    <option value="12:00:00">12 am</option>
                                    <option value="13:00:00">1 pm</option>
                                    <option value="14:00:00">2 pm</option>
                                    <option value="15:00:00">3 pm</option>
                                    <option value="16:00:00">4 pm</option>
                                    <option value="17:00:00">5 pm</option>
                                    <option value="18:00:00">6 pm</option>
                                    <option value="19:00:00">7 pm</option>
                                    <option value="20:00:00">8 pm</option>
                                    <option value="21:00:00">9 pm</option>
                                    <option value="22:00:00">10 pm</option>
                                </select>
                            </div>

                            <div class="col-md-3">
                                <label for="">End time</label>
                                <select class="form-control show-tick" name="end" id="end">
                                    <option value="">Start Time</option>
                                    <option value="10:00:00">10 am</option>
                                    <option value="11:00:00">11 am</option>
                                    <option value="12:00:00">12 am</option>
                                    <option value="13:00:00">1 pm</option>
                                    <option value="14:00:00">2 pm</option>
                                    <option value="15:00:00">3 pm</option>
                                    <option value="16:00:00">4 pm</option>
                                    <option value="17:00:00">5 pm</option>
                                    <option value="18:00:00">6 pm</option>
                                    <option value="19:00:00">7 pm</option>
                                    <option value="20:00:00">8 pm</option>
                                    <option value="21:00:00">9 pm</option>
                                    <option value="22:00:00">10 pm</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">

                                <input type="radio" id="available"
                                       name="available" value="yes" >
                                <label for="available">Available</label>
                                <input type="radio" id="unavailable"
                                       name="available" value="no">
                                <label for="unavailable">Unavailable</label>
                            </div>

                            <div class="col-md-6">


                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn  btn-raised bg-green waves-effect">UPDATE</button>
                    <button type="button" class="btn  btn-raised bg-blue waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </form>
        </div>
    </div>
</div>

@section('script')
    <script>
        $(document).on('click','#editbutton',function () {
            document.getElementById("day").value = $(this).data('id2');
            document.getElementById("start").value = $(this).data('id4');
            document.getElementById("end").value = $(this).data('id5');
            document.getElementById("hospitalid").value = $(this).data('id1');
            document.getElementById("scheduleid").value = $(this).data('id6');
//            document.getElementById("available").value = $(this).data('id7');

            if($(this).data('id7')=='yes'){
                document.getElementById("available").checked=true;
            }else {
                document.getElementById("unavailable").checked=true;
            }
        })
    </script>
@endsection
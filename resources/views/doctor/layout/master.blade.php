<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>:: Deshi Doctor ~ Admin ::</title>


    <!-- Custom Css -->
    <link href="{{ asset('back-end/assets/css/main.css') }}" rel="stylesheet" type="text/css">

    <link rel="icon" href="{{ asset('back-end/favicon.ico')}}" type="image/x-icon"><!-- Favicon-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{ asset('back-end/assets/plugins/morrisjs/morris.css') }}" rel="stylesheet" />


    <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('back-end/assets/css/themes/all-themes.css') }}" rel="stylesheet" />




{{--These are for Book Appoinment Form--}}

<!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('back-end/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <!-- Wait Me Css -->
    <link href="{{ asset('back-end/assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('back-end/assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    {{--This is for Appoinment Doctor Schedule Form--}}
    <link rel="stylesheet" href="{{ asset('back-end/assets/plugins/fullcalendar/fullcalendar.min.css')}}">

    <link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    {{------------------------------------js-----------------------------}}
    <script src="{{ asset('back-end/assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
    <script src="{{ asset('back-end/assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>



</head>

<body class="theme-cyan">

<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- #Float icon -->

<!-- #Float icon -->
<!-- Morphing Search  -->
<div id="morphsearch" class="morphsearch">
    {{--<form class="morphsearch-form">--}}
        {{--<div class="form-group m-0">--}}
            {{--<input value="" type="search" placeholder="Search..." class="form-control morphsearch-input" />--}}
            {{--<button class="morphsearch-submit" type="submit">Search</button>--}}
        {{--</div>--}}
    {{--</form>--}}
    <div class="morphsearch-content">
        <div class="dummy-column">
            <h2>People</h2>
            <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar1.jpg')}}" alt=""/>
                <h3>Sara Soueidan</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar4.jpg')}}" alt=""/>
                <h3>Rachel Smith</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar3.jpg')}}" alt=""/>
                <h3>Peter Finlan</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar6.jpg')}}" alt=""/>
                <h3>Patrick Cox</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar6.jpg')}}" alt=""/>
                <h3>Tim Holman</h3>
            </a></div>
        <div class="dummy-column">
            <h2>Popular</h2>
            <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar1.jpg')}}" alt=""/>
                <h3>Sara Soueidan</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar4.jpg')}}" alt=""/>
                <h3>Rachel Smith</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar3.jpg')}}" alt=""/>
                <h3>Peter Finlan</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar6.jpg')}}" alt=""/>
                <h3>Patrick Cox</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar5.jpg')}}" alt=""/>
                <h3>Tim Holman</h3>
            </a> </div>
        <div class="dummy-column">
            <h2>Recent</h2>
            <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar1.jpg')}}" alt=""/>
                <h3>Sara Soueidan</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar4.jpg')}}" alt=""/>
                <h3>Rachel Smith</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar3.jpg')}}" alt=""/>
                <h3>Peter Finlan</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar6.jpg')}}" alt=""/>
                <h3>Patrick Cox</h3>
            </a> <a class="dummy-media-object" href="#"> <img class="round" src="{{ asset('back-end/assets/images/random-avatar5.jpg')}}" alt=""/>
                <h3>Tim Holman</h3>
            </a></div>
    </div>
    <!-- /morphsearch-content -->
    <span class="morphsearch-close"></span> </div>
<!-- Top Bar -->
<nav class="navbar clearHeader">
    <div class="container-fluid">
        <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" href="{{asset(route('doctordashboard'))}}">  Doctor Information</a> </div>
        <ul class="nav navbar-nav navbar-right">

        </ul>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="admin-image"> <img src="{{ asset('back-end/assets/images/random-avatar7.jpg')}}" alt=""> </div>
            <div class="admin-action-info"> <span>Welcome</span>
                <h3>{{\Illuminate\Support\Facades\Auth::guard('doctor')->user()->name}}</h3>
                <ul>
                    <li><a data-placement="bottom" title="Full Screen" href="{{route('doctorlogout')}}" ><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
                </ul>
            </div>

        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active open"><a href="{{asset(route('doctordashboard'))}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-add"></i><span>My options</span> </a>

                        <ul class="ml-menu">
                            <li><a href="{{route('doctorchamber')}}">My chambers</a></li>
                            <li><a href="{{route('getAllAppointments')}}">Account setting</a></li>
                        </ul>

                    </li>

                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-calendar-check"></i><span>Appointment</span> </a>
                    <ul class="ml-menu">
                        <li><a href="{{route('getAllAppointments')}}">My appointments</a></li>
                    </ul>
                </li>

                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-calendar-check"></i><span>Blog</span> </a>
                    <ul class="ml-menu">
                        <li><a href="{{ route('writeblog')}}">Write blog</a></li>
                        <li><a href="{{ route('myblogs')}}">Your blogs</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
{{--Yield Section--}}

@yield('content')


<div class="color-bg"></div>


<!-- Jquery Core Js -->



<script src="{{ asset('back-end/assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->



<script src="{{ asset('back-end/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script> <!-- Sparkline Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/chartjs/Chart.bundle.min.js')}}"></script> <!-- Chart Plugins Js -->

<script src="{{ asset('back-end/assets/js/morphing.js')}}"></script><!-- Custom Js -->
<script src="{{ asset('back-end/assets/js/pages/charts/sparkline.min.js')}}"></script>
<script src="{{ asset('back-end/assets/js/pages/charts/chartjs.min.js')}}"></script>
<script src="{{ asset('back-end/assets/js/pages/index.js')}}"></script>

{{--These are for Book Appoinment Form--}}

<script src="{{ asset('back-end/assets/plugins/autosize/autosize.js')}}"></script> <!-- Autosize Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

<script src="{{ asset('back-end/assets/js/pages/forms/basic-form-elements.js')}}"></script>

{{--These are for Appoinment Doctor Schedule Form--}}

<script src="{{ asset('back-end/assets/bundles/fullcalendarscripts.bundle.js')}}"></script><!--/ calender javascripts -->
<script src="{{ asset('back-end/assets/js/pages/calendar/calendar.js')}}"></script>

<script>
    setTimeout(function() {
        $('.alert').fadeOut('slow');
    }, 3000);

</script>

@yield('script')

</body>

</html>

@extends('doctor.layout.master')

<section class="content profile-page">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12 p-l-0 p-r-0">

            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="card">

                    <div class="body">
                        <h4></h4>
                        <table class="table">
                            <thead>
                            <tr>

                                <th scope="col">id</th>
                                <th scope="col">Doctor id</th>
                                <th scope="col">title</th>
                                <th scope="col">subtitle</th>
                                <th scope="col">author</th>
                                <th scope="col">type</th>
                                <th scope="col">blog post</th>
                                <th scope="col">blog image</th>

                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <th scope="row"> {{ $blog->id }}</th>
                                <td>{{ $blog->doctor_id  }}</td>
                                <td>{{ $blog->title }}</td>
                                <td>{{ $blog->subtitle }}</td>
                                <td>{{ $blog->author }}</td>
                                <td>{{ $blog->type }}</td>
                                <td>{{ $blog->blog_post }}</td>
                                <td><img src="{{asset('image/blog-photo/'.$blog->image)}}" height="200px" width="200px"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>





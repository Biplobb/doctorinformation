@extends('doctor.layout.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Blog</h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        @if(session('postSuccess'))
                            <div class="alert alert-success">
                                <strong> {{session('postSuccess')}} !!</strong>
                            </div>
                        @endif

                        <form action="{{route('blog-post')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="body">
                                <div class="row clearfix">

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder=" Title"  name="title">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="Sub Title" name="subtitle">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-control">
                                            <select name="type" id="" class="form-control">
                                                <option>Type</option>
                                                <option value="Featured">Featured</option>
                                                <option value="Regular">Regular</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" class="form-control no-resize" placeholder="Type your blog post here....." name="blog_post"></textarea>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-control">
                                        <label for="Image">Insert image</label>
                                        <input type="file" name="image" >
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
        </div>


    </section>
@endsection


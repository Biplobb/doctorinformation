@extends('doctor.layout.master')
{{--@extends('admin.layouts.master')--}}

@section('content')
    <section class="content profile-page">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-md-12 p-l-0 p-r-0">
                    <section class="boxs-simple">
                        <div class="profile-header">
                            <div class="profile_info">
                                <div class="profile-image"><img
                                            src="{{ asset('image/doctor-photo/'.$doctorprofile->photo)}}" alt=""></div>
                                <h4 class="mb-0"><strong>{{ $doctorprofile->name }}</strong></h4>
                                <span class="text-muted col-white">{{$doctorprofile->speciality}}</span>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>{{$doctorprofile->name}}</h2>
                        </div>
                        <div class="body">
                            <p class="text-default"> galley of type and scrambled it to make a type specimen book. It
                                has survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                    <div class="card">
                        @if(session('updated'))
                            <div class="alert alert-success">
                                <strong> {{session('updated')}} !!</strong>
                            </div>
                        @endif
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#info" data-toggle="tab"
                                                                          aria-expanded="false">Doctor Info</a></li>
                            </ul>
                            <!-- Tab panes -->

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="info">
                                    <h4> {{$doctorprofile->name}}</h4>
                                    <p>Speciality {{$doctorprofile->speciality}}</p>
                                    <p>Contact : {{$doctorprofile->phone}}</p>
                                    <p>Email : {{$doctorprofile->email}}</p>
                                    <p>Address : {{$doctorprofile->address}}</p>
                                    <p>Awards : {{$doctorprofile->awards}}</p>
                                    <p>Experience : {{$doctorprofile->experience}} years</p>
                                </div>
                            </div>
                            <button type="button" class="btn btn-raised btn-primary waves-effect" data-toggle="modal"
                                    data-target="#defaultModal">UPDATE INFO
                            </button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


<div class="modal fade in" id="defaultModal" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Update your info</h4>
            </div>
            <div class="modal-body">

                <form action="{{route('update')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="name">Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{$doctorprofile->name}}">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="email">Email</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email"
                                               value="{{$doctorprofile->email}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="phone">Phone No.</label>
                                        <input type="text" name="phone" class="form-control" placeholder="Phone No."
                                               value="{{$doctorprofile->phone}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="address">Address</label>
                                        <input type="text" name="address" class="form-control" placeholder="Address"
                                               value="{{$doctorprofile->address}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="member">Membership</label>
                                    <div class="form-line">
                                        <input type="text" name="member" class="form-control" placeholder="Membership"
                                               value="{{$doctorprofile->member}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="award">Awards</label>
                                        <input type="text" class="form-control" name="awards" placeholder="Awards"
                                               value="{{$doctorprofile->awards}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="experience">Experience</label>
                                        <input type="text" class="form-control" name="experience"
                                               placeholder="Experience" value="{{$doctorprofile->experience}}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn bg-green waves-effect">UPDATE</button>
                        <button type="button" class="btn bg-white waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
@endsection

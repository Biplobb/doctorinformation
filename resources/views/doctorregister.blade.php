@extends('layouts.master')
@section('title', 'Deshidoctor | Join')
@section('content')
    <section id="breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Join as a doctor</h2>
                    <p>Welcome massage goes to here</p>
                </div>
            </div>
        </div>
    </section>
    <section id="about-us" class="space-top bg-color v2">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 no-padding">

                    @foreach ($errors->all() as $message)
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                    @endforeach

                    @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                    @endif
                        {!! Form::open(['route' => 'join','enctype'=>'multipart/form-data','method'=>'post']) !!}
                        {{csrf_field()}}
                        <div class="col-sm-12">
                            <div class="form-group col-sm-6">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" id="email" placeholder="Your email" required>
                            </div>


                            <div class="form-group col-sm-6">
                                <label for="email1">Password</label>
                                <input type="text" class="form-control" name="password" id="email1" placeholder="password" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="designation">Title/Designation</label>
                                <input type="text" class="form-control" name="designation" id="designation" placeholder="Title/Designation" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="specialty">Speciality</label>
                                <select class="form-control " name="speciality" required>

                                    <option value="Cardiology">Cardiology</option>
                                    <option value="Dental">Dental</option>
                                    <option value="Dermatology">Dermatology</option>
                                    <option value="Gastroenterology">Gastroenterology</option>
                                    <option value="Medicine">Medicine</option>
                                    <option value="Physiotherapy">Physiotherapy</option>
                                    <option value="Orthopaedics">Orthopaedics</option>




                            </div>
                            <div class="form-group col-sm-6">
                                <label for="phone">Phoneggggggggggg</label>
                                <input type="text" class="form-control"  name="phone" id="phone" required>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="gender">Gender</label>
                                <select class="form-control" name="gender" required>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>





                            <div class="form-group col-sm-6">
                                <label for="phone">Phone</label>
                                <input type="text" class="form-control"  name="phone" id="phone" required>
                            </div>


                            <div class="form-group col-sm-6">
                                <label for="location">Address/Location</label>
                                <input type="text" class="form-control"  name="location" id="location" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="reg_no">BMDC Reg. No</label>
                                <input type="text" class="form-control"  name="bmdc" id="reg_no" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="nid">National ID No.</label>
                                <input type="text" class="form-control"  name="nid" id="nid" required>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="reg_no">Degrees/Qualification</label>
                                <input type="text" class="form-control"  name="degree" id="degree">
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="experience">Years of experience</label>
                                <input type="text" class="form-control"  name="experience" id="experience" required>
                            </div>


                            <div class="form-group col-sm-6">
                                <label for="experience">Hospitals:</label>
                                @foreach($data as $d)
                                    <input type="checkbox" name="hospital[]" id="{{$d->id}}" value={{$d->id}}>
                                    <label for="{{$d->id}}">{{$d->name}}</label>
                                @endforeach
                            </div>



                            <div class="form-group col-sm-6">
                                <label for="interested-at">Interested at: </label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Interview">Interview</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Tele-treatment">Tele-treatment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Online-appointment">Online appointment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Video-conference-treatment">Video conference treatment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Writing-at-doctor-blog">Writing at doctor blog</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Corporate-patient-treatment">Corporate patient treatment</label>
                                <label class="checkbox-inline"><input type="checkbox" name="interest[]" value="Medicine-information">Medicine information</label>
                            </div>

                            <div class="form-group col-sm-6">
                                <label for="image">Your Image</label>
                                <input type="file" class="form-control"  name="image" id="image" required>
                            </div>
                        </div>


                        <div class="form-group col-sm-12">
                            <h4>Terms and conditions</h4>
                           <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur delectus,
                               dolore dolores facere laboriosam laudantium maiores
                               possimus reiciendis sint soluta tenetur vero. Aperiam debitis hic illum laboriosam maiores minus rem!</p>
                        </div>


                        <div class="form-group col-sm-12">
                            <div class="button-item pull-right">
                                <label class="checkbox-inline"><input type="checkbox" id="termsaccept" name="termsaccept" value="">I accept the term and conditions.</label>
                               <button class="btn radius-4x" id="submitbtn">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="col-sm-4 col-sm-offset-1 about-block text-right">
                    <img src="{{ asset('front-end/assets/images/why.png') }}" alt="Columba">
                </div>
            </div>
        </div>
    </section>

@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $("#submitbtn").attr("disabled", "disabled");
        });

        $(document).on('click','#termsaccept',function () {
            if ($('#termsaccept').is(':checked')) {
                $('#termsaccept').val("yes");
                $("#submitbtn").attr("disabled", false);
            }else{
                $('#termsaccept').val("not");
                $("#submitbtn").attr("disabled", true);
            }
        })
    </script>

@endsection
@extends('layouts.master')

@section('content')
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 bread-block text-center">
                    <h2>Page Title</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
            </div>
        </div>
    </section>
    <!--action-->
    <section class="space">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p>Phasellus ornare sagittis purus id accumsan. Aenean a tempus justo. Quisque sollicitudin bibendum maximus. Donec euismod felis et libero consectetur blandit. Integer et fringilla purus. Phasellus sed ante eget est porttitor imperdiet. Aenean ultrices massa non purus suscipit, suscipit sodales sapien placerat. Ut ut porttitor orci, efficitur euismod dui. Vivamus bibendum lectus hendrerit felis commodo, vitae feugiat massa fringilla. Phasellus condimentum, sem nec aliquam vestibulum, massa lacus lobortis erat, sit amet lobortis quam massa quis quam. Suspendisse a sem quis eros mattis molestie. Vivamus nec lorem sodales, hendrerit nulla varius, condimentum nisi.
                    </p>
                    <p>Cras semper lorem id tellus varius, a ullamcorper arcu luctus. Integer ullamcorper sapien placerat consectetur egestas. Integer elementum eu arcu ut rutrum. Curabitur mauris tortor, tincidunt vitae iaculis id, maximus in massa. Suspendisse elementum ante eu lorem cursus egestas. Ut a mi dignissim, lacinia massa eu, congue ipsum. Phasellus sed rutrum lacus. Maecenas pulvinar lacus id luctus viverra. Mauris in accumsan diam, ut imperdiet orci. Maecenas sodales velit id quam auctor, eget viverra felis dictum. Praesent malesuada id risus non tempor. Curabitur consectetur nisi lacus, eu molestie lorem blandit at. Fusce nulla lacus, consequat vel tempus sed, tincidunt vitae tellus. Nulla vestibulum vehicula faucibus. Mauris id justo eu est fringilla efficitur quis eu orci.
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
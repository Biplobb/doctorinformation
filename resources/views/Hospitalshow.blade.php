@extends('layouts.master')
@section('title', 'Deshidoctor | Hospital')
@section('content')

    @foreach($Hospital as $Hospital)

        <section id="breadcrumb" class="light-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 bread-block animate-in move-up animated">
                        <h2>{{ $Hospital->name }}</h2>
                        <p>{{ $Hospital->location }}</p>
                    </div>
                </div>
            </div>
        </section>
        <section id="single-service" class="space">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 service-block">
                        <div class="row">
                            <div class="col-md-12">
                                <img class="img-responsive" src="{{ asset('image/hospital-photo/'.$Hospital->image) }}" alt="{{ $Hospital->name }}">
                                <br />
                                <br />


                                <div id="sub-menu" class="tabs">
                                    <ul class="nav nav-tabs">
                                        <li class=""><a  href="#hospital-overview">Overview</a></li>
                                        <li class=""><a href="#services">Services</a></li>
                                        <li class=""><a href="#hospital-doctor">Doctors</a></li>
                                        <li class=""><a  href="#contacts">Contacts</a></li>
                                    </ul>
                                    <div class="panel-group accordion">
                                        <div class="panel panel-default" id="hospital-overview">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a>
                                                        <i class="icofont icofont-sound-wave-alt" aria-hidden="true"></i>Overview
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse">
                                                <div class="panel-body">{{ $Hospital->about }}</div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default" id="services">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a>
                                                        <i class="icofont icofont-ui-settings" aria-hidden="true"></i>Services
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse">
                                                <div class="panel-body">{{ $Hospital->service }}</div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default" id="hospital-doctor">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a href="javaScript:void(0)">
                                                        <i class="icofont icofont-doctor" aria-hidden="true"></i>Doctors
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse" onClick="event.stopPropagation();">
                                                <section class="v5 bg-color small-space">
                                                    <div class="row">
                                                        @foreach($doctorprofile as $doctorprofile)
                                                            <a href="{{ route('doctor_profile',['id'=>$doctorprofile->id])  }}">

                                                                <div class="col-sm-3 team-block" >

                                                                    <div class="inner">


                                                                            <img  width="200px" height="200px"  src="{{ asset('image/doctor-photo/'.$doctorprofile->dphoto) }}" alt="Columba">

                                                                    </div>
                                                                    <div class="team-text no-padding">
                                                                        <div class="name">{{   $doctorprofile->name }} </div>
                                                                        <div class="experience">{{ $doctorprofile->doctor_address }}</div>
                                                                    </div>

                                                                </div>
                                                            </a>


                                                        @endforeach
                                                    </div>

                                                    <div class="row">
                                                        <a href="{{ url('allhospitaldoctor') }}" class="btn btn-small">More Doctors</a>
                                                    </div>
                                                </section>
                                            </div>

                                        </div>
                                        <div class="panel panel-default" id="hospital-review">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a>
                                                        <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>Reviews
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse">
                                                <div class="panel-body">{{ $Hospital->review }}</div>

                                                <div class="panel-body">

                                                </div>

                                            </div>
                                        </div>
                                        <div class="panel panel-default" id="contacts">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a>
                                                        <i class="icofont icofont-coffee-cup" aria-hidden="true"></i>Contacts
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="panel-collapse">

                                                <div class="panel-body">{{ $Hospital->contact }}</div>

                                                <div class="panel-body">

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <address>
                                                                <p>
                                                                    {{ $Hospital->location }}
                                                                </p>
                                                            </address>
                                                        </div>
                                                        <div class="col-md-8">


                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d4342.184732931857!2d90.42134112957606!3d23.77134672105723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1518095272267" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                        <aside class="col-sm-3">
                            <div class="widget contact">
                                <h1>20% Off</h1>
                                <p>Get Discount through Deshidoctor</p>
                                <a href="#" class="btn">Contact Us</a>
                            </div>
                            <div class="widget testimonials">
                                <div class="inner">
                                    <div class="icon">“</div>
                                    <h1>About</h1>
                                    <p>Before selecting a Financial Adviser we had discussed our finances with a few other advisers, but from our first meeting with Financial solutions it was apparent that their depth of knowledge far exceeded that of others we had spoken with.</p>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            @endforeach
        </section>
@endsection

@section('script')
    <script>

        $(document).scroll(function () {
            var top = $(document).scrollTop();
            $('#show').html(top);
            if(top>= 738){
                $('#sub-menu .nav').addClass('sub-menu-fixed');
            }else {
                $('#sub-menu .nav').removeClass('sub-menu-fixed');
            }
        });
        $(document).ready(function(){
            $('a[href*="#"]')
                .not('[href="#"]')
                .not('[href="#0"]')
                .click(function(event) {
                    if (
                        location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                        location.hostname == this.hostname
                    ) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            event.preventDefault();
                            $('html, body').animate({
                                scrollTop: target.offset().top - 145
                            }, 300, function() {
                                var $target = $(target);
                                $target.focus();
                                if ($target.is(":focus")) {
                                    return false;
                                } else {
                                    $target.attr('tabindex', '-1');
                                    $target.focus();
                                };
                            });
                        }
                    }
                });
        });
    </script>
@endsection
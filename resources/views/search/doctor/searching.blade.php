
@extends('search.doctor.layouts.master')




@section('content')

    <section class="content">
        <H1 align="center">Please search any Doctor</H1>
        <div class="container-fluid">
            <div class="block-header">
                <div class="titless" style="overflow:hidden">
                    <div class="title" align="center"  >
                        {!! Form::open(['route' => 'searchPDFdoctor','method'=>'post','class'=>'class_name']) !!}
                        {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}

                        <button class="clickable" id="clickable">.        search    .</button>


                        {!! Form::close() !!}
                    </div>
                    <center>
                    <div class="btn-group">
                        <button type="button" id="forms" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-user" aria-hidden="true"></i> Message
                        </button>

                    </div>
                    </center>
                </div>
                <h2>Doctor all information</h2>
                <small class="text-muted">Patient information  </small>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Doctor Information</h2>
                        </div>
                        <div class="body">

                            @if($errors->any())
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $er)
                                        {{$er}}
                                    @endforeach
                                </div>
                            @endif


                            @foreach($patient as $patient)
                                <div class="row clearfix">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Doctor Name:</h3>
                                                <h4>{{$patient->name}}</h4>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Doctor designation:</h3>
                                                <h4>{{$patient->designation}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Doctor Address:</h3>
                                                <h4>{{$patient->location}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="row clearfix">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Experience:</h3>
                                                <h4>{{$patient->experience}}</h4>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Doctor speciality :</h3>
                                                <h4>{{$patient->speciality}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Gender:</h3>
                                                <h4>{{$patient->gender}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">


                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Doctor Image:</h3>
                                                <img height="200" width="150" src="{{ asset('image/doctor-photo/'.$patient->image)}}" alt="">


                                            </div>
                                        </div>
                                    </div>
                                </div>




                        </div>


                        @endforeach








                    </div>




                </div>
            </div>
        </div>

        </div>


        </div>
    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


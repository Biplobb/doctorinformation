<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> Doctor Information</title>


    <!-- Custom Css -->
    <link href="{{ asset('back-end/assets/css/main.css') }}" rel="stylesheet" type="text/css">

    <link rel="icon" href="{{ asset('back-end/favicon.ico')}}" type="image/x-icon"><!-- Favicon-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{ asset('back-end/assets/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

    <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('back-end/assets/css/themes/all-themes.css') }}" rel="stylesheet" />

    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>


{{--These are for Book Appoinment Form--}}

<!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('back-end/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <!-- Wait Me Css -->
    <link href="{{ asset('back-end/assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('back-end/assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    {{--This is for Appoinment Doctor Schedule Form--}}
    <link rel="stylesheet" href="{{ asset('back-end/assets/plugins/fullcalendar/fullcalendar.min.css')}}">


    <link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css'>



</head>

<body class="theme-cyan">

<div class="overlay"></div>

<nav class="navbar clearHeader">
    <div class="container-fluid">
        <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" href="{{asset(url('admin'))}}">Deshi Doctor</a> </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="admin-image"> <img src="{{ asset('back-end/assets/images/random-avatar7.jpg')}}" alt=""> </div>
            <div class="admin-action-info"> <span>Welcome</span>



                <ul>
                    <li><a data-placement="bottom" title="Full Screen" href="{{route('homelogout')}}" ><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
                </ul>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">Your Options</li>

                <li class="active open"><a href="{{asset(route('homelogout'))}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>




                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-add"></i><span>Patient </span> </a>
                    <ul class="ml-menu">


                        <li><a href="{{URL::to('patientallinformation/'. $re->id  ) }}">All Information</a></li>
                        <li><a href="{{URL::to('addpatientinformation/'. $re->id  ) }}">Add Information</a></li>
                    </ul>
                </li>

                <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-calendar-check"></i><span>Message</span> </a>
                    <ul class="ml-menu">
                        <li><a href="{{route('allpatientmessage')}}">All Message</a></li>

                    </ul>
                </li>






            </ul>
            </li>












            {{-- @endif--}}
            </ul>
        </div>
        <!-- #Menu -->
    </aside>
</section>


{{--Yield Section--}}

@yield('content')

<div class="color-bg"></div>
<!-- Jquery Core Js -->

<script src="{{ asset('back-end/assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('back-end/assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->

<script src="{{ asset('back-end/assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

<script src="{{ asset('back-end/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script> <!-- Sparkline Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/chartjs/Chart.bundle.min.js')}}"></script> <!-- Chart Plugins Js -->

<script src="{{ asset('back-end/assets/js/morphing.js')}}"></script><!-- Custom Js -->
<script src="{{ asset('back-end/assets/js/pages/charts/sparkline.min.js')}}"></script>
<script src="{{ asset('back-end/assets/js/pages/charts/chartjs.min.js')}}"></script>
<script src="{{ asset('back-end/assets/js/pages/index.js')}}"></script>

{{--These are for Book Appoinment Form--}}

<script src="{{ asset('back-end/assets/plugins/autosize/autosize.js')}}"></script> <!-- Autosize Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

<script src="{{ asset('back-end/assets/js/pages/forms/basic-form-elements.js')}}"></script>

{{--These are for Appoinment Doctor Schedule Form--}}

<script src="{{ asset('back-end/assets/bundles/fullcalendarscripts.bundle.js')}}"></script><!--/ calender javascripts -->
<script src="{{ asset('back-end/assets/js/pages/calendar/calendar.js')}}"></script>


<script>
    setTimeout(function() {
        $('.alert').fadeOut('slow');
    }, 3000);

</script>


<script>
    CKEDITOR.replace( 'blog_post' );
</script>
<script>
    CKEDITOR.replace( 'service' );
</script>

<script>
    CKEDITOR.replace( 'news_post' );
</script>


@yield('script')

</body>

</html>
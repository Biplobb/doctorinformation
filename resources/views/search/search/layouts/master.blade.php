<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title> Doctor Information</title>


    <!-- Custom Css -->
    <link href="{{ asset('back-end/assets/css/main.css') }}" rel="stylesheet" type="text/css">

    <link rel="icon" href="{{ asset('back-end/favicon.ico')}}" type="image/x-icon"><!-- Favicon-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{ asset('back-end/assets/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

    <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('back-end/assets/css/themes/all-themes.css') }}" rel="stylesheet" />

    <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>


{{--These are for Book Appoinment Form--}}

<!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('back-end/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <!-- Wait Me Css -->
    <link href="{{ asset('back-end/assets/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
    <!-- Bootstrap Select Css -->
    <link href="{{ asset('back-end/assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    {{--This is for Appoinment Doctor Schedule Form--}}
    <link rel="stylesheet" href="{{ asset('back-end/assets/plugins/fullcalendar/fullcalendar.min.css')}}">


    <link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css'>


    {{------------------------------------js-----------------------------}}


    {{--<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>--}}

</head>

<body class="theme-cyan">
<!-- Page Loader -->
{{--<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-cyan">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>--}}
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- #Float icon -->

<!-- #Float icon -->
<!-- Morphing Search  -->

<!-- Top Bar -->
<nav class="navbar clearHeader">
    <div class="container-fluid">
        <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" href="{{asset(url('admin'))}}"> Doctor Information</a> </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
      <div class="user-info">
            <div class="admin-image"> <img src="{{ asset('back-end/assets/images/random-avatar7.jpg')}}" alt=""> </div>
          <div class="admin-action-info"> <span>Welcome</span>

            {{--  <h3>{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->name}}</h3>--}}

              <ul>
                  <li><a data-placement="bottom" title="Full Screen" href="{{route('homelogout')}}" ><i class="fa fa-power-off" aria-hidden="true"></i> Logout</a></li>
              </ul>
          </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">Your Options</li>

                <li class="active open"><a href="{{asset(route('homelogout'))}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>


              {{--  @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->hasRole('supportadmin'))--}}

                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-add"></i><span>Doctors </span> </a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('doctorindex')}}">All Doctors</a></li>
                            <li><a href="{{ route('adddoctor')}}">Add Doctor</a></li>

                            <li><a href="{{route('viewdoctorschedule')}}">View doctor schedule</a></li>
                            <li><a href="{{route('addorremovechamber')}}">Add or Remove Chamber</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-calendar-check"></i><span>Appointment</span> </a>
                        <ul class="ml-menu">
                            <li><a href="{{route('allappointment')}}">All appointments</a></li>
                            <li><a href="{{route('appointmenttrash')}}">Appointments Trash</a></li>
                        </ul>
                    </li>



                   {{-- <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-add"></i><span>Hospital service</span> </a>
                        <ul class="ml-menu">
                            <li><a href="{{ route('hospitalcreate')}}">Add Hospital</a></li>
                            <li><a href="{{ route('hospitalindex')}}">All Hospital</a></li>
                        </ul>
                    </li>--}}



                        </ul>
                    </li>












             {{-- @endif--}}
            </ul>
        </div>
        <!-- #Menu -->
    </aside>
</section>


{{--Yield Section--}}

@yield('content')

<div class="color-bg"></div>
<!-- Jquery Core Js -->

<script src="{{ asset('back-end/assets/bundles/libscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->
<script src="{{ asset('back-end/assets/bundles/mainscripts.bundle.js')}}"></script><!-- Custom Js -->

<script src="{{ asset('back-end/assets/bundles/vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

<script src="{{ asset('back-end/assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script> <!-- Sparkline Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/chartjs/Chart.bundle.min.js')}}"></script> <!-- Chart Plugins Js -->

<script src="{{ asset('back-end/assets/js/morphing.js')}}"></script><!-- Custom Js -->
<script src="{{ asset('back-end/assets/js/pages/charts/sparkline.min.js')}}"></script>
<script src="{{ asset('back-end/assets/js/pages/charts/chartjs.min.js')}}"></script>
<script src="{{ asset('back-end/assets/js/pages/index.js')}}"></script>

{{--These are for Book Appoinment Form--}}

<script src="{{ asset('back-end/assets/plugins/autosize/autosize.js')}}"></script> <!-- Autosize Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/momentjs/moment.js')}}"></script> <!-- Moment Plugin Js -->
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ asset('back-end/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js')}}"></script>

<script src="{{ asset('back-end/assets/js/pages/forms/basic-form-elements.js')}}"></script>

{{--These are for Appoinment Doctor Schedule Form--}}

<script src="{{ asset('back-end/assets/bundles/fullcalendarscripts.bundle.js')}}"></script><!--/ calender javascripts -->
<script src="{{ asset('back-end/assets/js/pages/calendar/calendar.js')}}"></script>


<script>
    setTimeout(function() {
        $('.alert').fadeOut('slow');
    }, 3000);

</script>


<script>
    CKEDITOR.replace( 'blog_post' );
</script>
<script>
    CKEDITOR.replace( 'service' );
</script>

<script>
    CKEDITOR.replace( 'news_post' );
</script>


@yield('script')

</body>

</html>
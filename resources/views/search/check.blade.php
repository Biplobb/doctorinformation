
@extends('search.search.layouts.master')




@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Patient all information</h2>
                <small class="text-muted">Patient information  </small>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Profile Information</h2>
                        </div>
                        <div class="body">

                            @if($errors->any())
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $er)
                                        {{$er}}
                                    @endforeach
                                </div>
                            @endif


                            @foreach($patient as $patient)
                                <div class="row clearfix">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient Name:</h3>
                                                <h4>{{$patient->name}}</h4>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient Present Address:</h3>
                                                <h4>{{$patient->present}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient permanent Address:</h3>
                                                <h4>{{$patient->permanent}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="row clearfix">

                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient contact number:</h3>
                                                <h4>{{$patient->contact}}</h4>

                                            </div>
                                        </div>
                                    </div>







                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient occupation :</h3>
                                                <h4>{{$patient->occupation}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Gender:</h3>
                                                <h4>{{$patient->gender}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">


                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <h3>Patient Image:</h3>
                                                <img height="200" width="150" src="{{ asset('image/patient/'.$patient->image)}}" alt="">


                                            </div>
                                        </div>
                                    </div>
                                </div>




                        </div>
                        <center>
                        <div class="row clearfix">
                            <div class="col-xs-12 ">

                                <h3>Other information for connect with patient</h3>
                                {!! Form::open(['url' => 'verificationlogin','enctype'=>'multipart/form-data','method'=>'post']) !!}



                                <input type="hidden" name="email" value="{{  $patient->email }}">
                               <h4> Verification code: </h4>
                                <input type="text" name="code" placeholder=".  verification code">
                                <h1>  <a href=""><button type="submit">submit</button></a></h1>
                                {!! Form::close() !!}


                            </div>
                        </div>
                        </center>

                        @endforeach








                    </div>




                </div>
            </div>
        </div>

        </div>


        </div>
    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


@extends('layouts.master')
@section('title', 'Doctor and patient | Home')
@section('content')
    <!--Banner Content
v5 center light-overlay
style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0,0,0,.5);"
    -->
    <section id="main-banner" class="v5 center light-overlay">

        <div class="container" >
            <div class="row">
                <div class="banner-table">
                    <div class="banner-block text-center">
                        <div class="row searcharea">
                            <div class="col-md-8 col-md-offset-2" style="margin-bott">

                              {{--  <div class="titless" style="overflow:hidden">
                                    <div class="title" align="center"  >
                                        {!! Form::open(['route' => 'searchPDF','method'=>'post','class'=>'class_name']) !!}
                                        {!! Form::text('title', null, array('placeholder' => '        Search Text','id'=>'search_text')) !!}

                                        <button class="clickable" id="clickable">.        search    .</button>

                                        --}}{{--{!! Form::submit('search') !!}--}}{{--
                                        {!! Form::close() !!}
                                    </div>
                                </div>--}}
                                {!! Form::open(['route' => 'searchPDFmain','method'=>'post','id','=',"search"]) !!}
                                {{--<form id="search" method="post" onsubmit="return validdata()">--}}
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="gutter">
                                               <select name="area" id="area" class="form-control" onchange="enable_lookingfor()">
                                                    <option value="">Search Area</option>
                                                    <option value="Dhaka">Dhaka</option>
                                                    <option value="Narayonganj">Narayganj</option>
                                                    <option value="Savar">Savar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="gutter">
                                                <select name="lookingfor" id="lookingfor" class="form-control" onchange="enable_keyword()">
                                                    <option value="">Looking for</option>
                                                    <option value="doctor">Doctor</option>
                                                    <option value="hospital">Hospital</option>
                                                    <option value="diagnostic">Diagnostics</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="gutter">
                                                <input id="keyword" class="form-control" name="keyword" type="text" placeholder="Type Keyword"  autocomplete="off">
                                                <input type="hidden" name="idnt" id="idnt">
                                                <div class="suggestion">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="gutter">
                                                <div class="search-icon">
                                                    <button id="dosearch"> <span class="fa fa-search" ></span></button>
                                                    {{--<button id="dosearch"> <span class="fa fa-search" ></span></button>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <h2>Your Health We Care</h2>
                            </div>
                        </div>
                        <ul class="services">
                            <li>
                                <a href="{{route('alldoctors')}}">
                                    <i class="icofont icofont-doctor"></i>
                                    <p>Doctor</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/hospitals')}}">
                                    <i class="icofont icofont-hospital"></i>
                                    <p>Hospital</p>
                                </a>
                            </li>


                            <li>
                                <a href="{{ url('diagnostics') }}">
                                    <i class="icofont icofont-read-book"></i>
                                    <p>Diagnostics</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Offers Area-->

    <!--Testimonials-->
    <section id="testimonials" class="space v1">
        <div class="container">
            <div class="col-sm-6 col-sm-offset-3 text-center main-heading animate-in move-up">
                <h2>Our group members and supervisor</h2>
                <p></p>
            </div>
            <div class="row">
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img height="200" width="150" src="{{ asset('front-end/assets/images/naz.jpg') }}" alt="Columba" class="user-image">

                        </div>
                        <div class="testi-text">

                            <div class="name">Mr. Naziour Rahaman</div>
                            <div class="designation">Lecturer  <br /><br /></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img height="200" width="150" src="{{ asset('front-end/assets/images/mk.jpg') }}" alt="Columba" class="user-image">

                        </div>
                        <div class="testi-text">

                            <div class="name">Md Kamrul Hassan</div>
                            <div class="designation">I am php developer.<br /><br /></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img height="200" width="150" src="{{ asset('front-end/assets/images/rakib.jpg') }}" alt="Columba" class="user-image">

                        </div>
                        <div class="testi-text">

                            <div class="name">Md Rakib Hassan</div>
                            <div class="designation">I am php developer.<br /><br /></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 testi-block">
                    <div class="inner">
                        <div class="testi-image text-center">
                            <img height="200" width="150" src="{{ asset('front-end/assets/images/rabiul.jpg') }}" alt="Columba" class="user-image">

                        </div>
                        <div class="testi-text">

                            <div class="name">Md Rabiul Auwal Masum</div>
                            <div class="designation">I am php developer.<br /><br /></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-center load-more">
                    <a href="javaScript:void(0)" class="btn btn-small"></a>
                </div>
            </div>
        </div>
    </section>
    <!--Client Logo-->
  
@endsection

@section('script')
    <script>
        var token='{{\Illuminate\Support\Facades\Session::token()}}';
    </script>
    <script src="{{asset('front-end/assets/js/myjs.js')}}"></script>
@endsection
@extends('layouts.master')

@section('content')
    <!--Bread Crumb-->
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in fade-in">
                    <h2>Our Doctors 02</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
                <div class="col-sm-6 bread-block text-right animate-in fade-in">
                    <a href="#" class="btn">Book appointment</a>
                </div>
            </div>
        </div>
    </section>
    <!--Meet Our Team-->
    <section id="our-team" class="space v4">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/doctor.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/doctor2.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/doctor3.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/doctor4.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-1.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-1.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-1.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in fade-in">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-1.jpg')}}" alt="Columba" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--action-->
    <section class="action-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 ">
                    <div class="col-sm-8 animate-in fade-in">
                        <h2>Planning A Visit To The Doctor</h2>
                    </div>
                    <div class="col-sm-4 animate-in fade-in">
                        <a href="#" class="btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('admin.layouts.master')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Hospital </h2>
            <small class="text-muted">Hospital information submit form</small>
        </div>
        <div class="row clearfix">
            {!! Form::open(['url' => '/admin/hospital/store','enctype'=>'multipart/form-data','method'=>'post']) !!}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Hospital Information</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Hospital Name" name="name">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="location " name="location">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Contact " name="contact">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="review " name="review">
                                    </div>
                                </div>
                            </div>


                        </div>
                       <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="about " name="about">
                                    </div>
                                </div>
                            </div>


                        </div> <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="exampleInputName1">Upload Image</label>
                                        <input type="file" name="photo" >
                                    </div>
                                </div>
                            </div>


                        </div>


                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select name="service[]" multiple="" class="ui fluid dropdown">
                                            <option value="">service</option>
                                            <option value="operation">operation</option>
                                            <option value="medicine">medicine</option>

                                        </select>
                                    </div>
                                </div>
                            </div>






                            <div class="col-sm-6 col-xs-12">

                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


    </div>

</section>
@endsection

{{--@include('admin.layouts.footer')--}}


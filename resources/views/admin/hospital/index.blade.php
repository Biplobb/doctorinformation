{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <h1>HOspital Table</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">location</th>
                <th scope="col">service</th>
                <th scope="col">about</th>
                <th scope="col">photo</th>
                <th scope="col">review</th>
                <th scope="col">phone</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($Hospital as $hospital_service)
                <tr>
                    <th scope="row">{{ $hospital_service->id }}</th>
                    <td>{{ $hospital_service->name }}</td>
                    <td>{{ $hospital_service->location }}</td>
                    <td>{{ $hospital_service->service }}</td>
                    <td>{{ $hospital_service->about }}</td>
                    <td>{{ $hospital_service->photo }}</td>
                    <td>{{ $hospital_service->review }}</td>
                    <td>{{ $hospital_service->contact }}</td>

                    <td>
                        <a href="{{ route('hospitaledit',['id'=>$hospital_service->id]) }}">Edit</a>
                        <a href="{{ route('hospitaldelete',['id'=>$hospital_service->id]) }}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <h1>Tests Order Table</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">address</th>
                <th scope="col">phone</th>

                <th scope="col">price</th>
                <th scope="col">Tests</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($diagnostic_center as $diagnostic_center)
                <tr>
                    <th scope="row">{{ $diagnostic_center->testorderid }}</th>
                    <td>{{ $diagnostic_center->testordername }}</td>
                    <td>{{ $diagnostic_center->testorderaddress }}</td>
                    <td>{{ $diagnostic_center->testorderphone }}</td>

                    <td>{{ $diagnostic_center->testsprice }}</td>
                    <td>{{ $diagnostic_center->testsname }}</td>

                    <td>

                        <a href="{{ route('testdestroy',['id'=>$diagnostic_center->testorderid]) }}">DELETE</a>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

@extends('admin.layouts.master')
@section('content')

    <section class="content">
    <div class="container-fluid">

    <div class="row clearfix">
            <div class="header">
                <h2>Add new Amdin</h2>
            </div>

        {!! Form::open(['route' => 'addnewsupportadmin','enctype'=>'multipart/form-data','method'=>'post','name'=>'addadmin']) !!}
        {{csrf_field()}}
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">

                @if(session('success'))
                    <div class="alert alert-success">{{session('success')}}</div>
                @endif

                    @if(session('warning'))
                        <div class="alert alert-danger">{{session('warning')}}</div>
                    @endif

                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Full Name" name="name" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Email" name="email" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="Phone"  name="phone" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row clearfix">

                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" placeholder="NID no." name="nid">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" class="form-control" placeholder="Password" name="password" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 col-xs-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="exampleInputName1">Upload Image</label>
                                    <input type="file" name="photo">
                                </div>
                            </div>
                        </div>



                    </div>



                    <div class="row clearfix">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        </div>
    </div>
    </div>
    </section>
@endsection
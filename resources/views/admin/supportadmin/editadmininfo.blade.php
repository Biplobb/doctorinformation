{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Edit Aupport Admin info</h2>
            </div>

            <div class="row clearfix">
                {!! Form::open(['route'=>'updateadmininfo','method'=>'patch','files'=>true]) !!}
{{--                <form action="{{route('updateadmininfo')}}" method="post">--}}
                    {{csrf_field()}}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" value="{{$data->admin_id}}" name="admin_id">
                    <div class="card">

                        @if(session('updated'))
                            <div class="alert alert-success">
                                {{session('updated')}}
                            </div>
                        @endif

                            @if(session('failed'))
                                <div class="alert alert-danger">
                                    {{session('failed')}}
                                </div>
                            @endif

                        <div class="header">
                            <h2>Profile Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{$data->name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" class="form-control" required placeholder="Email" name="email" value="{{$data->email}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" required placeholder="Phone"  name="phone" value="{{$data->phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <label for="">Define role</label>

                                        <div class="form-line">
                                            <input type="radio" id="contactChoice2" name="role_id" value="6" {{ $data->role_name=='superadmin'? "checked='checked'":''}}>
                                            <label for="contactChoice2">Super admin *</label>
                                        </div>

                                        <div class="form-line">
                                            <input type="radio" id="contactChoice3" name="role_id" value="5" {{ $data->role_name=='supportadmin'? "checked='checked'":''}}>
                                            <label for="contactChoice3">Support admin</label>
                                        </div>

                                </div>

                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">Update</button>
                                </div>
                            
                            </div>

                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


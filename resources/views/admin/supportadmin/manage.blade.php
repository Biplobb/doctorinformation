{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>All Doctors</h2>
                <small class="text-muted">Welcome to Swift application</small>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">

                            @if(session('updatesuccess'))
                                <div class="alert alert-success">
                                    {{session('updatesuccess')}}
                                </div>
                            @endif

                            @if(session('ddeleted'))
                                <div class="alert alert-info">
                                    {{session('ddeleted')}}
                                </div>
                            @endif

                            @if(session('deletesuccess'))
                                <div class="alert alert-danger">
                                    {{session('deletesuccess')}}
                                </div>
                            @endif

                            <div class="member-card verified">
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Edit</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Delete</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Block</a></li>
                                        </ul>
                                    </li>
                                </ul>

                                <div class="">

                                    <table class="table">
                                        <thead>


                                        <tr>
                                            <th scope="col">Sl.</th>
                                            <th scope="col">name</th>
                                            <th scope="col">phone</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">NID</th>
                                            <th scope="col">photo</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php

                                            $i=1;

                                        @endphp
                                        @foreach($data as $d)
                                            <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $d->name }}</td>
                                                <td>{{ $d->phone }}</td>
                                                <td>{{ $d->email }}</td>
                                                <td>{{ $d->nid }}</td>
                                                <td>{{Html::image('image/admin-photo/'.$d->photo,'photo',['style'=>'width:90px;height:90px'])}}</td>
                                                <td>
                                                    <a href="{{route('editadmininfo',[$d->admin_id])}}" class="btn  btn-raised btn-info waves-effect">Edit</a>
                                                    <a href="{{route('deleteAdminData',[$d->admin_id])}}" class="btn  btn-raised btn-danger waves-effect">Delete</a>
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

@extends('admin.layouts.master')
@section('content')

<section class="content">

    <div class="container-fluid">
        <div class="block-header">
            <h2>Add News</h2>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">


                    <form action="{{route('newsPost')}}" method="post" enctype="multipart/form-data">
                        {{--{!! Form::open(['url' => 'admin/news/create/','enctype'=>'multipart/form-data','method'=>'post']) !!}--}}

                        {{csrf_field()}}

                        <div class="body">
                            <div class="row clearfix">





                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder=" Title"  name="news_title">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Author" name="author">
                                        </div>
                                    </div>
                                </div>
                            </div>




                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="form-line">
                                    <textarea rows="4" class="form-control no-resize" placeholder="Type your news post here....." name="news_post"></textarea>
                                </div>
                            </div>
                        </div>




                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-control">

                                        <label for="Image">Insert image</label>
                                        <input type="file" name="image" >
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                            <button type="submit" class="btn btn-raised">Cancel</button>
                        </div>

                    </form>

                </div>


            </div>
        </div>
    </div>
    </div>


    {{--{!! Form::close() !!}--}}
</section>
@endsection


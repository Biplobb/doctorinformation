@extends('admin.layouts.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Blog List</h2>
            </div>


            @if(session()->has('message'))
                {{ session('message') }}
            @endif
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>


                            <th> id</th>
                            <th>News title</th>
                            <th>Author</th>
                            <th>News post</th>
                            <th>News Image</th>
                            <th>Action</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach($news as $n)
                            <tr>
                                <td>{{ $n->id }}</td>
                                <td>{{ $n->news_title }} </td>
                                <td>{{ $n->author }}</td>
                                <td>{{ $n->news_post }}</td>
                                <td><img src="{{asset('image/news-photo/'.$n->image)}}" width="400px",height="400px"></td>

                                <td>
                                    <a href="{{route('showNews',['id'=>$n->id])}}">Show</a>
                                    <a href="{{route('editNews',['id'=>$n->id])}}">Edit</a>
                                    <a href="{{route('destroynews',['id'=>$n->id])}}">Delete</a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
        </div>
        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}
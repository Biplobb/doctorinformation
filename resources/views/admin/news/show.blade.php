
{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')






@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>News Details </h2>
                <small class="text-muted">Blog information form</small>
            </div>
            <div class="row clearfix">
                {!! Form::open(['url' => '/admin/news/store','enctype'=>'multipart/form-data','method'=>'post']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>News Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">



                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">id</th>
                                            <th scope="col">News title</th>
                                            <th scope="col">Author</th>
                                            <th scope="col">News post</th>
                                            <th scope="col">News image</th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>


                                            <th scope="row"> {{ $news->id }}</th>
                                            <td>{{ $news->news_title  }}</td>
                                            <td>{{ $news->author }}</td>
                                            <td>{{ $news->news_post }}</td>
                                            <td><img src="{{asset('image/news-photo/'.$news->image)}}" width="400px",height="400px"></td>

                                        </tr>

                                        </tbody>
                                    </table>

                                </div>




                                <div class="col-sm-6 col-xs-12">

                                </div>
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                    <button type="submit" class="btn btn-raised">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}



























{{--<section class="content profile-page">--}}
{{--<div class="container-fluid">--}}
{{--<div class="row clearfix">--}}
{{--<div class="col-md-12 p-l-0 p-r-0">--}}

{{--</div>--}}
{{--</div>--}}
{{--<div class="row clearfix">--}}
{{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}

{{--<div class="card">--}}

{{--<div class="body">--}}
{{--<h4></h4>--}}
{{--<table class="table">--}}
{{--<thead>--}}
{{--<tr>--}}


{{--<th scope="col">id</th>--}}
{{--<th scope="col">Doctor id</th>--}}
{{--<th scope="col">title</th>--}}
{{--<th scope="col">subtitle</th>--}}
{{--<th scope="col">author</th>--}}
{{--<th scope="col">type</th>--}}
{{--<th scope="col">blog post</th>--}}
{{--<th scope="col">blog image</th>--}}

{{--</tr>--}}
{{--</thead>--}}
{{--<tbody>--}}

{{--<tr>--}}


{{--<th scope="row"> {{ $blog->id }}</th>--}}
{{--<td>{{ $blog->doctor_id  }}</td>--}}
{{--<td>{{ $blog->title }}</td>--}}
{{--<td>{{ $blog->subtitle }}</td>--}}
{{--<td>{{ $blog->author }}</td>--}}
{{--<td>{{ $blog->type }}</td>--}}
{{--<td>{{ $blog->blog_post }}</td>--}}
{{--<td><img src="{{asset('storage/image/'.$blog->image)}}"></td>--}}


{{--<td>{{Html::image('photo/'.$doctorprofile->photo,'photo',['style'=>'width:90px;height:90px'])}}</td>--}}


{{--</tr>--}}

{{--</tbody>--}}
{{--</table>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}
{{--</section>--}}



















{{--<section class="content profile-page">--}}
{{--<div class="container-fluid">--}}
{{--<div class="row clearfix">--}}
{{--<div class="col-md-12 p-l-0 p-r-0">--}}

{{--</div>--}}
{{--</div>--}}
{{--<div class="row clearfix">--}}
{{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">--}}

{{--<div class="card">--}}

{{--<div class="col-sm-12">--}}
{{--<div class="form-group">--}}
{{--<div class="form-line">--}}
{{--<textarea rows="4" class="form-control no-resize" placeholder="Type your comment here....." name="comment"></textarea>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}


{{--<div class="col-xs-12">--}}
{{--<button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>--}}

{{--</div>--}}





{{--<div class="body">--}}
{{--<h4></h4>--}}
{{--<table class="table">--}}
{{--<thead>--}}
{{--<tr>--}}
{{--<th scope="col">blog id</th>--}}
{{--<th scope="col">user id</th>--}}
{{--<th scope="col">comments</th>--}}
{{--</tr>--}}
{{--</thead>--}}

{{--</table>--}}


{{----}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}
{{--</section>--}}

















{{--@include('admin.layouts.footer')--}}


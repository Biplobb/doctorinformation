@extends('admin.layouts.master')
@section('content')

    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Edit News</h2>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">


                        {!! Form::open(['url' => '/admin/news/update/'.$news->id,'enctype'=>'multipart/form-data','method'=>'patch']) !!}


                        <div class="body">
                            <div class="row clearfix">




                                <div class="body">
                                    <div class="row clearfix">

                                        <div class="col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" placeholder="News Title"  value="{{ $news->news_title }}"  name="news_title">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="body">
                                            <div class="row clearfix">

                                                <div class="col-sm-4 col-xs-12">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input type="text" class="form-control" placeholder="Author" value="{{ $news->author }}"  name="author">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>





                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        {{ Form::textarea('news_post', $news->news_post)}}
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-sm-12">
                                                <div class="form-group">

                                                    {{--                                @if ("{{asset('storage/image/'.$blog->image)}}")--}}
                                                    <img src="{{asset('image/news-photo/'.$news->image)}}" width="400px",height="400px">
                                                    {{--@else--}}
                                                    {{--<p>No Image Found</p>--}}
                                                    {{--@endif--}}


                                                    {{--image <input type="file" name="image" value="{{ $blog->image }}"/>--}}
                                                </div>

                                                <div class="form-control">

                                                    <label for="Image">Update Image</label>
                                                    <input type="file" name="image">

                                                </div>
                                                <br><br>
                                            </div>





                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-raised g-bg-cyan"   >update</button>
                                                <button type="submit" class="btn btn-raised">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

        {!! Form::close() !!}
    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

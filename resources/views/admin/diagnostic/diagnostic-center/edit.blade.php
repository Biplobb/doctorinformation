
@extends('admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Diagnostic Center </h2>
                <small class="text-muted">Diagnostic Center information submit form</small>
            </div>
            <div class="row clearfix">
{{--                {!! Form::open(['url' => '/admin/diagnostic/diagnostic-center/update/'.$diagnostic_center->id,'enctype'=>'multipart/form-data','method'=>'patch']) !!}--}}
                {!! Form::open(['route' => ['diagonosticsupdate',$diagnostic_center->id],'enctype'=>'multipart/form-data','method'=>'patch']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Diagnostic Center Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Test Name" name="name" value="{{ $diagnostic_center->name }}">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="location" name="location"  value="{{ $diagnostic_center->location }}">
                                        </div>
                                    </div>
                                </div>





                                <div class="row clearfix">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" placeholder="about" name="about" value="{{ $diagnostic_center->about }}">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label>    Update image</label>
                                                <input type="file" class="form-control" placeholder="photo" name="photo" value="{{ $diagnostic_center->photo }}">
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-sm-6 col-xs-12">

                                </div>
                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">update</button>
                                    <button type="submit" class="btn btn-raised">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <h1>Diagnostic Table</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">location</th>
                <th scope="col">about</th>
                <th scope="col">image</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($diagnostic_center as $diagnostic_center)
                <tr>
                    <th scope="row">{{ $diagnostic_center->id }}</th>
                    <td>{{ $diagnostic_center->name }}</td>
                    <td>{{ $diagnostic_center->location }}</td>
                    <td>{{ $diagnostic_center->about }}</td>
                    <td>{{ $diagnostic_center->photo }}</td>

                    <td>

{{--                        <a href="{{ url('admin/diagnostic/diagnostic-center/edit/'.$diagnostic_center->id) }}">Edit</a>--}}
                        <a href="{{ route('diagonosticsedit',['id'=>$diagnostic_center->id]) }}">Edit</a>
{{--                        <a href="{{ url('/admin/diagnostic/diagnostic-center/destroy/'.$diagnostic_center->id) }}">Delete</a>--}}
                        <a href="{{ route('diagonosticsdestroy',['id'=>$diagnostic_center->id])}}">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

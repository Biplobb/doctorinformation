
@extends('admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Diagnostic center</h2>
                <small class="text-muted">Diagnostic information submit form</small>
            </div>
            <div class="row clearfix">
                {!! Form::open(['url' => '/admin/diagnostic/diagnostic-center-test/store','enctype'=>'multipart/form-data','method'=>'post']) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Diagnostic Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class=" col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="">Diagnostic Name</label>
                                            <select class="form-control" name="Diagnostic_Center_id"   data-parsley-required="true" placeholder="Diagnostic Name">
                                                @foreach ($diagnostic_center as $diagnostic_center)
                                                    {

                                                    <option value="{{ $diagnostic_center->id }}">{{ $diagnostic_center->name }}</option>

                                                    }

                                                @endforeach







                                            </select>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row clearfix">
                                <div class=" col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="">Tests Name</label>

                                                @foreach ($tests as $tests)

                                                <input type="checkbox" name="tests[]" id="{{$tests->id}}" value={{$tests->id}}>
                                                <label for="{{$tests->id}}">{{$tests->name}}</label>

                                                <input type="text" placeholder="previous price" name="previouss[]">
                                                <input type="text" placeholder="Discount percentage" name="discounts[]">%



                                                @endforeach





                                        </div>

                                    </div>
                                </div>


                            </div>


                       {{--     <div class="row clearfix">
                                <div class=" col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control" name="name"   data-parsley-required="true">
                                                @foreach ($tests as $tests)
                                                    {

                                                    <option value="{{ $tests->name }}">{{ $tests->name }}</option>
                                                    }
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>


                            </div>
--}}
                            </div>
                            <div class="row clearfix">


                                <div class="col-xs-12">
                                    <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                    <button type="submit" class="btn btn-raised">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>


        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}


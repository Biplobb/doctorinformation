{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Add Chmaber Schedule for <span style="color: #00b6f1">{{$doctors->name}}</span></h2>
            <small class="text-muted">Doctors Schedule form</small>
        </div>

        @if(session('scheduleadded'))
            <div class="alert alert-success">
                {{session('scheduleadded')}}
            </div>
        @endif

        @if(session('dataexist'))
            <div class="alert alert-danger">
                {{session('dataexist')}}
            </div>
        @endif
        @if(session('errors'))
            <div class="alert alert-danger">
                {{session('errors')}}
            </div>
        @endif

        <form action="{{route('adddoctorschedule')}}" method="post" name="scheduleform" onsubmit="return validatescheduleform()">
            {{csrf_field()}}



            <div class="row clearfix">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Doctor's Account Information <small>Description text here...</small> </h2>
                        </div>
                        <div class="body">

                            <div class="row clearfix">
                                <div class="col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label for="">First visit fee</label>
                                                <input type="text" class="form-control" placeholder="First time visit fees" name="first_fees">
                                            </div>
                                        </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="">Second visit fee</label>
                                            <input type="text" class="form-control" placeholder="Second time visit fees" name="second_fees">
                                        </div>
                                    </div>
                                 </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-6 col-xs-12">

                                    <div class="form-group">
                                        <span id="validatedoctorid"></span>
                                        <select class="form-control show-tick" name="doctor_id" id="doctor_id">
                                            <option value="">Select doctor</option>
                                                <option value="{{$doctors->id}}">{{$doctors->name}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12">
                                    <div class="form-group drop-custum">
                                        <span id="validatehospital_id"></span>
                                        <select class="form-control show-tick" name="hospital_id" id="hospital_id">
                                            <option value="">Hospital / Chamber</option>
                                            @foreach($hospital as $hosp)
                                            <option value="{{$hosp->hospitalid}}">{{$hosp->hospitalname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Doctor's Account Information <small>Description text here...</small> </h2>
                        </div>
                        <div class="body">
                            <ul class="list-group">
                                @foreach($days as $day)
                            <li class="list-group-item clearfix">
                                <div class="row clearfix">
                                    <div class="col-sm-1 col-xs-12">
                                        <div class="input-group">
                                            <div class="">
                                                <input type="checkbox" name="day[]" id="id.{{$day->id}}" class="filled-in chk-col-pink" value="{{$day->id}}">
                                                <label for="id.{{$day->id}}">{{$day->day_name}}</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group drop-custum">
                                            <span id="validatestarttime"></span>
                                            <select class="form-control show-tick" name="start[]" id="start">
                                                <option value="">Start Time</option>
                                                <option value="10:00">10 am</option>
                                                <option value="11:00">11 am</option>
                                                <option value="12:00">12 am</option>
                                                <option value="13:00">1 pm</option>
                                                <option value="14:00">2 pm</option>
                                                <option value="15:00">3 pm</option>
                                                <option value="16:00">4 pm</option>
                                                <option value="17:00">5 pm</option>
                                                <option value="18:00">6 pm</option>
                                                <option value="19:00">7 pm</option>
                                                <option value="20:00">8 pm</option>
                                                <option value="21:00">9 pm</option>
                                                <option value="22:00">10 pm</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group drop-custum">
                                            <select class="form-control show-tick" name="end[]">
                                                <option value="">End Time</option>
                                                <option value="10:00">10 am</option>
                                                <option value="11:00">11 am</option>
                                                <option value="12:00">12 am</option>
                                                <option value="13:00">1 pm</option>
                                                <option value="14:00">2 pm</option>
                                                <option value="15:00">3 pm</option>
                                                <option value="16:00">4 pm</option>
                                                <option value="17:00">5 pm</option>
                                                <option value="18:00">6 pm</option>
                                                <option value="19:00">7 pm</option>
                                                <option value="20:00">8 pm</option>
                                                <option value="21:00">9 pm</option>
                                                <option value="22:00">10 pm</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group drop-custum">
                                            <select class="form-control show-tick" name="interval[]">
                                                <option value="">Select Interval</option>
                                                <option value="15">15 Min</option>
                                                <option value="20">20 Min</option>
                                                <option value="30">30 Min</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                            </li>
                                @endforeach
                            </ul>
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-raised g-bg-cyan">Submit</button>
                                <button type="submit" class="btn btn-raised">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
        {{--</div>--}}
    </div>

</section>
@endsection

@section('script')
    <script>
        function validatescheduleform() {
            var doctor_id=document.forms["scheduleform"]["doctor_id"].value;
            var hospital_id=document.forms["scheduleform"]["hospital_id"].value;

            var start=document.forms["scheduleform"]["start[]"];
            var end=document.forms["scheduleform"]["end[]"];
            var day=document.forms["scheduleform"]["day[]"];


            if(doctor_id==''){
                $('#validatedoctorid').text('Select doctor!!');
                $('#validatedoctorid').css({ "color": "#f71111","font-weight":"bold"});
                return false;
            }else if( hospital_id==''){
                $('#validatehospital_id').text('Select hospital !!');
                $('#validatehospital_id').css({ "color": "#f71111","font-weight":"bold"});
                return false;
            }


            if( (start[0].selectedIndex!=''
                || start[1].selectedIndex!=''
                || start[2].selectedIndex!=''
                || start[3].selectedIndex!=''
                || start[4].selectedIndex!=''
                || start[5].selectedIndex!=''
                || start[6].selectedIndex!='')
                &&
                (end[0].selectedIndex!=''
                || end[1].selectedIndex!=''
                || end[2].selectedIndex!=''
                || end[3].selectedIndex!=''
                || end[4].selectedIndex!=''
                || end[5].selectedIndex!=''
                || end[6].selectedIndex!='')
                &&
                (day[0].checked ||
                day[1].checked ||
                day[2].checked ||
                day[3].checked ||
                day[4].checked ||
                day[5].checked ||
                day[6].checked)
            ){
                return true;
                } else {
                alert("Select day and time properly !!");
                return false;
                }
        }
    </script>
@endsection
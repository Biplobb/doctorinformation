
{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')

@section('content')

<section class="content profile-page">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12 p-l-0 p-r-0">
                <section class="boxs-simple">
                    <div class="profile-header">
                        <div class="profile_info">
                            <div class="profile-image"> <img src="{{ asset('image/doctor-photo/'.$doctorprofile->photo)}}" alt=""> </div>
                            <h4 class="mb-0"><strong>{{ $doctorprofile->name }}</strong></h4>
                            <span class="text-muted col-white">{{$doctorprofile->speciality}}</span>
                        </div>
                    </div>
                </section>
             </div>
            </div>

                <div class="row clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>{{$doctorprofile->name}}</h2>
                            </div>
                            <div class="body">
                                <p class="text-default">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                                <blockquote>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                    <li role="presentation" class="active"><a href="#info" data-toggle="tab" aria-expanded="false">Doctor Info</a></li>
                                    <li role="presentation" class=""><a href="#chamber" data-toggle="tab" aria-expanded="true">Chambers</a></li>
                                    <li role="presentation"><a href="#timeschedule" data-toggle="tab" aria-expanded="true">Time Schedule</a></li>
                                </ul>
                                <!-- Tab panes -->

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="info">
                                        <h4> {{$doctorprofile->name}}</h4>
                                        <p>Speciality {{$doctorprofile->speciality}}</p>
                                        <p>Contact : {{$doctorprofile->phone}}</p>
                                        <p>Email : {{$doctorprofile->email}}</p>
                                        <p>Address : {{$doctorprofile->address}}</p>
                                        <p>Awards : {{$doctorprofile->awards}}</p>
                                        <p>Experience : {{$doctorprofile->experience}} years</p>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="chamber">
                                        @php $i=1; @endphp
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Sl.</th>
                                                    <th>Chamber</th>
                                                    <th>Location</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            @foreach($chamber as $cmbr)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$cmbr->chamber}}</td>
                                                    <td>{{$cmbr->location}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="timeschedule">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Chamber</th>
                                                    <th>Start time</th>
                                                    <th>End time</th>
                                                    <th>Interval</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($timeschedule as $tmsd)
                                                    <tr>
                                                        <td>{{$tmsd->hospitalname}}</td>
                                                        <td>{{$tmsd->start}}</td>
                                                        <td>{{$tmsd->end}}</td>
                                                        <td>{{$tmsd->interval}} minutes</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

</section>
@endsection


{{--@include('admin.layouts.footer')--}}


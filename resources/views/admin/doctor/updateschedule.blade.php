{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>Update Doctor schedule</h2>
                <h3>Hospital : {{$data->hospitalname}}</h3>

            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">

                            @if(session('updated'))
                                <div class="alert alert-success">
                                    <strong> {{session('updated')}} !!</strong>
                                </div>
                            @endif
                            <ul class="list-group">

                                    <li class="list-group-item clearfix">
                                        <div class="row clearfix">
                                            <div class="col-sm-12 col-xs-12">
                                                <h4>Select day</h4>
                                                <div class="input-group">

                                                    <form action="{{route('updateSchedule')}}" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="scheduleid" value="{{$data->id}}">
                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox" {{$data->day_id==1 ? 'checked="checked"' : '' }} name="day" id="1" class="filled-in chk-col-pink" value="1">--}}
                                                        {{--<label for="1">Saturday</label>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox" {{$data->day_id==2 ? 'checked="checked"' : '' }} name="day" id="2" class="filled-in chk-col-pink" value="2">--}}
                                                        {{--<label for="2">Sunday</label>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox" {{$data->day_id==3 ? 'checked="checked"' : '' }} name="day" id="3" class="filled-in chk-col-pink" value="3">--}}
                                                        {{--<label for="3">Monday</label>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox" {{$data->day_id==4 ? 'checked="checked"' : '' }} name="day" id="4" class="filled-in chk-col-pink" value="4">--}}
                                                        {{--<label for="4">Tuesday</label>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox"  {{$data->day_id==5 ? 'checked="checked"' : '' }}name="day" id="5" class="filled-in chk-col-pink" value="5">--}}
                                                        {{--<label for="5">Wednesday</label>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox" {{$data->day_id==6 ? 'checked="checked"' : '' }} name="day" id="6" class="filled-in chk-col-pink" value="6">--}}
                                                        {{--<label for="6">Thursday</label>--}}
                                                    {{--</div>--}}

                                                    {{--<div class="">--}}
                                                        {{--<input type="checkbox" {{$data->day_id==7 ? 'checked="checked"' : '' }} name="day" id="7" class="filled-in chk-col-pink" value="7">--}}
                                                        {{--<label for="7">Friday</label>--}}
                                                    {{--</div>--}}
                                                        <div class="form-line">
                                                            <label for="">Days</label>

                                                            <input type="radio" id="contactChoice2"
                                                                   name="day" value="1" {{ $data->day_id==1? "checked='checked'":''}}>
                                                            <label for="contactChoice2">Monday</label>

                                                            <input type="radio" id="contactChoice3"
                                                                   name="day" value="2" {{ $data->day_id==2? "checked='checked'":''}}>
                                                            <label for="contactChoice3">Sunday</label>

                                                            <input type="radio" id="contactChoice4"
                                                                   name="day" value="3" {{ $data->day_id==3? "checked='checked'":''}}>
                                                            <label for="contactChoice4">Monday</label>

                                                            <input type="radio" id="contactChoice5"
                                                                   name="day" value="4" {{ $data->day_id==4? "checked='checked'":''}}>
                                                            <label for="contactChoice5">Tuesday</label>

                                                            <input type="radio" id="contactChoice8"
                                                                   name="day" value="5" {{ $data->day_id==5? "checked='checked'":''}}>
                                                            <label for="contactChoice8">Wednesday</label>

                                                            <input type="radio" id="contactChoice9"
                                                                   name="day" value="6" {{ $data->day_id==6? "checked='checked'":''}}>
                                                            <label for="contactChoice9">Thursday</label>

                                                            <input type="radio" id="contactChoice6"
                                                                   name="day" value="7" {{ $data->day_id==7? "checked='checked'":''}}>
                                                            <label for="contactChoice6">Friday</label>
                                                        </div>
                                                </div>
                                            </div>

                                            <div class="row clearfix">
                                                <div class="col-sm-12 col-lg-12 col-xs-12">

                                                <div class="col-sm-3 col-xs-12">
                                                    <div class="form-group drop-custum">
                                                        <label for="">Start time</label>
                                                        <select class="form-control show-tick" name="start">
                                                            <option value="">Start Time</option>
                                                            <option value="10:00" {{$data->start=='10:00:00'? 'selected="selected"':'' }}>10 am</option>
                                                            <option value="11:00" {{$data->start=='11:00:00'? 'selected="selected"':'' }}>11 am</option>
                                                            <option value="12:00" {{$data->start=='12:00:00'? 'selected="selected"':'' }}>12 am</option>
                                                            <option value="13:00" {{$data->start=='13:00:00'? 'selected="selected"':'' }}>1 pm</option>
                                                            <option value="14:00" {{$data->start=='14:00:00'? 'selected="selected"':'' }}>2 pm</option>
                                                            <option value="15:00" {{$data->start=='15:00:00'? 'selected="selected"':'' }}>3 pm</option>
                                                            <option value="16:00" {{$data->start=='16:00:00'? 'selected="selected"':'' }}>4 pm</option>
                                                            <option value="17:00" {{$data->start=='17:00:00'? 'selected="selected"':'' }}>5 pm</option>
                                                            <option value="18:00" {{$data->start=='18:00:00'? 'selected="selected"':'' }}>6 pm</option>
                                                            <option value="19:00" {{$data->start=='19:00:00'? 'selected="selected"':'' }}>7 pm</option>
                                                            <option value="20:00" {{$data->start=='20:00:00'? 'selected="selected"':'' }}>8 pm</option>
                                                            <option value="21:00" {{$data->start=='21:00:00'? 'selected="selected"':'' }}>9 pm</option>
                                                            <option value="22:00" {{$data->start=='22:00:00'? 'selected="selected"':'' }}>10 pm</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-xs-12">
                                                    <div class="form-group drop-custum">
                                                        <label for="">End time</label>
                                                        <select class="form-control show-tick" name="end">
                                                            <option value="">End Time</option>
                                                            <option value="10:00"{{$data->end=='10:00:00'? 'selected="selected"':'' }} >10 am</option>
                                                            <option value="11:00"{{$data->end=='11:00:00'? 'selected="selected"':'' }}>11 am</option>
                                                            <option value="12:00"{{$data->end=='12:00:00'? 'selected="selected"':'' }}>12 am</option>
                                                            <option value="13:00"{{$data->end=='13:00:00'? 'selected="selected"':'' }}>1 pm</option>
                                                            <option value="14:00"{{$data->end=='14:00:00'? 'selected="selected"':'' }}>2 pm</option>
                                                            <option value="15:00"{{$data->end=='15:00:00'? 'selected="selected"':'' }}>3 pm</option>
                                                            <option value="16:00"{{$data->end=='16:00:00'? 'selected="selected"':'' }}>4 pm</option>
                                                            <option value="17:00"{{$data->end=='17:00:00'? 'selected="selected"':'' }}>5 pm</option>
                                                            <option value="18:00"{{$data->end=='18:00:00'? 'selected="selected"':'' }}>6 pm</option>
                                                            <option value="19:00"{{$data->end=='19:00:00'? 'selected="selected"':'' }}>7 pm</option>
                                                            <option value="20:00"{{$data->end=='20:00:00'? 'selected="selected"':'' }}>8 pm</option>
                                                            <option value="21:00"{{$data->end=='21:00:00'? 'selected="selected"':'' }}>9 pm</option>
                                                            <option value="22:00"{{$data->end=='22:00:00'? 'selected="selected"':'' }}>10 pm</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-xs-12">
                                                    <div class="form-group drop-custum">
                                                        <label for="">Interval</label>
                                                        <select class="form-control show-tick" name="interval">
                                                            <option value="">Duration</option>
                                                            <option value="15" {{$data->interval==15? 'selected="selected"':'' }}>15 Min</option>
                                                            <option value="20" {{$data->interval==20? 'selected="selected"':'' }}>20 Min</option>
                                                            <option value="30" {{$data->interval==30? 'selected="selected"':'' }}>30 Min</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-3 col-xs-12">
                                                    <div class="form-group drop-custum">
                                                        <label for="">Available</label>
                                                        <select class="form-control show-tick" name="available">
                                                            <option value="">Select Availability</option>
                                                            <option value="yes" {{$data->available=='yes'? 'selected="selected"':'' }}>Available</option>
                                                            <option value="no" {{$data->available=='no'? 'selected="selected"':'' }}>Unavailable</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <button type="submit" class="btn  btn-raised btn-success waves-effect">Update</button>
                                        </div>
                                        </form>
                                    </li>



                            </ul>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

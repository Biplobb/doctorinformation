{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>All Doctors</h2>
                <small class="text-muted">Welcome to Swift application</small>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">

                            @if(session('updatesuccess'))
                                <div class="alert alert-success">
                                    {{session('updatesuccess')}}
                                </div>
                            @endif

                                @if(session('deletesuccess'))
                                    <div class="alert alert-danger">
                                        {{session('deletesuccess')}}
                                    </div>
                                @endif

                            <div class="member-card verified">
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Edit</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Delete</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Block</a></li>
                                        </ul>
                                    </li>
                                </ul>

                                <div class="">

                                    <table class="table">
                                        <thead>


                                        <tr>
                                            <th scope="col">Sl.</th>
                                            <th scope="col">name</th>
                                            <th scope="col">speciality</th>
                                            <th scope="col">phone</th>
                                            <th scope="col">address</th>
                                            <th scope="col">member</th>
                                            <th scope="col">awards</th>
                                            <th scope="col">experience</th>

                                            {{--<th scope="col">chamber/Hospital</th>--}}
                                            <th scope="col">photo</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php

                                            $i=1;

                                        @endphp
                                        @foreach($doctorprofile as $doctorprofile)

                                            <tr>
                                                <th scope="row">{{ $i++ }}</th>
                                                <td>{{ $doctorprofile->name }}</td>
                                                <td>{{ $doctorprofile->speciality }}</td>
                                                <td>{{ $doctorprofile->doctor_phone }}</td>
                                                <td>{{ $doctorprofile->doctor_address }}</td>
                                                <td>{{ $doctorprofile->member }}</td>
                                                <td>{{ $doctorprofile->awards }}</td>
                                                <td>{{ $doctorprofile->experience }}</td>

                                                {{--<td>{{ $doctorprofile->hospital_name }}</td>--}}
                                                <td>{{Html::image('image/doctor-photo/'.$doctorprofile->photo,'photo',['style'=>'width:90px;height:90px'])}}</td>

                                                <td>
                                                    @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->hasRole('supportadmin'))
                                                        <a href="{{route('showdoctorinfo',[$doctorprofile->id])}}" class="btn  btn-raised btn-success waves-effect">Show</a>
                                                    @else
                                                        <a href="{{route('showdoctorinfo',[$doctorprofile->id])}}" class="btn  btn-raised btn-success waves-effect">Show</a>
                                                        <a href="{{route('editdoctorinfo',[$doctorprofile->id])}}" class="btn  btn-raised btn-info waves-effect">Edit</a>
                                                        <a href="{{route('deletedoctor',[$doctorprofile->id])}}" class="btn  btn-raised btn-danger waves-effect">Delete</a>

                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')

@section('content')

    <section class="content" xmlns="http://www.w3.org/1999/html">
        <div class="container-fluid">
            <div class="block-header">
                {{--<h2>Add Chmaber Schedule for <span style="color: #00b6f1">{{$doctors->name}}</span></h2>--}}
                <small class="text-muted">Add or Remove Doctor Chamber/Hospital</small>
            </div>

            @if(session('scheduleadded'))
                <div class="alert alert-success">
                    {{session('scheduleadded')}}
                </div>
            @endif

            @if(session('dataexist'))
                <div class="alert alert-danger">
                    {{session('dataexist')}}
                </div>
            @endif

            <form action="{{route('postaddorremovechmaber')}}" method="post" name="scheduleform" id="myform" onsubmit="return validate()">
                {{csrf_field()}}
                <div class="row clearfix">
                    <div class="col-md-12 col-xs-12">
                        <div class="card">
                            @if(session('delete'))
                                <div class="alert alert-danger">
                                    {{session('delete')}}
                                </div>
                            @endif
                                @if(session('add'))
                                    <div class="alert alert-success">
                                        {{session('add')}}
                                    </div>
                                @endif
                                @if(session('exist'))
                                    <div class="alert alert-danger">
                                        {{session('exist')}}
                                    </div>
                                @endif
                            <div class="header">
                                <h2>Add or Remove Doctor Schedule<small>Select options bellow ...</small> </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group">
                                            <label for="doctor_id">Select Doctor</label>
                                            <span id="validatedoctorid" class="font-bold col-pink"></span>
                                            <select class="form-control show-tick" name="doctor_id" id="doctor_id">
                                                <option value="">Select doctor</option>
                                                @foreach($doctors as $doctors)
                                                    <option value="{{$doctors->id}}">{{$doctors->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-xs-12">
                                        <div class="form-group drop-custum">
                                            <label for="doctor_id">Select Hospital</label>
                                            <span id="validatehospital_id" class="font-bold col-pink"></span>
                                            <select class="form-control show-tick" name="hospital_id" id="hospital_id">
                                                <option value="">Hospital / Chamber</option>
                                                @foreach($hospitals as $hosp)
                                                    <option value="{{$hosp->id}}">{{$hosp->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="opt_type" id="opt_type">

                                    <div class="col-sm-6 col-xs-12">
                                        <div class="form-group ">
                                            <button type="button" name="submitbutton" class="btn  btn-raised btn-success waves-effect" id="confirm" value="add" data-toggle="modal" data-target="#defaultModal">Add Hospital</button>
                                            <button type="button" name="submitbutton" class="btn  btn-raised btn-danger waves-effect" id="delete" value="remove" data-toggle="modal" data-target="#deletemodal">Remove Hospital</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
        </div>

    </section>

    <div class="modal fade Modal-Col-Pink"  id="defaultModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title font-bold col-pink" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body"> <strong><p class="font-bold col-teal">You want to add this add this change? </p></strong> </div>
                <div class="modal-footer">
                    <button type="button" id="close" class="btn btn-raised  btn-success waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="yesconfirm" class="btn btn-raised  btn-danger waves-effect" data-dismiss="modal">CONFIRM</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade Modal-Col-Pink"  id="deletemodal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title font-bold col-pink" id="defaultModalLabel">Alert !!</h4>
                </div>
                <div class="modal-body"> <strong><p class="font-bold col-pink">Do you want to delete this data? </p></strong> </div>
                <div class="modal-footer">
                    <button type="button" id="close" class="btn btn-raised  btn-success waves-effect" data-dismiss="modal">CLOSE</button>
                    <button type="button" id="yesdelete" class="btn btn-raised  btn-danger waves-effect" data-dismiss="modal">DELETE</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('script')


    <script>



        $(document).on('click','#yesconfirm',function () {
            $('#opt_type').val('add');
            $('#myform').submit();
        });

        $(document).on('click','#yesdelete',function () {
            $('#opt_type').val('remove');
            $('#myform').submit();
        });

         function validate()
         {
            if(document.getElementById('doctor_id').value=='')
            {
                $('#validatedoctorid').text("Select Doctor");
                return false;
            }else if(document.getElementById('hospital_id').value=='')
            {
                $('#validatehospital_id').text("Select hospital");
                return false;
            }

            return true;

        }
    </script>


@endsection
{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')
@section('content')
    <section class="content">

        <div class="container-fluid">
            <div class="block-header">
                <h2>All Doctors schedule</h2>

            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        @if(session('deleted'))
                            <div class="alert alert-danger!!">
                                {{session('deleted')}}
                            </div>
                        @endif

                        <div class="body">
                            <div class="member-card verified">
                                <ul class="header-dropdown">
                                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="zmdi zmdi-more-vert"></i></a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Edit</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Delete</a></li>
                                            <li><a href="javascript:void(0);" class=" waves-effect waves-block">Block</a></li>
                                        </ul>
                                    </li>
                                </ul>


                                <div class="">

                                    <table class="table">
                                        <thead>

                                        <tr>
                                            <th scope="col">Sl.</th>
                                            <th scope="col">Doctor name</th>
                                            <th scope="col">Hospital name</th>
                                            <th scope="col">Day</th>
                                            <th scope="col">Start time</th>
                                            <th scope="col">End time</th>
                                            <th scope="col">Duration</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i=1;
                                        @endphp
                                        @foreach($data as $doctorprofiles)
                                            <tr>
                                                <td >{{$i++}}</td>
                                                <td>{{ $doctorprofiles->doctorname }}</td>
                                                <td>{{ $doctorprofiles->hospitalname }}</td>
                                                <td>{{ $doctorprofiles->day_name }}</td>
                                                <td>{{ $doctorprofiles->start }}</td>
                                                <td>{{ $doctorprofiles->end }}</td>
                                                <td>{{ $doctorprofiles->interval }}</td>
                                                <td><a href="{{route('editschedule', ['id' => $doctorprofiles->id,'doctor_id'=>$doctorprofiles->doctor_id])}}" class="btn  btn-raised btn-success waves-effect" title="Edit"><i class="material-icons">autorenew</i></a></td>
                                                <td><a href="{{route('deleteschedule', ['id' => $doctorprofiles->schid])}}" class="btn  btn-raised btn-danger waves-effect" title="Delete"><i class="material-icons">delete</i></a></td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section>
@endsection

{{--@include('admin.layouts.footer')--}}

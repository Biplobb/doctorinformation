{{--@include('admin.layouts.header')--}}
@extends('admin.layouts.master')

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Add Doctor</h2>
                <small class="text-muted">Welcome to Swift application</small>
            </div>

            <div class="row clearfix">
                {!! Form::open(['route' =>['updatedoctorinfo', $doctorprofile->id],'method'=>'patch','files'=>true]) !!}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Profile Information</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Full Name" name="name" value="{{$doctorprofile->name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Speciality" name="speciality" value="{{$doctorprofile->speciality}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Your Phone"  name="phone" value="{{$doctorprofile->phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" class="form-control" placeholder="Email Address" name="address" value="{{$doctorprofile->email}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Address" name="address" value="{{$doctorprofile->address}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Membership" name="member" value="{{$doctorprofile->member}}">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Achievement and Awards" name="awards" value="{{$doctorprofile->awards}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Experience Year" name="experience" value="{{$doctorprofile->experience}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Chamber Location" name="chamber" value="{{$doctorprofile->chamber}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <label for="">Degree</label>
                                            <input type="text" class="form-control" placeholder="Degree" name="degree" value="{{$doctorprofile->degree}}">
                                        </div>
                                    </div>
                                </div>


                            <div class="row clearfix">
                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        {{Form::label('photo')}}
                                        {{Html::image('image/doctor-photo/'.$doctorprofile->photo,'photo',['style'=>'width:90px;height:90px'])}}<br>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group">
                                        <label for="exampleInputName1">Upload Image</label>
                                        <input type="file" name="photo" class="form-control" >
                                    </div>
                                </div>

                                <div class="col-sm-4 col-xs-12">
                                    <div class="form-group ">
                                        <button type="submit" class="btn btn-raised g-bg-cyan">Update</button>
                                        <button type="submit" class="btn btn-raised">Cancel</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>


    </section>




@endsection

{{--@include('admin.layouts.footer')--}}



@extends('admin.layouts.master')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>Add Doctor</h2>
            <small class="text-muted">Doctors information submit form</small>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Profile Information</h2>
                    </div>
                    <div class="body">

                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $er)
                                    {{$er}}
                                @endforeach
                            </div>
                        @endif
                            {{--,'name'=>'adddoctorprofile'--}}

                    {!! Form::open(['route' => 'adddoctorprofileinfo','enctype'=>'multipart/form-data','method'=>'post']) !!}
                    {{csrf_field()}}
                        <div class="row clearfix">

                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Full Name" name="name" required>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Email" name="email" required>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <select class="form-control" name="speciality" id="" required>
                                            <option value="">Select speciality</option>
                                            <option value="Cardiology">Cardiology</option>
                                            <option value="Dental">Dental</option>
                                            <option value="Dermatology">Dermatology</option>
                                            <option value="Gastroenterology">Gastroenterology</option>
                                            <option value="Medicine">Medicine</option>
                                            <option value="Orthopaedics">Orthopaedics</option>
                                            <option value="Psychology">Psychology</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-3 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Your Phone"  name="phone" required>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Set Address" name="address" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="password" class="form-control" placeholder="Set password" name="password" required>
                                    </div>
                                </div>
                            </div>



                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Membership" name="member">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row clearfix">


                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="south">Hospitals :</label>
                                        @foreach($data as $d)
                                            <input type="checkbox" name="hospital[]" id="{{$d->id}}" value={{$d->id}}>
                                            <label for="{{$d->id}}">{{$d->name}}</label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="">Degree</label>
                                        <input type="text" class="form-control" placeholder="Degree" name="degree" required>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="row clearfix">
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Achievement and Awards" name="awards">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Experience Year" name="experience" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="exampleInputName1">Upload Image</label>
                                        <input type="file" name="photo" required>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="row clearfix">
                            <div class="col-xs-12 ">
                                <button type="submit" class="btn btn-raised g-bg-cyan pull-right">Submit</button>
                                <button type="submit" class="btn btn-raised pull-right">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>


    </div>
</section>
@endsection

{{--@include('admin.layouts.footer')--}}


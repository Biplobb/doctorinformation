@extends('layouts.master')
@section('title', 'Deshidoctor | About')
@section('content')
    <!--Bread Crumb-->
    <section id="breadcrumb" class="space light-overlay" data-stellar-background-ratio="0.4">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 bread-block animate-in fade-in">
                    <h2>About Us</h2>
                    <p>Affordable Treatments, Honest & Experienced Dentists.</p>
                </div>
                <div class="col-sm-6 bread-block text-right animate-in fade-in">
                    <a href="#" class="btn">Book appointment</a>
                </div>
            </div>
        </div>
    </section>
    <!--About US-->
    <section id="about-us" class="space">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 about-block animate-in move-up">
                    <div class="inner text-right no-padding">
                        <img src="{{asset('front-end/assets/images/about-img.png')}}" alt="About us">
                    </div>
                </div>
                <div class="col-sm-6 about-block animate-in move-up">
                    <h2>Affordable Treatments, Honest & Experienced Dentists,
                        <span>Friendly Care.</span></h2>
                    <p>At Columba, we pride ourselves in premium yet affordable dental care. We’re a warm family of 14 offices situated (as of April of 2016) throughout the Southern Ontario region in Canada. Some of our offices have been operating for over ten years now. With just the right number of offices, we’re able to continuously provide our patients with immaculate patient care, without sacrificing the quality of care or convenience provided by the sheer number of locations operating today.</p>
                </div>
            </div>
        </div>
    </section>
    <!--counter-->
    <section id="counter" class="space bg-color">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 counter-block animate-in move-up" data-count="20">
                    <div class="odometer">0</div>
                    <h3>Years Of Experience</h3>
                    <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                </div>
                <div class="col-sm-3 counter-block animate-in move-up" data-count="35">
                    <div class="odometer">0</div>
                    <h3>Talent Doctors</h3>
                    <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                </div>
                <div class="col-sm-3 counter-block animate-in move-up" data-count="90">
                    <div class="odometer">0</div>
                    <h3>Professional Awards</h3>
                    <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                </div>
                <div class="col-sm-3 counter-block animate-in move-up" data-count="1470">
                    <div class="odometer">0</div>
                    <h3>Satisfied Clients</h3>
                    <p>Curabitur tincidunt eu neque ut posuere. Nam ac condimentum massa, sed faucibus lorem. Ut dapibus lacus </p>
                </div>
            </div>
        </div>
    </section>
    <!--Meet Our Team-->
    <section id="our-team" class="space v4">
        <div class="container">
            <div class="col-sm-6 col-sm-offset-3 text-center main-heading animate-in move-up">
                <h2>Meet Our Doctor</h2>
                <p>Proin viverra, purus at bibendum molestie, lorem mi dignissim mauris, sit amet elementum massa augue vel massa</p>
            </div>
            <div class="row">
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba1" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba2" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba3" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba4" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba5" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba6" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba7" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
                <div class="col-sm-3 team-block text-center animate-in move-up">
                    <div class="inner">
                        <div class="user-img">
                            <img src="{{asset('front-end/assets/images/team-v2-3.jpg')}}" alt="Columba8" class="img-circle">
                            <div class="hover center">
                                <ul class="socials">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a> </li>
                                </ul>
                            </div>
                        </div>
                        <div class="name">Matthew Ray</div>
                        <div class="experience">36 years experience</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--action-->
    <section class="action-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 ">
                    <div class="col-sm-8 animate-in move-up">
                        <h2>Planning A Visit To The Doctor</h2>
                    </div>
                    <div class="col-sm-4 animate-in move-up">
                        <a href="#" class="btn">Contact Us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
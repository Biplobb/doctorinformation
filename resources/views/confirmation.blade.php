@extends('layouts.master')
@section('title', 'Deshidoctor | Confirmation')
@section('content')
    <section id="error-page" class="screen-height center">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-offset-3 error-block text-center">
                    @if(session('oldperson'))
                        <h2>{{session('oldperson')}}</h2>
                    @elseif(session('newperson'))
                        <h2>{{session('newperson')}}</h2>
                    @endif
                    <h3>Your appointment is recorded.</h3>
                    <h3>We will contact with you soon.</h3>
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('layouts.master')
@section('title', 'Deshidoctor | Error')
@section('content')
    <section id="error-page" class="screen-height center">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-offset-3 error-block text-center">
                    <h1>404</h1>
                    <h4>Oops, page not found.</h4>
                    <a href="{{route('homeindex')}}" class="btn">Back To Home</a>
                </div>
            </div>
        </div>
    </section>
@endsection
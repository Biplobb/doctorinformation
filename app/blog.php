<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $fillable=['title','category_id','author','blog_post','type','image','doctor_id'];


public function category(){
    return $this->belongsTo(Category::class);
    }
}

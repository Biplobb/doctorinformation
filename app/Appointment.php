<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;
    protected $dates=['deleted_at'];

    protected  $fillable=['doctor_id','hospital_id','name','address','phone','email','time','date','day','appointment_round','status'];
}

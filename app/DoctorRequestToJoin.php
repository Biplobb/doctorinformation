<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorRequestToJoin extends Model
{
    //
    protected $fillable=['name','email','designation','specialty','gender','phone','location','bmdc_reg_no','nid','degree','experience','interested-at','image'];
}

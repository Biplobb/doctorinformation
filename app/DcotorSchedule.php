<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DcotorSchedule extends Model
{
    //
    protected $fillable=['doctor_id','hospital_id','day_id','interval','start','end','available'];
}

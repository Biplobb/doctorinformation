<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestToJoin extends Model
{
    protected $fillable=['name','username','password','present','permanent','contact','occupation','code','email','gender','image'];
}

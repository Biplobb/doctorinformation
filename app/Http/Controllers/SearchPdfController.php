<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;


class SearchPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }
    public function verification(Request $request)
    {
return $request->code;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)

    {


        $results=array();

        $results=DB::table('user_request_to_join')->select('id','userid','name')
            ->where('name','like', '%' . $request->title. '%')
            ->orWhere('userid','like', '%' . $request->title. '%')->get();
$patient=DB::table('user_request_to_join')->select('*')
    ->where('name','like', '%' . $request->title. '%')
    ->orWhere('userid','like', '%' . $request->title. '%')->get();
        if(empty($request->title)){

            return view('search.notfound',compact('results','patient'));

        }

        if($request->title){


            return view('search.searching',compact('results','patient'));
        }





    }
    public function searchPDFdoctor(Request $request)

    {


        $results=array();


        $re= DB::table('doctor_profiles')->select('id')
            ->where('name','like', '%' . $request->title. '%')
            ->orWhere('doc_id','like', '%' . $request->title. '%')
            ->first();



        $results=DB::table('doctor_profiles')->select('id','doc_id','name')
            ->where('name','like', '%' . $request->title. '%')
            ->orWhere('doc_id','like', '%' . $request->title. '%')->get();

     $patient=DB::table('doctor_profiles')->select('*')
    ->where('name','like', '%' . $request->title. '%')
    ->orWhere('doc_id','like', '%' . $request->title. '%')->get();
        if(empty($request->title)){

            return view('search.doctor.notfound',compact('results','patient','re'));

        }

        if($request->title){


            return view('search.doctor.searching',compact('results','patient','re'));
        }





    }
    public function searchPDFmain(Request $request)

    {
        if($request->area && $request->lookingfor=='doctor' && $request->keyword){
        $data=DB::table('doctor_hospitals')

            ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
            ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')

            ->where('doctor_profiles.name','=',$request->keyword)

            ->Where('doctor_profiles.location','=',$request->area)
            ->select('doctor_profiles.id as did','doctor_profiles.name as dname','degree',
                'doctor_profiles.speciality as speciality','doctor_profiles.phone as dphone','doctor_profiles.location as address ','doctor_profiles.image as dphoto','hospital.name as hname')

            ->distinct('did')
            ->paginate(10);

            return view('search',compact('data'));
        }
        if($request->area && $request->lookingfor=='hospital' && $request->keyword){
            $Hospital=DB::table('hospital')

                ->where('hospital.name','=',$request->keyword)

                ->Where('hospital.location','=',$request->area)->orderBy('hospital.id', 'asc')->paginate(15);
            return view('hospitalservice',compact('Hospital'));
            $data=DB::table('doctor_profiles')->select('*')



                ->where('name','=',$request->keyword)

                ->Where('location','=',$request->area)


                ->distinct('did')
                ->paginate(10);
        }
if($request->area && $request->lookingfor=='diagnostic' && $request->keyword){
    $diagnostics=DB::table('diagnostic_center')->select('*')
        ->where('name','=',$request->keyword)

        ->Where('location','=',$request->area)

        ->paginate(5);
    return view('diagnostics',compact('diagnostics'));
            return view('hospitalservice',compact('Hospital'));
            $data=DB::table('doctor_profiles')->select('*')



                ->where('name','=',$request->keyword)

                ->Where('location','=',$request->area)


                ->distinct('did')
                ->paginate(10);
        }



        //fffffffffffffffffffffffffffffffffffffffffff
        if($request->area && $request->lookingfor=='doctor'){
            $data=DB::table('doctor_hospitals')

                ->leftJoin('doctor_profiles','doctor_profiles.id','=','doctor_hospitals.doctor_id')
                ->leftJoin('hospital','hospital.id','=','doctor_hospitals.hospital_id')

                ->Where('doctor_profiles.location','=',$request->area)
                ->select('doctor_profiles.id as did','doctor_profiles.name as dname','degree',
                    'doctor_profiles.speciality as speciality','doctor_profiles.phone as dphone','doctor_profiles.location as address ','doctor_profiles.image as dphoto','hospital.name as hname')

                ->distinct('did')
                ->paginate(10);

            return view('search',compact('data'));
        }
        if($request->area && $request->lookingfor=='hospital'){
            $Hospital=DB::table('hospital')

                ->where('hospital.name','=',$request->keyword)

                ->Where('hospital.location','=',$request->area)->orderBy('hospital.id', 'asc')->paginate(15);
            return view('hospitalservice',compact('Hospital'));
            $data=DB::table('doctor_profiles')->select('*')
                ->Where('location','=',$request->area)
                ->distinct('did')
                ->paginate(10);
        }

        if($request->area && $request->lookingfor=='diagnostic'){
            $diagnostics=DB::table('diagnostic_center')->select('*')

                ->Where('location','=',$request->area)

                ->paginate(5);
            return view('diagnostics',compact('diagnostics'));
            return view('hospitalservice',compact('Hospital'));
            $data=DB::table('doctor_profiles')->select('*')



                ->where('name','=',$request->keyword)

                ->Where('location','=',$request->area)


                ->distinct('did')
                ->paginate(10);
        }
        else{
            return view('404');
        }

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

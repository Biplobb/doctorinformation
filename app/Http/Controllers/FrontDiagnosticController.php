<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class FrontDiagnosticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $diagnostics=DB::table('diagnostic_center')->select('*')->get();
       return view('diagnostics',compact('diagnostics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {







        $testss=DB::table('diagnostic_center_tests')->select('tests.name as testname','diagnostic_center_tests.previous as diagnosticcentertestsprevious','diagnostic_center_tests.discount as diagnosticcentertestsdiscount','diagnostic_center_tests.discountprice as diagnosticcentertestsdiscountprice','tests.id as testid')->

        leftJoin('tests','diagnostic_center_tests.tests_id','tests.id')
            ->where('diagnostic_center_tests.Diagnostic_Center_id',$id)->get();
    $diagnostic_center_tests=DB::table('diagnostic_center_tests')

            ->leftJoin('tests','diagnostic_center_tests.Tests_id','tests.id')
            ->leftJoin('diagnostic_center','diagnostic_center_tests.Diagnostic_Center_id','diagnostic_center.id')
            ->select('diagnostic_center_tests.id as diagnosticcentertestsid','tests.name as testsname','diagnostic_center_tests.id as diagnosticcentertests','diagnostic_center.image as diagnosticphoto','diagnostic_center.id as diagnosticcenterid','diagnostic_center.name as diagnosticcentername','diagnostic_center.location as diagnosticcenterlocation','diagnostic_center.about as diagnosticcenterabout')->
           where('diagnostic_center_tests.Diagnostic_Center_id','=',$id)->get();





           $diagnostics=DB::table('diagnostic_center')->orderBy('diagnostic_center.id', 'dec')->take(3)->select('*')->get();


        return view('diagnosticshow',compact('diagnostic_center_tests','testss','diagnostics'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}

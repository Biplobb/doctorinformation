<?php

namespace App\Http\Controllers;
use App\blog;
use App\Category;
use App\DoctorProfile;
use App\comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class FrontBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctorprofile=DoctorProfile::all();
        $blog= blog::all();
        $recent_posts = DB::table('blogs')
            ->orderByRaw('updated_at - created_at DESC')
            ->limit(5)
            ->get();
        $categories = Category::all();

        return view('blog',compact('blog','doctorprofile','recent_posts','categories'));
    }
    public function blog_category_view($id){
        $doctorprofile=DoctorProfile::all();

         $recent_posts = DB::table('blogs')
            ->orderByRaw('updated_at - created_at DESC')
            ->get();
          $blog= blog::where('category_id', '=', $id)->get();
        $categories = Category::all();

        return view('blog',compact('blog','doctorprofile','recent_posts','categories'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blogs = blog::find($id);
       $comment_view= comment::all()
            ->where('status', '1');
        $recent_posts = DB::table('blogs')
            ->orderByRaw('updated_at - created_at DESC')
            ->limit(5)
            ->get();

//        $comment_view = comment::all()->where('blog_id','=',$id,'&&','status','=','1');
        return view('blogdetails',compact('blogs','comment_view','recent_posts'));
    }








    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

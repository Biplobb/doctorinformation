<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Seaker;

class BloodSeakerController extends Controller
{
    public function index()
    {
    }


    public function create()
    {
    }


    public function store(Request $request)
    {
             $data=['name'=>$request->name,
            'address' => $request->address,
            'hname' => $request->hname,
            'haddress' => $request->haddress,
            'relationship' => $request->relationship,
            'phone' => $request->phone,
            'email' => $request->email,
            'donor_id' => $request->donor_id

    ];

        Seaker::create($data);
        return redirect('/blood')->with('message','Your Request has been Placed Successfully');


    }


}

<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class medicaltourism extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }
    public function index()
    {
        $Hospital=DB::table('medicaltourism')->get();
        return view('admin.medicaltourism.index',compact('Hospital'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        {
            return view('admin.medicaltourism.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!empty($request->hasFile('photo')) & !empty($request->service))
        {
            $destinationPath="image/medicaltourism-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));



            if ($success) {

                DB::table('medicaltourism')->insert(['hospital'=>$request->hospital,'country'=>$request->country,'branch'=>$request->branch,'address'=>$request->address,'overview'=>$request->overview,'service'=>$request->service,'procedures'=>$request->procedures,'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
                return redirect('admin/tourism/index/');
            }
        }


        elseif(!empty($request->hasFile('photo')))
        {
            $destinationPath="image/medicaltourism-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));

            if ($success) {
                DB::table('medicaltourism')->insert(['hospital'=>$request->hospital,'country'=>$request->country,'branch'=>$request->branch,'address'=>$request->address,'overview'=>$request->overview,'procedures'=>$request->procedures,'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
                return redirect('admin/tourism/index/');
            }


        }


    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id


     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Hospital= DB::table('medicaltourism')
            ->where('id', $id)
            ->first();
        return view('admin.medicaltourism.edit')->with('Hospital', $Hospital);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldimage = DB::table('medicaltourism')->select('photo')->find($id);



        if (!empty($request->hasFile('photo')) & !empty($request->service)) {


            $file = $request->photo;
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            Image::make($file)->resize(800, 400)->save($file->move('image/medicaltourism-photo/', $filename));
            $photo = $filename;
            $filename = ($photo);

            //Storage::delete('hospital-photo/'.$oldimage->photo);

            $data=[
                'hospital'=>$request->hospital,
                'country'=>$request->country,
                'branch'=>$request->branch,
                'address'=>$request->address,

                'overview'=>$request->overview,

                'service'=>$request->service,

                'procedures'=>$request->procedures,
                'photo'=>$filename];

            DB::table('medicaltourism')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/tourism/index/');
        }
        elseif (!empty($request->service)) {



            $data=[
                'hospital'=>$request->hospital,
                'country'=>$request->country,
                'branch'=>$request->branch,
                'address'=>$request->address,

                'overview'=>$request->overview,

                'service'=>$request->service,

                'procedures'=>$request->procedures,


                'photo' => $oldimage->photo
            ];

            DB::table('medicaltourism')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/tourism/index/');
        } else {
            $data=[
                'hospital'=>$request->hospital,
                'country'=>$request->country,
                'branch'=>$request->branch,
                'address'=>$request->address,

                'overview'=>$request->overview,

                'service'=>$request->service,

                'procedures'=>$request->procedures,



                'photo' => $oldimage->photo,
            ];
            DB::table('medicaltourism')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/tourism/index/');
        }



    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('medicaltourism')
            ->where('id', $id)
            ->delete();


        return redirect('admin/tourism/index/');
    }
}

<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use App\diagnosticcenters;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Request;


class DiagnosticCenterController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }

    public function index()
    {
          $diagnostic_center=DB::table('diagnostic_center')->get();

       return view('admin.diagnostic.diagnostic-center.index',compact('diagnostic_center'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.diagnostic.diagnostic-center.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if($request->hasFile('photo'))
        {
            $destinationPath="image/diagnostic-photo";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
            $filename=rand(111111,999999).".".$extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
        }
        $data=['name'=>$request->name,
            'location'=>$request->location,
            'about'=>$request->about,

            'photo'=>$filename];
        if ($success) {

            DB::table('diagnostic_center')->insert(['name'=>$request->name,'about'=>$request->about, 'location'=>$request->location,'photo'=>$filename,'created_at'=>now(),'updated_at'=>now()]);
            return redirect('admin/diagnostic/diagnostic-center/');
        }



        /*$data = array();
         $data['name'] = $request->name;
         $data['location'] = $request->location;


       DB::table('diagnostic_center')->insert($data);
         return redirect('admin/diagnostic/diagnostic-center/');*/




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diagnostic_center= DB::table('diagnostic_center')
            ->where('id', $id)
            ->first();

        return view('admin.diagnostic.diagnostic-center.edit')->with('diagnostic_center', $diagnostic_center);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $oldimage = DB::table('diagnostic_center')->select('photo')->find($id);



        if ($request->hasFile('photo')) {


            $file = $request->photo;
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            Image::make($file)->resize(800,400)->save($file->move('image/diagnostic-photo/', $filename));
            $photo = $filename;
            $filename = ($photo);
            Storage::delete('diagnostic-photo/' . $oldimage->photo);
            $data = ['name' => $request->name,
                'location' => $request->location,
                'about' => $request->about,

                'photo' => $photo];

            DB::table('diagnostic_center')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/diagnostic/diagnostic-center/');
        } else {
             $data = ['name' => $request->name,
                'location' => $request->location,
                'about' => $request->about,
                'photo' => $oldimage->photo];
            DB::table('diagnostic_center')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/diagnostic/diagnostic-center/');
        }



        }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        DB::table('diagnostic_center')
            ->where('id', $id)
            ->delete();


        return redirect('admin/diagnostic/diagnostic-center/');
    }
}

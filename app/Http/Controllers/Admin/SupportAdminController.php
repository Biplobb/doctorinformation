<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class SupportAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }

    public function showform()
    {
        return view('admin.supportadmin.showaddform');
    }

    public function addnewsupportadmin(Request $request)
    {



        $existemail=DB::table('admins')->select('email')->where('email','=',$request->email)->first();
        if(sizeof($existemail)==0)
        {
            if($request->name!='' && $request->email!=''&& $request->password!='')
            {
                if($request->hasFile('photo')) {
                    $file = $request->photo;
                    $extention = $file->getClientOriginalExtension();
                    $filename = rand(111111, 999999) . "." . $extention;
                    Image::make($file)->resize(200, 200)->save($file->move('image/admin-photo/', $filename));
                    $photo = $filename;
                    $filename = ($photo);

                    DB::table('admins')->insert(['name' => $request->name, 'email' => $request->email, 'phone' => $request->phone, 'nid' => $request->nid, 'photo' => $photo, 'password' => bcrypt($request->password), 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);

                    $lastid=DB::table('admins')->select('id')->orderBy('created_at','desc')->first();

                    return redirect(route('addrole',['id' => $lastid->id]));
                }
            }
            return redirect()->back()->with('warning','Fill up required filed !!');

        }
    }

    public function addrole($id)
    {

        $roles=DB::table('roles')->select('id','name')->get();

        if($id==''){
            return response('Invalid request !!');
        }else{
            $userInfo=DB::table('admins')->select('id','name')->where('id','=',$id)->first();
            if(sizeof($userInfo)==0)
            {
                return response('Invalid request !!');
            }
            return view('admin.supportadmin.addrole',compact(['roles','userInfo']));
        }
    }

    public function submitRole(Request $request)
    {
       $olddata=DB::table('admin_role')->select('id')->where('admin_id','=',$request->admin_id)->where('role_id','=',$request->role_id)->first();
       if(sizeof($olddata)==0)
       {
           DB::table('admin_role')->insert(['admin_id' => $request->admin_id,'role_id' => $request->role_id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
           return redirect()->back()->with('success','Role assigned !!');
       }
        return redirect()->back()->with('exist','Role already assigned !!');
    }

    public function manage()
    {
        $data=DB::table('admin_role')->select('admins.name as name','admins.id as admin_id','roles.id as role_id','photo','email','phone','nid')
            ->leftJoin('admins','admins.id','admin_role.admin_id')
            ->leftJoin('roles','roles.id','admin_role.role_id')
            ->where('roles.name','=','supportadmin')
            ->get();

        $roles=DB::table('roles')->select('name','id')->get();

        return view('admin.supportadmin.manage',compact('data','roles'));
    }

    public function editadmininfo($id)
    {
        $data=DB::table('admin_role')->select('admins.name as name','admins.id as admin_id','roles.id as role_id','roles.name as role_name','roles.id as role_id','photo','email','phone','nid')
            ->leftJoin('admins','admins.id','admin_role.admin_id')
            ->leftJoin('roles','roles.id','admin_role.role_id')
            ->where('admins.id','=',$id)
            ->first();
        return view('admin.supportadmin.editadmininfo',compact('data'));
    }


    public function updateData(Request $request)
    {
        if($request->name!='' && $request->email!='' && $request->admin_id!=''){

        $data=['name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            ];
        DB::table('admins')
            ->where('id', $request->admin_id)
            ->update($data);

        DB::table('admin_role')->where('admin_id',$request->admin_id)->update(['role_id'=>$request->role_id]);
        return redirect()->back()->with('updated','Info updated !!');

        }
        return redirect()->back()->with('failed','Provide info correctly !!');

    }
    public function deleteAdminData($id)
    {
//        DB::table('admins')->where('id', $id)->delete();
        Admin::destroy($id);
        return redirect()->back()->with('ddeleted','Info deleted !!');
    }


}

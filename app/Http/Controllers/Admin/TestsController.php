<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\tests;
use Illuminate\Http\Request;
use DB;
use Redirect;
use Session;


class TestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

   /* public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }*/

    public function index()
    {
        $tests=tests::all();
        return view('admin.diagnostic.tests.index',compact('tests'));
    }
    public function testsindex()
    {
        $tests=tests::all();
        return view(' diagnostic-admin.tests.index',compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.diagnostic.tests.create');
    }
    public function testscreate(Request $request)
    {
        $re =DB::table('diagnostic_center')->select('id')->where('id','=',$request->id)->first();;

        return view( 'diagnostic-admin.tests.create',compact('re'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $re =DB::table('diagnostic_center')->select('id')->where('id','=',$request->id)->first();;

        tests::create($request->all());
        session()->flash('message','succesfully inserted');
        return redirect('admin/diagnostic/tests/');
    }
    public function testsstore(Request $request)
    {

        $re =DB::table('diagnostic_center')->select('id')->where('id','=',$request->id)->first();


        $tests=tests::all();
        session()->flash('message','succesfully inserted');
        return view('diagnostic-admin/tests/index',compact('re','tests'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tests=tests::find($id);
        return view('admin.diagnostic.tests.edit',compact('tests'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tests=tests::find($id);
        $tests->update( $request->all());
        return redirect('/admin/diagnostic/tests/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       tests::destroy($id);
        session()->flash('message','successfully deleted!!!!!');
        return redirect('/admin/diagnostic/tests/');
    }
}

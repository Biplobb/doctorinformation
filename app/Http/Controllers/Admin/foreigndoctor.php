<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class foreigndoctor extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctor=DB::table('foreigndoctor')->select('*')->get();
        return view('admin.foreigndoctor.index',compact('doctor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getforeigndoctor($id)
    {
        $doctor=DB::table('foreigndoctor')->where('id','=',$id)->select('name','qualification','photo','overview')->get()->first();


        $data=DB::table('foreigndoctor_medicaltourism')
            ->leftJoin('medicaltourism','foreigndoctor_medicaltourism.medicaltourism_id','=','medicaltourism.id')
            ->leftJoin('foreigndoctor','foreigndoctor_medicaltourism.foreigndoctor_id','=','foreigndoctor.id')
            ->where('foreigndoctor_medicaltourism.foreigndoctor_id','=',$id)
            ->select('medicaltourism.hospital as hospitalname','medicaltourism.id as hospitalid','medicaltourism.country as hospitalcountry','medicaltourism.branch as hospitalbranch',
                'medicaltourism.address as hospitaladdress','medicaltourism.description as hospitaldescription','medicaltourism.overview as hospitaloverview',
                'medicaltourism.accredition as hospitalaccredition','medicaltourism.service as hospitalservice','foreigndoctor.name as foreigndoctorname','foreigndoctor.speciality as foreigndoctorspeciality','foreigndoctor.qualification as foreigndoctorqualification','foreigndoctor.overview  as foreigndoctoroverview'
              )
            ->distinct()
            ->get();

        if(sizeof($data)==0){
            return view('404');
        }
        return view('foreigndoctor',compact('doctor','data'));
    }

     public function create()
    {
        $data=DB::table('medicaltourism')->select('id','hospital')->get();
        return view('admin.foreigndoctor.create',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $id='';
        if($request->hasFile('photo'))
        {
            $destinationPath="image/foreign-doctor";
            $file=$request->file('photo');
            $extention=$file->getClientOriginalExtension();
             $filename=rand(111111,999999).".".$extention;
             $photos=$filename;
           Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
           }

        $data=['name'=>$request->name,
            'qualification'=>$request->qualification,
            'speciality'=>$request->speciality,
            'overview'=>$request->overview,
            'email'=>$request->email,
            'password'=>$request->password,
            'photo'=>$photos,
             'created_at'=>now(),
             'updated_at'=>now()];

        if( DB::table('foreigndoctor')->insert($data));
        {
       $id = DB::getPdo()->lastInsertId();

             for($i=0;$i<sizeof($request->hospital);$i++)
            {
                DB::table('foreigndoctor_medicaltourism')->insert(['foreigndoctor_id'=>$id,'medicaltourism_id'=>$request->hospital[$i],'assigned'=>'no','first_fees'=>null,'second_fees'=>null,'created_at'=>Carbon::now(),'updated_at'=>Carbon::now()]);
            }
        }
        return redirect('admin/foreigndoctorindex');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $singledoctor= DB::table('foreigndoctor')
            ->where('id', $id)
            ->first();



        return view('admin.foreigndoctor.edit',compact('singledoctor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldimage = DB::table('foreigndoctor')->select('photo')->find($id);



        if ($request->hasFile('photo')) {


            $file = $request->photo;
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            Image::make($file)->resize(800, 400)->save($file->move('image/foreign-doctor/', $filename));
            $photo = $filename;
            $filename = ($photo);
            //Storage::delete('hospital-photo/'.$oldimage->photo);
            $data=['name'=>$request->name,
                'qualification'=>$request->qualification,
                'speciality'=>$request->speciality,
                'overview'=>$request->overview,
                'email'=>$request->email,
                'password'=>$request->password,
                'password'=>$request->password,
                'photo'=>$filename];

            DB::table('foreigndoctor')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/foreigndoctorindex');
        } else {
            $data=['name'=>$request->name,
                'qualification'=>$request->qualification,
                'speciality'=>$request->speciality,
                'overview'=>$request->overview,
                'email'=>$request->email,
                'password'=>$request->password,
                'photo' => $oldimage->photo];
            DB::table('foreigndoctor')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/foreigndoctorindex');
        }



    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('foreigndoctor')
            ->where('id', $id)
            ->delete();


        return redirect('admin/foreigndoctorindex');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\DcotorSchedule;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DoctorScheduleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }



    public function create($id)
    {
        $days=DB::table('days')->select('id','day_name')->get();
        $doctors=DB::table('doctor_profiles')->select('id','name')->where('id','=',$id)->get()->first();

        $hospital=DB::table('doctor_hospitals')
        ->leftJoin('doctor_profiles','doctor_profiles.id','doctor_hospitals.doctor_id')
        ->leftJoin('hospital','hospital.id','doctor_hospitals.hospital_id')
        ->where('assigned','=','no')
        ->where('doctor_id','=',$id)
        ->select('hospital.id as hospitalid','hospital.name as hospitalname')
        ->get();

//        return redirect(route('doctorschedule',[['id' => $id]]));

        return view('admin/doctor/schedule',compact('days','doctors','hospital'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start=$request->input('start');
        $end=$request->input('end');
        $day=$request->input('day');
        $hospital_id=$request->input('hospital_id');
        $intr=$request->input('interval');
        $doctor_id=$request->input('doctor_id');

        for($i=0;$i<sizeof($day);$i++){

            if($start[$day[$i] - 1]!='' && $end[$day[$i] - 1]!='' && $intr[$day[$i] - 1]!='') {

                $data = count(DcotorSchedule::where('doctor_id', '=', $doctor_id)
                    ->where('hospital_id', '=', $hospital_id)
                    ->where('day_id', '=', $day[$i])
                    ->where('day_id', '=', $day[$i])
                    ->where('start', '=', $start[$day[$i] - 1])
                    ->where('end', '=', $end[$day[$i] - 1])
                    ->select('doctor_id', 'hospital_id', 'day_id')
                    ->get());
                if ($data != 0) {
                    return redirect(route('doctorschedule'))->with('dataexist', 'Data already exist !!');
                } else {
                    DB::table('doctor_hospitals')->where('doctor_id', '=', $doctor_id)->where('hospital_id', '=', $hospital_id)->update(['assigned' => 'yes', 'first_fees' => $request->first_fees, 'second_fees' => $request->second_fees]);
                    DcotorSchedule::create(['doctor_id' => $doctor_id, 'hospital_id' => $hospital_id, 'day_id' => $day[$i], 'interval' => $intr[$day[$i] - 1], 'start' => $start[$day[$i] - 1], 'end' => $end[$day[$i] - 1], 'available' => 'yes']);

                }
            }
            else{
                return redirect()->back()->with('errors','Give information properly');
            }
        }
        return redirect(route('doctorschedule',['id' => $doctor_id]))->with('scheduleadded','Schedule added successfully');
    }


    public function show($id)
    {

    }


    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function viewallSchedule()
    {
        $data=DcotorSchedule::select('dcotor_schedules.id as schid','dcotor_schedules.id','doctor_id','doctor_profiles.name as doctorname','hospital.name as hospitalname','day_name','start','end','interval')
        ->leftJoin('doctor_profiles','doctor_profiles.id','=','dcotor_schedules.doctor_id')
        ->leftJoin('hospital','hospital.id','=','dcotor_schedules.hospital_id')
        ->leftJoin('days','days.id','=','dcotor_schedules.day_id')
        ->orderBy('day_name','asc')
        ->get();
        return view('admin/doctor/viewalldoctorschedule',compact('data'));
    }

    public function editSchedule($scheduleid,$doctorid)
    {
        $data=DcotorSchedule::select('dcotor_schedules.id as id','start','end','interval','day_id','hospital.name as hospitalname','available')
            ->leftJoin('hospital','hospital.id','=','dcotor_schedules.hospital_id')
            ->where('dcotor_schedules.id','=',$scheduleid)
            ->where('doctor_id','=',$doctorid)
            ->get()
            ->first();
        if(sizeof($data)==0){
            return "No data found";
        }else{
            return view('admin/doctor/updateschedule',compact('data'));
        }
    }
    public function deleteschedule($id)
    {
        DB::table('dcotor_schedules')->where('id', '=', $id)->delete();
        return redirect()->back()->with('deleted','Schedule deleted');

    }

    public function updateSchedule(Request $request)
    {
        DB::table('dcotor_schedules')->where('id','=',$request->scheduleid)
            ->update(['start'=>$request->start,'end'=>$request->end,'day_id'=>$request->day,'available'=>$request->available,'interval'=>$request->interval]);
        return redirect()->back()->with('updated','Schedule updated');
    }
}

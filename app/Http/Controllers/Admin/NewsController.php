<?php

namespace App\Http\Controllers\Admin;

use App\news;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }


    public function index()
    {
        $news= news::all();
        return view('admin.news.index',compact('news'));
    }


    public function create()
    {
        $news=news::all();

        return view('admin.news.create',compact('news'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('image') && $request->file('image')->extension()==('jpeg'||'png')){
            $destinationPath="image/news-photo";
            $file=$request->image;
            $extention=$file->getClientOriginalExtension();
            $filename=rand(1111111,9999999).".".$extention;
            $file->move($destinationPath,$filename);
            $photo=$filename;

            news::create(['news_title'=>$request->news_title,'author'=>$request->author,'news_post'=>$request->news_post,'image'=>$photo]);
            return redirect('admin/news/');
        }else {
            echo "no";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news= news::find($id);
        return view('admin.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news=news::select('id','news_title','author','news_post','image')->where('id','=',$id)->get()->first();
        return view('admin.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $oldimage = DB::table('news')->select('image')->find($id);

        if ($request->hasFile('image')) {

            $destinationPath = "image/news-photo";
            Storage::delete('news-photo/' . $oldimage->image);
            $file = $request->image;
            $extention = $file->getClientOriginalExtension();
            $filename = rand(1111111, 9999999) . "." . $extention;
            $file->move($destinationPath, $filename);
            $photo = $filename;
            $filename = ($photo);

            $data = ['news_title' => $request->news_title,
                'author' => $request->author,
                'news_post' => $request->news_post,
                'image' => $photo];
            DB::table('news')
                ->where('id', $id)
                ->update($data);
            return redirect('/admin/news/');
        } else {

            $data = ['news_title' => $request->news_title,
                'author' => $request->author,
                'news_post' => $request->news_post,
                'image' => $oldimage->image];
            DB::table('news')
                ->where('id', $id)
                ->update($data);
            return redirect('/admin/news/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        news::destroy($id);
        session()->flash('message','successfully deleted!!!');
        return redirect(('admin/news/'));
    }
}

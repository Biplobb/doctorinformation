<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    public function index()
    {
        return view('admin.equipment.index');
    }

    public function create()
    {
        return view('admin.equipment.create');
    }

    public function show()
    {
        return view('admin.equipment.show');
    }
}

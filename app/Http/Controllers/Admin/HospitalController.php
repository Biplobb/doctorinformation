<?php

namespace App\Http\Controllers\Admin;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('auth:admin');
        // $this->middleware('roles');
    }

    public function index()
    {

        $Hospital=DB::table('hospital')->get();
        return view('admin.hospital.index',compact('Hospital'));

      /*  return view('admin.hospital.index',compact('hospital_service'));*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
return view('admin.hospital.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {

        $data = array();



        $data['name'] = $request->name;

        $data['email'] = $request->email;
        $data['password'] = $request->password;
        $data['location'] = $request->location;

        $data['service'] =implode(',', $request->service);
        $data['about'] = $request->about;
        $data['review'] = $request->review;





        $image = $request->file('image');


        if ($image) {

            $destinationPath = "image/hospital-photo";
            $file = $request->file('image');
            $extention = $file->getClientOriginalExtension();
            $filename = substr(md5(time()),'0','6') . "." . $extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));

            if ($success) {

                $data['image'] = $filename;
                DB::table('hospital')->insert($data);
                Session::put('message', 'Successfully Diagnostic center    !');

                return redirect()->back()->with('success', 'Thank you !! Your request is recorded, We will contact with you soon.');
            }
            else {
                return redirect()->back();
            }

        }


        return redirect()->back();





    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Hospital= DB::table('hospital')
            ->where('id', $id)
            ->first();

        return view('admin.hospital.edit')->with('Hospital', $Hospital);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $oldimage = DB::table('hospital')->select('photo')->find($id);



        if ($request->hasFile('photo')) {


            $file = $request->photo;
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            Image::make($file)->resize(800, 400)->save($file->move('image/hospital-photo/', $filename));
            $photo = $filename;
            $filename = ($photo);
            //Storage::delete('hospital-photo/'.$oldimage->photo);
            $data=['name'=>$request->name,
                'location'=>$request->location,
                'service'=>implode(',',$request->service),
                'about'=>$request->about,
                'contact'=>$request->contact,
                'review'=>$request->review,

                'photo'=>$filename];

            DB::table('hospital')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/hospital/index/');
        } else {
            $data=['name'=>$request->name,
                'location'=>$request->location,
                'service'=>implode(',',$request->service),
                'about'=>$request->about,
                'contact'=>$request->contact,
                'review'=>$request->review,

                'photo' => $oldimage->photo];
            DB::table('hospital')
                ->where('id', $id)
                ->update($data);
            return redirect('admin/hospital/index/');
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('hospital')
            ->where('id', $id)
            ->delete();


        return redirect('admin/hospital/index');
    }
}

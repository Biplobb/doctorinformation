<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Donar;

class BloodController extends Controller
{
    public function index()
    {
        $donars = Donar::select('id','name','address','phone','date','blood_group','email','area','upozila','status')
            ->where('status', '=', '1')->paginate(10);
        $district=DB::table('districts')->get();
        $upozila=DB::table('upazilas')->get();
        return view('admin.blood.index',compact('donars','district','upozila'));


    }

    public function create()
    {
        $district=DB::table('districts')->get();
        $upozila=DB::table('upazilas')->get();
        return view('admin.blood.create',compact('district','upozila'));;
    }
    public function store(Request $request)
    {

        $data=['name'=>$request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'date' => $request->date,
            'blood_group' => $request->blood_group,
            //'gender' => $request->gender,
            'email' => $request->email,
            'area' => $request->area,
            'upozila' => $request->upozila,
           'status' => $request->status];



        Donar::create($data);
        return redirect('/admin/blood/index')->with('message','Inseart Successfully') ;


    }
    public function show($id)
    {
        $donar = Donar::find($id);
        return view('/admin/blood/show', compact('donar'));
    }









    public function edit($id)
    {
        $donar=Donar::find($id);


        $district=DB::table('districts')->get();
        $upozila=DB::table('upazilas')->get();


        return view('/admin/blood/edit',compact('donar','district','upozila'));
    }
    public function update(Request $request, $id)
    {


        $donar=Donar::find($id);
        $donar->update($request->all());
        return redirect('admin/blood/index');
    }
    public function delete($id)
    {
        Donar::destroy($id);

        session()->flash('message','successfully Deleted !!!!');
        return redirect('admin/blood/index');
    }

}






<?php

namespace App\Http\Controllers\Doctor;

use App\DoctorProfile;
use App\DoctorRequestToJoin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Redirect;
use Session;
use Carbon;

class DoctorController extends Controller
{

    public  function __construct()
    {
        $this->middleware('auth:doctor')->except('join');
    }

    public function index()
    {
        $doctorprofile=DB::table('doctor_profiles')
            ->where('doctor_profiles.id','=',Auth::guard('doctor')->user()->id)
            ->select('name','id','email','speciality','phone','address','speciality','member','awards','experience','photo')
            ->get()
            ->first();
        return view('doctor/dashboard',compact('doctorprofile'));
    }

    public function doctorchamber()
    {
        $data=DB::table('dcotor_schedules')
            ->leftJoin('doctor_profiles','doctor_profiles.id','dcotor_schedules.doctor_id')
            ->leftJoin('hospital','hospital.id','dcotor_schedules.hospital_id')
            ->leftJoin('days','days.id','dcotor_schedules.day_id')
            ->where('doctor_profiles.id','=',Auth::guard('doctor')->user()->id)
            ->select('dcotor_schedules.id as scheduleid','hospital.name as chamber','hospital.id as hospitalid','location','start','end','day_name','days.id as day_id','available')
            ->get();
        return view('doctor.doctorchamber',compact('data'));
    }



    public function updateschedulebydoctor(Request $request)
    {
        DB::table('dcotor_schedules')->where('id','=',$request->scheduleid)
            ->update(['start'=>$request->start,'end'=>$request->end,'day_id'=>$request->day,'available'=>$request->available]);
        return redirect()->back()->with('updated','Schedule updated');
    }

    public function update(Request $request)
    {
        DB::table('doctor_profiles')->where('id','=',Auth::guard('doctor')->user()->id)
            ->update(['name'=>$request->name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'address'=>$request->address,
                'member'=>$request->member,
                'awards'=>$request->awards,
                'experience'=>$request->experience,
            ]);
        return redirect()->back()->with('updated','Profile updated');
    }

    public function join(Request $request)
    {

        $id = '';


        if ($request->hasFile('image')) {
            $destinationPath = "image/doctor-photo";
            $file = $request->file('image');
            $extention = $file->getClientOriginalExtension();
            $filename = rand(111111, 999999) . "." . $extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));

        }

        $data = ['name' => $request->name,
            'designation' => $request->designation,
            'gender' => $request->gender,
            'location' => $request->location,

            'nid' => $request->nid,
            'password' =>$request->password,
            'experience' => $request->experience,
            'email' => $request->email,

            'speciality' => $request->speciality,
            'phone' => $request->phone,
            'degree' => $request->degree,
            'interest'=>implode(',',$request->interest),

            'image' => $filename];



        if ( DB::table('Doctor_Profiles')->insert($data)) {
            $id = DoctorProfile::select('id')->orderBy('created_at', 'desc')->get()->first();
            for ($i = 0; $i < sizeof($request->hospital); $i++) {
                DB::table('doctor_hospitals')->insert(['doctor_id' => $id->id, 'hospital_id' => $request->hospital[$i], 'assigned' => 'no', 'first_fees' => null, 'second_fees' => null ]);
            }
        }
        return redirect()->back();


        /*  if ($success) {

              $data['image'] = $filename;
              DB::table('Doctor_Profiles')->insert($data);
              Session::put('message', 'Successfully Doctor Profiles     !');

              return redirect()->back()->with('success', 'Thank you !! Your request is recorded, We will contact with you soon.');
          }
          else {
              return redirect()->back();
          }

      }


      return redirect()->back();




  }*/

    }

}

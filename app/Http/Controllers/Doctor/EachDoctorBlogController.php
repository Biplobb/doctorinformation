<?php

namespace App\Http\Controllers\Doctor;

use App\blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EachDoctorBlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:doctor');
    }

    public function index(){
        return view('doctor/writeblog');
    }

    public function store(Request $request)
    {

        if($request->hasFile('image') && $request->file('image')->extension()==('jpeg'||'png')){
            $destinationPath="image/blog-photo";
            $file=$request->image;
            $extention=$file->getClientOriginalExtension();
            $filename=rand(1111111,9999999).".".$extention;
            $file->move($destinationPath,$filename);
            $photo=$filename;

            blog::create(['title'=>$request->title,'subtitle'=>$request->subtitle,'author'=>Auth::guard('doctor')->user()->name,'blog_post'=>$request->blog_post,'type'=>$request->type,'image'=>$photo,'doctor_id'=>Auth::guard('doctor')->user()->id]);
            return redirect()->back()->with('postSuccess','Blog Posted');
        }else {
            echo "no";
        }

    }

    public function myblogs()
    {
        $blog= blog::find(Auth::guard('doctor')->user()->id);
        return view('doctor.myblogs',compact('blog'));
    }
}

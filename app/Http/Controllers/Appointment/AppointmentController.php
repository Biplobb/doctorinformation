<?php

namespace App\Http\Controllers\Appointment;

use App\Appointment;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AppointmentController extends Controller

{


    public function __construct()
    {
        $this->middleware('auth:admin')->except(['bookAppointment','appointmentconfirmation']);
    }

    public function bookAppointment(Request $request)
    {
        $data=Appointment::where('phone','=',$request->phone)->where('date','=',$request->apptDate)->select('phone','date')->get();
        if(sizeof($data)==0){
            if($request->doctor_id!='' && $request->hospital_id!='' && $request->name !='' && $request->address != '' && $request->phone != '' && $request->times!='' && $request->apptDate!='' && $request->dayOfWeek!=''){
                $d=Appointment::select('phone')->where('phone','=',$request->phone)->first();
                if(sizeof($d)==1){
                    Appointment::create(['doctor_id' => $request->doctor_id, 'hospital_id' => $request->hospital_id, 'name' => $request->name, 'address' => $request->address, 'phone' => $request->phone, 'email'=>$request->email,'time' => $request->times, 'date' => $request->apptDate, 'day' => $request->dayOfWeek,'appointment_round'=>'old','status'=>'not']);
//                    return redirect('confirmation')->with('oldperson','Thank you for being with us once again!!');
                     return redirect()->back()->with('oldperson','Thank you for being with us once again!!');
                }else{
                    Appointment::create(['doctor_id' => $request->doctor_id, 'hospital_id' => $request->hospital_id, 'name' => $request->name, 'address' => $request->address, 'phone' => $request->phone, 'email'=>$request->email,'time' => $request->times, 'date' => $request->apptDate,'appointment_round'=>'new', 'day' => $request->dayOfWeek,'status'=>'not']);
//                    return redirect('confirmation')->with(['newperson'=>'Thank you for being with us','appintment'=>'Your appointment has been recorded !!','message'=>'We will contact with you soon']);
                    return redirect()->back()->with(['newperson'=>'Thank you for being with us','appintment'=>'Your appointment has been recorded !!','message'=>'We will contact with you soon.']);
                }
            }else{
                return redirect()->back()->with('wornginfor','Please provide valid information !!');
            }
        }else{
            return redirect()->back()->with('alreadydone','Appointment already recorde d!!');
        }

    }

    public function getallAppointment()
    {


        $data=Appointment::leftJoin('doctor_profiles','appointments.doctor_id','doctor_profiles.id')
        ->leftJoin('hospital','appointments.hospital_id','hospital.id')
        ->where('status','=','not')
        ->select('appointments.id as apptid','doctor_profiles.name as doctor_name','hospital.name as hospital_name','day','date','time','appointments.name','appointments.address','appointments.phone')
        ->orderBy('appointments.created_at','desc')
        ->get();


        return view('admin.appointment.allappointment',compact('data'));
    }

    public function appointmentconfirmation()
    {
        return view('confirmation');
    }

    public function confirmAppointment(Request $request)
    {
        DB::table('appointments')
            ->where('id', $request->apptid)
            ->update(['status' => 'yes']);
    }

    public function deleteAppointment(Request $request)
    {
        Appointment::where('id', $request->apptid)->delete();
    }

    public function appointmenttrash()
    {
        $data=Appointment::leftJoin('doctor_profiles','appointments.doctor_id','doctor_profiles.id')
            ->leftJoin('hospital','appointments.hospital_id','hospital.id')
            ->where('status','=','not')
            ->select('appointments.id as apptid','doctor_profiles.name as doctor_name','hospital.name as hospital_name','day','date','time','appointments.name','appointments.address','appointments.phone')
            ->orderBy('deleted_at','desc')
            ->onlyTrashed()->get();
        return view('admin.appointment.appointmenttrash' , compact('data'));
    }

    public function permanentdelete(Request $request)
    {
        if( Appointment::where('id',$request->apptid)->withTrashed()->forceDelete()){
//            $data=Appointment::select('appointments.id as apptid','doctor_profiles.name as doctor_name','hospital.name as hospital_name','day','date','time','appointments.name','appointments.address','appointments.phone')
//                ->leftJoin('doctor_profiles','appointments.doctor_id','doctor_profiles.id')
//                ->leftJoin('hospital','appointments.hospital_id','hospital.id')
//                ->where('status','=','not')
//                ->orderBy('deleted_at','desc')
//                ->onlyTrashed()->get();
            return redirect()->back()->with('permanentdelete','Data deleted');
//            return view('admin.appointment.appointmenttrash',compact('data'))->with('permanentdelete','Data deleted');
        }



    }
}

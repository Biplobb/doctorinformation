<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use DB;
use Redirect;
use Session;


class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

         return view('userregister');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function joinuser(Request $request)
    {


        $data = array();


                $data['name'] = $request->name;
                $data['userid'] =substr(md5(time()),'0','6');
                $data['password'] = $request->password;
                $data['present'] = $request->present;
                $data['permanent'] = $request->permanent;
                $data['contact'] = $request->contact;
                $data['occupation'] = $request->occupation;
                $data['code'] = substr(md5(time()),'0','5');


                $data['email'] = $request->email;
                $data['gender'] = $request->gender;

                $image = $request->file('image');









                if ($image) {

                    $destinationPath = "image/patient";
                    $file = $request->file('image');
                    $extention = $file->getClientOriginalExtension();
                    $filename = substr(md5(time()),'0','6') . "." . $extention;

                    $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));
                    if ($success) {

                        $data['image'] = $filename;
                        DB::table('user_request_to_join')->insert($data);
                        Session::put('message', 'Successfully patient information inserted     !');

                        return redirect()->back()->with('success', 'Thank you !! ');
                    }
                    else {
                        return redirect()->back();
                    }

                }


        return redirect()->back();




            }





    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function patientjoinlogins(Request $request)
    {

        $email = $request->email;
        $password = $request->password;

        $result = DB::table('user_request_to_join')
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        $re= DB::table('user_request_to_join')->select('id')
            ->where('email', $email)
            ->where('password', $password)
            ->first();


        if($result){

            Session::put('id',$result->id);

            return view('patient-admin.index',compact('re'));
            /*return Redirect::to('/admin');*/
        }else{
            Session::put('exception','Email or Password Invalid');
            return redirect()->back();
        }
    }
 public function addpatientinformation(Request $request)
    {

        $re= DB::table('user_request_to_join')->select('id')
            ->where('id', $request->id)

            ->first();

         $p=$request->id;
        return view('patient-admin-form.create',compact('p','re'));
    }
    public function patientallinformation(Request $request)
    {

        $ids=$request->id;
         $re=$request->id;
         $patient=DB::table('user_request_to_join')->where('id','=',$ids)->select('name','present','permanent','contact','occupation','gender','image')->get();




         $data=DB::table('patientinformation')
            ->leftJoin('user_request_to_join','patientinformation.user_id','=','user_request_to_join.id')

            ->where('patientinformation.user_id','=',$ids)
            ->select('patientinformation.prescriptionwrite as prescriptionwrite','patientinformation.report as report','patientinformation.prescription as prescription'
                )

            ->get();

        /* $data=DB::table('patientinformation')
            ->leftJoin('user_request_to_join','patientinformation.user_id','=','user_request_to_join.id')

            ->where('patientinformation.user_id','=',$ids)
            ->select('patientinformation.prescriptionwrite as prescriptionwrite','patientinformation.report as report','patientinformation.prescription as prescription',
                ' user_request_to_join.name as patientname',' user_request_to_join.userid as patientuserid',' user_request_to_join.present as patientpresent',
                'user_request_to_join.permanent as patientpermanent','user_request_to_join.contact as patientcontact','user_request_to_join.occupation as patientoccupation',
                'user_request_to_join.gender as patientgender','user_request_to_join.image as patientimage')
            ->distinct()
            ->get();*/


        return view('patient-admin.information.index',compact('patient','data','re'));

    }
    public function addpatientinformationss(Request $request)
    {


        $re= DB::table('user_request_to_join')->select('id')
            ->where('id', $request->id)

            ->first();

           $p=$request->id;
        return view('patient-admin-form.create',compact('p','re'));
    }
    public function allpatientmessage(Request $request)
    {
return 1;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function patientinformationstore(Request $request)
    {

        $data = array();


        $data['user_id'] = $request->user_id;

        $data['prescriptionwrite'] = $request->prescriptionwrite;




         $report = $request->file('report');
         $prescription	 = $request->file('prescription	');




        if ($request->hasFile('report') & $request->hasFile('prescription') ) {


            $destinationPath = "image/patient";
            $file = $request->file('report');
            $extention = $file->getClientOriginalExtension();
            $filename = substr(md5(time()),'0','6') . "." . $extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));







            if ($success) {
                $destinationPath1 = "image/patient-prescription";
                $file1 = $request->file('prescription');
                $extention1 = $file1->getClientOriginalExtension();
                $prescription = substr(md5(time()),'0','8') . "." . $extention1;
                $success1=Image::make($file1)->resize(800,400)->save($file1->move($destinationPath1,$prescription));

                $data['report'] = $filename;
                $data['prescription'] = $prescription;
                DB::table('patientinformation')->insert($data);
                Session::put('message', 'Successfully patient information inserted     !');

                return redirect()->back()->with('success', 'Thank you !! ');
            }
            else {
                return redirect()->back();
            }

        }
        elseif($request->hasFile('report')  ) {


            $destinationPath = "image/patient";
            $file = $request->file('report');
            $extention = $file->getClientOriginalExtension();
            $filename = substr(md5(time()),'0','6') . "." . $extention;
            $success=Image::make($file)->resize(800,400)->save($file->move($destinationPath,$filename));







            if ($success) {

                $data['report'] = $filename;

                DB::table('patientinformation')->insert($data);
                Session::put('message', 'Successfully patient information inserted     !');

                return redirect()->back()->with('success', 'Thank you !! ');
            }
            else {
                return redirect()->back();
            }

        }
elseif ($request->hasFile('prescription') ) {



    $destinationPath1 = "image/patient-prescription";
    $file1 = $request->file('prescription');
    $extention1 = $file1->getClientOriginalExtension();
    $prescription = substr(md5(time()),'0','8') . "." . $extention1;
    $success1=Image::make($file1)->resize(800,400)->save($file1->move($destinationPath1,$prescription));





    if ($success1) {



        $data['prescription'] = $prescription;
        DB::table('patientinformation')->insert($data);
        Session::put('message', 'Successfully patient information inserted     !');

        return redirect()->back()->with('success', 'Thank you !! ');
    }
    else {
        return redirect()->back();
    }

}
elseif (!$request->hasFile('report') & !$request->hasFile('prescription') ) {



        if ($request->prescriptionwrite) {

            $data['prescriptionwrite'] = $request->prescriptionwrite;
            DB::table('patientinformation')->insert($data);
            Session::put('message', 'Successfully patient information inserted     !');

            return redirect()->back()->with('success', 'Thank you !! ');
        }
        else {
            return redirect()->back();
        }

    }

        return redirect()->back();


    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

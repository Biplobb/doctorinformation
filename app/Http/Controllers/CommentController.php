<?php

namespace App\Http\Controllers;
use App\comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('roles');
    }

    public function index()
    {
        $comment = comment::all();
        return view('admin.blog.comment',compact('comment'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        comment::create(['blog_id'=>$request->blog_id,'name'=>$request->name,'email'=>$request->email,'comments'=>$request->comments]);
        session()->flash('message','Thanks! Your Comment has been posted Successfully. Waiting for Approval ...');
       return redirect('blogdetails/'.$request->blog_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['status'] = 1;


        DB::table('comments')
            ->where('id', $id)
            ->update($data);

        return redirect('admin/blog/comment');
    }


    public function update(Request $request, $id)
    {

//

    }

    public function destroy($id)
    {
        DB::table('comments')
            ->where('id', $id)
            ->delete();

        return redirect('admin/blog/comment');

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class adminController extends Controller
{
    public function index()
    {

        return view('admin.admin_login');
    }

    public function AdminIndex(){
        $id = Session::get('id');
        if ($id == NULL) {
            return Redirect::to('/admin-panel')->send();
        }
        return view('admin.index');
    }

    public function auth_check(Request $request){
return $request;
        $email = $request->email;
        $password = $request->password;

        $result = DB::table('tbl_admin_login')
            ->where('email', $email)
            ->where('password', $password)
            ->first();
        if($result){
            Session::put('id',$result->id);
            return Redirect::to('/admin');
        }else{
            Session::put('exception','Email or Password Invalid');
            return Redirect::to('admin-panel');
        }

    }
    public function logout() {
        Session::put('id', null);
        Session::put('message', 'You are successfully logout!!');
        return Redirect::to('/admin-panel');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

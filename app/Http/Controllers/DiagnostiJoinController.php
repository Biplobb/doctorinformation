<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use DB;
use Redirect;
use Session;

class DiagnostiJoinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function joindiagnosticcenter(Request $request)
    {


        $data = array();


        $data['name'] = $request->name;

        $data['email'] = $request->email;
        $data['password'] = $request->password;
        $data['location'] = $request->location;
        $data['about'] = $request->about;


        $image = $request->file('image');


        if ($image) {

            $destinationPath = "image/diagnostic";
            $file = $request->file('image');
            $extention = $file->getClientOriginalExtension();
            $filename = substr(md5(time()),'0','6') . "." . $extention;
            $success = $image->move($destinationPath, $filename);

            if ($success) {

                $data['image'] = $filename;
                DB::table('diagnostic_center')->insert($data);
                Session::put('message', 'Successfully Diagnostic center    !');

                return redirect()->back()->with('success', 'Thank you !! Your request is recorded, We will contact with you soon.');
            }
            else {
                return redirect()->back();
            }

        }


        return redirect()->back();





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

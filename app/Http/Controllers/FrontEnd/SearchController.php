<?php

namespace App\Http\Controllers\FrontEnd;

use App\DoctorProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function searchdoctor(Request $request){
        $option ='';
        $data=DoctorProfile::where('name','like','%'.$request->keyword.'%')->select('name','id','degree')->get();

        $option .="<ul class='datalist'>";
        if(sizeof($data)>0){
            foreach ($data as $d)
            {
                $option .="<li class='lists'  data-id='$d->id' data-name='$d->name'>".$d->name."<p>$d->degree</p></li>";
            }
        }else{
            $option .="<li>No data found</li>";
        }
        $option .="</ul>";
        return $option;
    }



    public function searchresult(Request $request)
    {

        if($request->lookingfor!='' && $request->idnt!='') {

            if ($request->lookingfor == 'blood' &&
                ($request->keyword == 'A+'
                    || $request->keyword == 'A-'
                    || $request->keyword == 'O+'
                    || $request->keyword == 'O-'
                    || $request->keyword == 'AB+'
                    || $request->keyword == 'AB-' || $request->keyword == 'B+' || $request->keyword == 'B-')
            ) {
                return redirect('blood/' . $request->keyword);
            } else if ($request->lookingfor == 'doctor' && $request->keyword != '' && $request->idnt != '') {
//            $data=DoctorProfile::select('id')
//                ->where('id','=',$request->dctid)
//                ->where('name','=',$request->keyword)->get()->first();
                return redirect(route('doctor_profile', ['id' => $request->idnt]));
            } else if ($request->lookingfor == 'hospital') {
                return redirect(route('gethospital', ['id' => $request->idnt]));
            } else {
                return view('404');
            }
        }
        else {
            return view('404');
        }
        
    }
}

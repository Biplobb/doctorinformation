<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class DoctorProfile extends Authenticatable
{

    use Uuids;
    public $incrementing = false;
    protected $guard='doctor';
    protected $fillable=['name','designation','gender','location','nid','experience','email','password','speciality','phone','degree','interest','image'];
}

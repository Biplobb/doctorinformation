<?php
/**
 * Created by PhpStorm.
 * User: Nahid Islam
 * Date: 1/22/2018
 * Time: 11:44 PM
 */

namespace App;


use Webpatser\Uuid\Uuid;

trait Uuids
{

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });
    }

}
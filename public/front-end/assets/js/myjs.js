


var base_url = window.location.origin;
var searchkey;
var keyword;
var url;


$(document).ready(function () {
    $("#lookingfor").attr("disabled", true);
    $("#keyword").attr("disabled", true);
    $("#dosearch").attr("disabled", true);

    $('#search').get(0).reset();
});
function enable_lookingfor() {
    $("#lookingfor").attr("disabled", false);
}
function enable_keyword() {
    $("#keyword").attr("disabled", false);
    $("#dosearch").attr("disabled", false);
}

$('#keyword').focus(function () {
    $('.suggestion').css({
        'display':'block'
    });
});

document.getElementById('dt').min= new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

$('#area').on('change', function() {
    if($(this).val()==''){
        $('#area').css('border-color','red');
        $("#lookingfor").attr("disabled", true);
        $("#keyword").attr("disabled", true);
        $("#dosearch").attr("disabled", true);
    }else{
        $('#area').css('border-color','');
        $("#lookingfor").val("");
        $("#lookingfor").attr("disabled", false);
    }
});






$('#lookingfor').on('change', function() {
    if($(this).val()!='' && document.getElementById('area').value!='') {
        $('#area').css('border-color','');
        $('#lookingfor').css('border-color','');
        searchkey = $(this).val();
        $('.suggestion').html('');
        $('#keyword').val('');
        if ($(this).val() == 'blood') {
            $('.suggestion').append("<ul>" +
                "<li class='lists' data-id='1' data-name='A+'>A+</li>" +
                "<li class='lists' data-id='2' data-name='A-'>A-</li>" +
                "<li class='lists' data-id='3' data-name='B+'>B+</li>" +
                "<li class='lists' data-id='4' data-name='B+'>B-</li>" +
                "<li class='lists' data-id='5' data-name='AB+'>AB+</li>" +
                "<li class='lists' data-id='6' data-name='AB-'>AB-</li>" +
                "<li class='lists' data-id='7' data-name='O+'>O+</li>" +
                "<li class='lists' data-id='8' data-name='O-'>O-</li>" +
                "</ul>");
        } else if ($(this).val() == 'doctor' ||$(this).val() == 'hospital') {
            $('.suggestion').html('');
        }
    }else{
        $('#lookingfor').css('border-color','red');
        $("#keyword").attr("disabled", true);
        $("#dosearch").attr("disabled", true);
    }
});

//  hide the suggestions
$('body').click(function(e){
    if(($(e.target).attr("id") != "keyword") ){
        $(".suggestion").hide();
    }
});

$(document).ready(function () {

    $('#keyword').keyup(function () {
         keyword = $(this).val();
        if(keyword.length>2 && searchkey=='doctor'){
            $.ajax({
                type:'post',
                url:base_url+"/sd",
                data:{
                    _token:token,
                    keyword:keyword
                },
                success:function (data) {
                    $('.suggestion').html('');
                    $('.suggestion').fadeIn();
                    $('.suggestion').append(data);
                    $('#dosearch').attr("disabled", false);
                }
            });
        }
        else if( keyword.length>2 && searchkey=='hospital')
        {
            $.ajax({
                type:'post',
                url:base_url+'/searchhospital',
                data:{
                    _token:token,
                    keyword:keyword
                },
                success:function (data) {
                    $('.suggestion').html('');
                    $('.suggestion').fadeIn();
                    $('.suggestion').append(data);
                    $('#dosearch').attr("disabled", false);
                }
            });
        }else{
            $('#idnt').val('');
        }
    });
});

$(document).on('click','.lists',function () {
    $('#keyword').val($(this).data('name'));
    $('#idnt').val($(this).data('id'));
    $('.suggestion').fadeOut();
});

function validdata() {
    if( document.getElementById('area').value=='')
    {
        alert("Invalid submission");
        return false;
    }else

    return true;
}


$(document).on('submit','#search',function (e) {
    // alert(base_url);
    e.preventDefault();
    // if (document.getElementById('idnt').value != '' && document.getElementById('area').value != ''){

    if (document.getElementById('area').value != ''){
        if (searchkey == 'doctor') {
            if(document.getElementById('idnt').value == '')
            {
                url = base_url+'/alldoctors';
                window.location.href = url;
            }else{
                url = base_url+'/profile/' + ($('#idnt').val());
                window.location.href = url;
            }

        } else if (searchkey == 'hospital') {

            if(document.getElementById('idnt').value == '')
            {
                url = base_url+'/hospitals';
                window.location.href = url;
            }else{
                url = base_url+'/hospitalshow/' + ($('#idnt').val());
                window.location.href = url;
            }

        } else if (searchkey == 'blood') {

            if(document.getElementById('idnt').value == '')
            {
                url = base_url+'/blood';
                window.location.href = url;
            }else{
                url = base_url+'/blood/' + ($('#idnt').val());
                window.location.href = url;
            }

        }
    }else {
        return false;
    }

});



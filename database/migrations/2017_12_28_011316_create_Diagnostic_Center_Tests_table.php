<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiagnosticCenterTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Diagnostic_Center_Tests', function (Blueprint $table) {

            $table->increments('id');



            $table->integer('Tests_id')->unsigned();

            $table->foreign('Tests_id')->references('id')->on('Tests');
            $table->integer('Diagnostic_Center_id')->unsigned();
            $table->string('previous');
            $table->string('discount');
            $table->string('discountprice');

            $table->foreign('Diagnostic_Center_id')->references('id')->on('Diagnostic_Center');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Diagnostic_Center_Tests');
    }
}

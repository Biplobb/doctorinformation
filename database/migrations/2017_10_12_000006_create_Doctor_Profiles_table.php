<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Doctor_Profiles', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name','25');

            $table->string('designation','25');
            $table->string('gender','25');
            $table->string('location','25');
            $table->string('nid','25');
            $table->string('doc_id','25');
            $table->string('experience','25');

            $table->string('email','50')->unique();
            $table->string('password');
            $table->string('speciality','25');
            $table->string('phone','25')->unique();
            $table->string('degree','25');





            $table->string('interest','25');

            $table->string('image','150');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Doctor_Profiles');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserRequestToJoin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('User_Request_To_Join', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('userid');
            $table->string('password');
            $table->string('present');
            $table->string('permanent');
            $table->string('contact');
            $table->string('occupation');
            $table->string('code');
            $table->string('email');
            $table->string('gender');
            $table->string('image');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role=new \App\Role();
        $role->name='supportadmin';
        $role->save();


        $role_superadmin=new \App\Role();
        $role_superadmin->name='superadmin';
        $role_superadmin->save();
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $days = [
            [
                'day_name'       => 'Saturday',
                'value'       => '6',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),
            ],
            [
                'day_name'       => 'Sunday',
                'value'       => '0',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
            [
                'day_name'       => 'Monday',
                'value'       => '1',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],

            [
                'day_name'       => 'Tuesday',
                'value'       => '2',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],
            [
                'day_name'       => 'Wednesday',
                'value'       => '3',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],

            [
                'day_name'       => 'Thursday',
                'value'       => '4',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ],

            [
                'day_name'       => 'Friday',
                'value'       => '5',
                'created_at' => Carbon::now()->toDateTimeString(),
                'updated_at' => Carbon::now()->toDateTimeString(),

            ]
        ];

        \Illuminate\Support\Facades\DB::table('days')->insert($days);
    }
}

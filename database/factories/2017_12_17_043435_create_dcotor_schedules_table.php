<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDcotorSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dcotor_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('doctor_id')->nullable();
            $table->integer('hospital_id')->unsigned();
            $table->integer('day_id')->unsigned();
            $table->integer('interval');

            $table->time('start');
            $table->time('end');
            $table->string('available');

            $table->foreign('doctor_id')->references('id')->on('Doctor_Profiles')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('hospital_id')->references('id')->on('hospital')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dcotor_schedules');
    }
}

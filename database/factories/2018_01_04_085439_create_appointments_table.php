<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('doctor_id')->nullable();
            $table->integer('hospital_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->time('time');
            $table->date('date');
            $table->string('day');
            $table->string('appointment_round');
            $table->string('status');
            $table->foreign('hospital_id')->references('id')->on('Hospital')->onDelete('cascade');
            $table->foreign('doctor_id')->references('id')->on('doctor_profiles')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title',100);
            $table->string('subtitle',200);
            $table->string('author',50);
            $table->text('blog_post');
            $table->string('type',50);
            $table->string('image',120);
            $table->uuid('doctor_id')->nullable();
            $table->foreign('doctor_id')->references('id')->on('Doctor_Profiles')->onDelete('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}

<?php
//Front end route list
Route::prefix('/')->group(function (){
    Route::get('','HomeController@index')->name('homeindex');

    Route::get('/join','HomeController@addnewdoctor')->name('addnewdoctor');
    Route::post('/join','Doctor\DoctorController@join')->name('join');

    Route::get('/joinuser','HomeController@addnewuser')->name('addnewuser');
    Route::post('/joinuser','PatientController@joinuser')->name('joinuser');




    Route::get('/addnewdiagnostic','HomeController@addnewdiagnostic')->name('addnewdiagnostic');
    Route::post('addnewdiagnostic','DiagnostiJoinController@joindiagnosticcenter')->name('storediagnostic');



    Route::get('/addnewhospital','HomeController@addnewhospital')->name('addnewhospital');
    Route::post('/addnewhospital','JoinHospitalController@store')->name('storehospital');







    Route::get('about','HomeController@about')->name('about');
    Route::get('appointment','HomeController@appointment')->name('appointment');
    Route::get('faq','HomeController@faq')->name('faq');
    Route::get('services','HomeController@services')->name('services');
    Route::get('alldoctors','HomeController@doctors')->name('alldoctors');
    Route::get('features','HomeController@features')->name('features');
    Route::get('blog','FrontBlogController@index')->name('blog');
    Route::get('blogdetails/{id}','FrontBlogController@show')->name('blogdetails');
    Route::post('blogcomment','CommentController@store')->name('blogcomment');
    Route::get('awards','HomeController@awards')->name('awards');
    Route::get('shop','HomeController@shop')->name('shop');
    Route::get('interview','HomeController@interview')->name('interview');
    Route::get('equipment','HomeController@equipment')->name('equipment');
    Route::get('news','FrontNewsController@index')->name('news');
    Route::get('newsdetails/{id}','FrontNewsController@show')->name('newsdetails');

    //Foreign Doctor profile activity
    Route::get('profiles/{id}','Admin\foreigndoctor@getforeigndoctor')->name('foreign_doctor');
    //Doctor profile activity
    Route::get('profile/{id}','Doctor\DoctorProfileController@getDoctorProfile')->name('doctor_profile');
    Route::post('getdata','Doctor\DoctorProfileController@getschedule')->name('getscheduledata');

    Route::get('india','HomeController@india')->name('india');

    Route::get('contact.html','HomeController@contacts')->name('contact');
    Route::get('others','HomeController@others')->name('others');

    Route::get('general-hospitals','HomeController@generalhospital')->name('generalhospitals');
    Route::get('dentalhospitals','HomeController@dentalhospital')->name('dentalhospitals');

    //Doctor search controller
    Route::post('sd','FrontEnd\SearchController@searchdoctor')->name('searchdoctor');
    Route::post('searchresult','FrontEnd\SearchController@searchresult')->name('searchresult');

    //Doctor appointment booking
    Route::post('/','Appointment\AppointmentController@bookAppointment')->name('bookappointment');

    //Doctor Search by their speciality
    Route::get('specialitysearch/{sp}','Doctor\DoctorSearchController@search')->name('specialitysearch');
    Route::get('alldoctors','Doctor\DoctorSearchController@alldoctors')->name('alldoctors');

    //get specific hospital
    Route::get('hospitalshow/{id}', 'FrontHospitalService@show')->name('gethospital');
    Route::post('searchhospital', 'FrontHospitalService@searchhospital')->name('searchhospital');
    Route::get('blog/category/{id}','FrontBlogController@blog_category_view');
});


Route::get('/blood', 'BloodDonorController@index');
Route::post('/blood/store', 'BloodDonoteController@store');
Route::get('/blood/', 'BloodDonoteController@index');


/*Blood Seaker*/
//Route::get('/blood/seakerindex', 'BloodSeakerController@index');
//Route::get('/blood/seakercreate', 'BloodSeakerController@create');
 Route::post('/blood/seakerstore', 'BloodSeakerController@store');


// Blood category filter
Route::get('/blood/{group}', 'BloodSearchController@index');

//Doctor schedule and chamber and new doctor add, remove
Route::prefix('admin')->group(function (){

    Route::get('321475687623adminlogin','Auth\AdminLoginController@showLoginForm')->name('adminlogin');
    Route::post('/','Auth\AdminLoginController@login')->name('adminloginsubmit');
    Route::get('adminlogout','Auth\AdminLoginController@logout')->name('adminlogout');

    //Add or remove support admin
    Route::get('addsupportadmin',['uses'=>'Admin\SupportAdminController@showform','as'=>'addsupportadmin','roles'=>['superadmin']]);
    Route::post('addnewsupportadmin',['uses'=>'Admin\SupportAdminController@addnewsupportadmin','as'=>'addnewsupportadmin','roles'=>['superadmin']]);
    Route::get('addrole/{id}',['uses'=>'Admin\SupportAdminController@addrole','as'=>'addrole','roles'=>['superadmin']]);
    Route::post('submitRole',['uses'=>'Admin\SupportAdminController@submitRole','as'=>'submitRole']);
    Route::get('manage',['uses'=>'Admin\SupportAdminController@manage','as'=>'manegeadmin']);
    Route::get('editadmininfo/{id}',['uses'=>'Admin\SupportAdminController@editadmininfo','as'=>'editadmininfo','roles'=>['superadmin']]);
    Route::get('deletedoctor/{id}',['uses'=>'Admin\SupportAdminController@editadmininfo','as'=>'editadmininfo','roles'=>['superadmin']]);
    Route::patch('updateadmininfo',['uses'=>'Admin\SupportAdminController@updateData','as'=>'updateadmininfo','roles'=>['superadmin']]);
    Route::get('deleteAdminData/{id}',['uses'=>'Admin\SupportAdminController@deleteAdminData','as'=>'deleteAdminData','roles'=>['superadmin']]);


    //Admin dashboard
    Route::get('/', ['uses'=>'Admin\AdminController@index','as'=>'adminindex','roles'=>['superadmin','supportadmin']]);
    //Doctor schedule add, update ,delete
    Route::get('/doctor-schedule/{id}', 'Admin\DoctorScheduleController@create')->name('doctorschedule');
    Route::post('/adddoctorschedule', 'Admin\DoctorScheduleController@store')->name('adddoctorschedule');
    Route::get('/viewdoctorschedule', 'Admin\DoctorScheduleController@viewallSchedule')->name('viewdoctorschedule');
    Route::get('/editschedule/{id}/{doctor_id}', 'Admin\DoctorScheduleController@editSchedule')->name('editschedule');
    Route::get('/deleteschedule/{id}', 'Admin\DoctorScheduleController@deleteschedule')->name('deleteschedule');
    Route::post('updateSchedule', 'Admin\DoctorScheduleController@updateSchedule')->name('updateSchedule');
    Route::get('addorremovechamber', 'Admin\DoctorController@addorremovechamber')->name('addorremovechamber');
    Route::post('postaddorremovechmaber', 'Admin\DoctorController@postaddorremovechmaber')->name('postaddorremovechmaber');


    //Doctor profile add, update delete
    Route::get('/doctorindex',['uses'=>'Admin\DoctorController@index','as'=>'doctorindex','roles'=>['superadmin','supportadmin']]);
    Route::get('/addnewdoctor',['uses'=>'Admin\DoctorController@create','as'=>'adddoctor','roles'=>['superadmin','supportadmin']]);
    Route::get('/show/{id}', 'Admin\DoctorController@show')->name('showdoctorinfo');

    Route::post('/addnewdocotrinfo/store', ['uses'=>'Admin\DoctorController@store', 'as'=>'adddoctorprofileinfo','roles'=>['superadmin','supportadmin']]);

    Route::get('/eachdoctor/edit/{id}',['uses'=>'Admin\DoctorController@edit','as'=>'editdoctorinfo','roles'=>['superadmin']]);
    Route::patch('/updateeachdoctor/{id}',['uses'=>'Admin\DoctorController@update','roles'=>['superadmin'],'as'=>'updatedoctorinfo']);
    Route::get('deletedoctor/{id}',['uses'=>'Admin\DoctorController@destroy','as'=>'deletedoctor','roles'=>['superadmin']]);


    //Foreign Doctor profile add, update delete
    Route::get('/foreigndoctorindex',['uses'=>'Admin\foreigndoctor@index','as'=>'foreigndoctorindex','roles'=>['superadmin','supportadmin']]);
    Route::get('/addnewforeigndoctor',['uses'=>'Admin\foreigndoctor@create','as'=>'addforeigndoctor','roles'=>['superadmin','supportadmin']]);

    Route::post('/store', 'Admin\foreigndoctor@store')->name('addnewforeigndoctor');
    Route::get('/edit/{id}',['uses'=>'Admin\foreigndoctor@edit','as'=>'editforeigndoctorinfo','roles'=>['superadmin']]);
    Route::patch('admin/update/{id}',['uses'=>'Admin\foreigndoctor@update','roles'=>['superadmin']])->name('updateforeigndoctorinfo');
    Route::get('deleteforeigndoctor/{id}',['uses'=>'Admin\foreigndoctor@destroy','as'=>'deleteforeigndoctor','roles'=>['superadmin']]);


//    Appointment functionality
    Route::get('allappointments',['uses'=>'Appointment\AppointmentController@getallAppointment','as'=>'allappointment','roles'=>['superadmin','supportadmin']]);
    Route::get('appointment-trash', ['uses'=>'Appointment\AppointmentController@appointmenttrash','as'=>'appointmenttrash','roles'=>['superadmin','supportadmin']]);
    Route::post('confirmAppointment', ['uses'=>'Appointment\AppointmentController@confirmAppointment','as'=>'confirmAppointment','roles'=>['superadmin','supportadmin']]);
    Route::post('deleteAppointment',  ['uses'=>'Appointment\AppointmentController@deleteAppointment','as'=>'deleteAppointment','roles'=>['superadmin','supportadmin']]);
    Route::post('permanentdelete', ['uses'=>'Appointment\AppointmentController@permanentdelete','as'=>'permanentdelete','roles'=>['superadmin']]);


    //Blog add, update delete
    Route::get('/blog', ['uses'=>'Admin\BlogController@index','as'=>'blogindex','roles'=>['superadmin']]);
    Route::get('/writeblog', ['uses'=>'Admin\BlogController@create','as'=>'createblog','roles'=>['superadmin']]);
    Route::get('showBlog/{id}', ['uses'=>'Admin\BlogController@show','as'=>'showBlog','roles'=>['superadmin']]);
    Route::get('editBlog/{id}', ['uses'=>'Admin\BlogController@edit','as'=>'editBlog','roles'=>['superadmin']]);
    Route::get('destroyBlog/{id}', ['uses'=>'Admin\BlogController@destroy','as'=>'destroyBlog','roles'=>['superadmin']]);
    Route::post('blogPost', 'Admin\BlogController@store')->name('blogPost');
    Route::patch('update/{id}',['uses'=>'Admin\BlogController@update','as'=>'updateblog','roles'=>['superadmin']]);


    /*Blog Comment Controller */
    Route::get('/admin/blog/comment',['uses'=>'CommentController@index','as'=>'allcomment','roles'=>['superadmin','supportadmin']]);
    Route::post('/admin/blog/comment/confirm/{id}', 'CommentController@edit');
    Route::post('/admin/blog/comment/delete/{id}', 'CommentController@destroy');



    /*Blood Donar*/
    Route::get('blood/index', ['uses'=>'CommentController@index','as'=>'index','roles'=>['superadmin','supportadmin']]);
    Route::get('blood/create',  ['uses'=>'Admin\BloodController@create','as'=>'createdonor','roles'=>['superadmin','supportadmin']]);
    Route::get('blood/show/{id}', ['uses'=>'Admin\BloodController@show','as'=>'show','roles'=>['superadmin','supportadmin']]);
    Route::get('blood/edit/{id}', ['uses'=>'Admin\BloodController@edit','as'=>'edit','roles'=>['superadmin','supportadmin']]);
    Route::patch('blood/update/{id}', 'Admin\BloodController@update');
    Route::get('blood/delete/{id}', ['uses'=>'Admin\BloodController@delete','as'=>'delete','roles'=>['superadmin','supportadmin']]);


    /*Blood Donar show  */
    Route::get('blood/requestdonorshow', ['uses'=>'Admin\RequestDonorShowController@index','as'=>'requestdonorshow','roles'=>['superadmin','supportadmin']]);
    Route::get('blood/pendingdonor', ['uses'=>'Admin\RequestDonorShowController@index','as'=>'pendingdonor','roles'=>['superadmin','supportadmin']]);
    Route::post('blood/pendingdonor/confirm/{id}', 'Admin\PendingDonorController@edit');
    Route::post('blood/pendingdonor/delete/{id}', 'Admin\PendingDonorController@destroy');


    /* Ambulance Controller */
    Route::get('ambulance/', ['uses'=>'Admin\AmbulanceController@index','as'=>'ambulanceindex','roles'=>['superadmin','supportadmin']]);
    Route::get('ambulancecreate', ['uses'=>'Admin\AmbulanceController@create','as'=>'create','roles'=>['superadmin','supportadmin']]);
    Route::get('ambulanceshow', ['uses'=>'Admin\AmbulanceController@show','as'=>'show','roles'=>['superadmin','supportadmin']]);


    //Diagnostic Center

    Route::get('diagnostic/diagnostic-center/', ['uses'=>'Admin\DiagnosticCenterController@index','as'=>'diagonosticsindex','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center/create', ['uses'=>'Admin\DiagnosticCenterController@create','as'=>'diagonosticscreate','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center/edit/{id}', ['uses'=>'Admin\DiagnosticCenterController@edit','as'=>'diagonosticsedit','roles'=>['superadmin','supportadmin']]);
    Route::post('/diagnostic/diagnostic-center/store', ['uses'=>'Admin\DiagnosticCenterController@store','as'=>'diagonosticsestore','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center/destroy/{id}', ['uses'=>'Admin\DiagnosticCenterController@destroy','as'=>'diagonosticsdestroy','roles'=>['superadmin']]);
    Route::patch('diagnostic/diagnostic-center/update/{id}',['uses'=>'Admin\DiagnosticCenterController@update','as'=>'diagonosticsupdate','roles'=>['superadmin']]);
    Route::get('diagnostic/diagnostic-center/show', ['uses'=>'Admin\DiagnosticCenterController@show','as'=>'diagonosticsshow','roles'=>['superadmin','supportadmin']]);

//Diagnostic center Tests

    Route::get('diagnostic/diagnostic-center-test/', ['uses'=>'Admin\DiagnosticCenterTestController@index','as'=>'diagonosticscenterindex','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center-test/create', ['uses'=>'Admin\DiagnosticCenterTestController@create','as'=>'diagonosticscentercreate','roles'=>['superadmin','supportadmin']]);
    Route::post('diagnostic/diagnostic-center-test/store', ['uses'=>'Admin\DiagnosticCenterTestController@store','as'=>'diagonosticscenterstore','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center-test/edit/{id}', ['uses'=>'Admin\DiagnosticCenterTestController@edit','as'=>'diagonosticscenteredit','roles'=>['superadmin','supportadmin']]);
    Route::patch('diagnostic/diagnostic-center-test/update/{id}', ['uses'=>'Admin\DiagnosticCenterTestController@update','as'=>'diagonosticscenterupdate','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center-test/destroy/{id}',  ['uses'=>'Admin\DiagnosticCenterTestController@destroy','as'=>'diagonosticscenterupdate','roles'=>['superadmin','supportadmin']]);
    Route::post('order/store',['uses'=>'Admin\Diagnostictestordercontroller@store','as'=>'teststore','roles'=>['superadmin','supportadmin']]);
    Route::get('testorder/',['uses'=>'Admin\Diagnostictestordercontroller@index','as'=>'testindex','roles'=>['superadmin','supportadmin']]);
    Route::get('testorder/delete/{id}',['uses'=>'Admin\Diagnostictestordercontroller@destroy','as'=>'testdestroy','roles'=>['superadmin','supportadmin']]);

    //Hospital service
    Route::get('hospital/create', ['uses'=>'Admin\HospitalController@create','as'=>'hospitalcreate','roles'=>['superadmin','supportadmin']]);
    Route::get('hospital/index/', ['uses'=>'Admin\HospitalController@index','as'=>'hospitalindex','roles'=>['superadmin','supportadmin']]);
    Route::post('/hospital/store', 'Admin\HospitalController@store');
    Route::get('hospital/edit/{id}', ['uses'=>'Admin\HospitalController@edit','as'=>'hospitaledit','roles'=>['superadmin','supportadmin']]);
    Route::patch('hospital/update/{id}', 'Admin\HospitalController@update');
    Route::get('hospital/destroy/{id}',  ['uses'=>'Admin\HospitalController@destroy','as'=>'hospitaldelete','roles'=>['superadmin','supportadmin']]);
//medical tourism service
    Route::get('tourism/create', ['uses'=>'Admin\medicaltourism@create','as'=>'tourismcreate','roles'=>['superadmin','supportadmin']]);
    Route::get('tourism/index/', ['uses'=>'Admin\medicaltourism@index','as'=>'tourismindex','roles'=>['superadmin','supportadmin']]);
    Route::post('/tourism/store', 'Admin\medicaltourism@store');
    Route::get('tourism/edit/{id}', ['uses'=>'Admin\medicaltourism@edit','as'=>'tourismedit','roles'=>['superadmin','supportadmin']]);
    Route::patch('tourism/update/{id}', 'Admin\medicaltourism@update');
    Route::get('tourism/destroy/{id}',  ['uses'=>'Admin\medicaltourism@destroy','as'=>'tourismdelete','roles'=>['superadmin','supportadmin']]);


    /* News Controller */
    Route::get('/news/', ['uses'=>'Admin\NewsController@index','as'=>'newsindex','roles'=>['superadmin','supportadmin']]);
    Route::get('/news/create', ['uses'=>'Admin\NewsController@create','as'=>'newscreate','roles'=>['superadmin','supportadmin']]);
    Route::post('/newsPost', 'Admin\NewsController@store')->name('newsPost');
    Route::get('showNews/{id}', ['uses'=>'Admin\NewsController@index','as'=>'showNews','roles'=>['superadmin','supportadmin']]);
    Route::get('editNews/{id}', ['uses'=>'Admin\NewsController@edit','as'=>'editNews','roles'=>['superadmin','supportadmin']]);
    Route::get('destroyNews/{id}',['uses'=>'Admin\NewsController@destroy','as'=>'destroynews','roles'=>['superadmin','supportadmin']]);
    Route::patch('/admin/news/update/{id}','Admin\NewsController@update');




    //Tests
    Route::get('diagnostic/tests/',['uses'=>'Admin\TestsController@index','as'=>'testindex','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/tests/create', ['uses'=>'Admin\TestsController@create','as'=>'testcreate','roles'=>['superadmin','supportadmin']]);
    Route::post('diagnostic/tests/store', 'Admin\TestsController@store');
    Route::get('diagnostic/tests/edit/{id}', ['uses'=>'Admin\TestsController@edit','as'=>'testedit','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/tests/destroy/{id}', ['uses'=>'Admin\TestsController@destroy','as'=>'testdelete','roles'=>['superadmin','supportadmin']]);
    Route::patch('diagnostic/tests/update/{id}', 'Admin\TestsController@update');
});

//Diagnostic front page
Route::get('diagnostics', 'FrontDiagnosticController@index');
Route::get('diagnosticshow/{id}', 'FrontDiagnosticController@show');
Route::get('singlediagnosticshow/{id}', 'FrontDiagnosticController@show');

//Hospital service Front page
Route::get('hospitals', 'FrontHospitalService@index');
Route::get('allindex', 'FrontHospitalService@allindex');
//Medical tourism Front page
Route::get('medicaltourism', 'Frontmedicaltourism@index');
Route::get('medicaltourismshow/{id}', 'Frontmedicaltourism@show');
Route::get('allmedicaltourism', 'Frontmedicaltourism@allindex');

// Hospital Front page
Route::get('allhospitaldoctor/{id}','FrontHospitalService@allhospitaldoctor');


Route::prefix('doctors')->group(function (){
    Route::get('/','Doctor\DoctorController@index')->name('doctordashboard');
    Route::get('doctorchamber','Doctor\DoctorController@doctorchamber')->name('doctorchamber');
    Route::get('doctorlogin','Auth\DoctorLoginController@showLoginForm')->name('doctorlogin');
    Route::get('doctorlogout','Auth\DoctorLoginController@logout')->name('doctorlogout');
    Route::post('doctorloginsubmit','Auth\DoctorLoginController@login')->name('doctorloginsubmit');
    Route::get('myappointments','Doctor\EachDoctorAppointmentsController@getAllAppointments')->name('getAllAppointments');
    Route::post('confirmAppointmentByDoctor','Doctor\EachDoctorAppointmentsController@confirmAppointmentByDoctor')->name('confirmAppointmentByDoctor');
    Route::post('deleteAppointmentByDoctor','Doctor\EachDoctorAppointmentsController@deleteAppointmentByDoctor')->name('deleteAppointmentByDoctor');

    Route::post('updateschedulebydoctor','Doctor\DoctorController@updateschedulebydoctor')->name('updateschedulebydoctor');
    Route::post('update','Doctor\DoctorController@update')->name('update');
    Route::get('write-blog','Doctor\EachDoctorBlogController@index')->name('writeblog');
    Route::post('post-blog','Doctor\EachDoctorBlogController@store')->name('blog-post');
    Route::get('myblogs','Doctor\EachDoctorBlogController@myblogs')->name('myblogs');
    Route::get('myblogs','Doctor\EachDoctorBlogController@myblogs')->name('myblogs');
    Route::get('/admin/diagnostic/testsdiscount/create','Doctor\EachDoctorBlogController@myblogs')->name('myblogs');

});

//diagnostic discount


Route::get('/admin/diagnostic/testsdiscount/create','Admin\DiscountController@create');
Route::get('/pdf/{id}','TestPdfController@getpdf');
Route::get('testpdf/','TestsPdfController@getpdf');





//admin diagnostic,hospital,doctor
Route::get('joindiagnosticadmin','JoinDiagnosticAdminController@index');


//doctor join login
Route::get('homelogout','HomeController@index')->name('homeindex');
Route::get('joindoctoradminlogin','JoinDoctorAdminController@joindoctoradminlogin');
Route::post('doctorjoinlogin','JoinDoctorAdminController@doctorjoinlogin');
//patient join login

Route::get('joindoctoradminlogin','JoinDoctorAdminController@joindoctoradminlogin');
Route::post('patientjoinlogin','JoinDoctorAdminController@doctorjoinlogin');
Route::post('patientjoinlogin','PatientController@index');

//hospital join login

Route::get('joindoctoradminlogin','JoinHospitalAdminController@joindoctoradminlogin');
Route::post('hospitaljoinlogins','JoinHospitalAdminController@hospitaljoinlogin');
//patients join login----------------------login

Route::get('joindoctoradminlogin','PatientController@joindoctoradminlogin');
Route::post('patientjoinlogins','PatientController@patientjoinlogins');
//diagnostic join login----------------------login

Route::get('joindoctoradminlogin','PatientController@joindoctoradminlogin');
Route::post('diagnosticjoinlogins','JoinDiagnosticAdminController@diagnosticjoinlogin');


//patient message-----

Route::post('verification','SendEmail@verification')->name('verification');
Route::post('verificationlogin','SendEmail@verificationlogin')->name('verificationlogin');

Route::get('patientallinformation/{id}','PatientController@patientallinformation')->name('patientallinformation');
Route::get('addpatientinformation/{id}','PatientController@addpatientinformation');

Route::get('addpatientinformationss/{id}','PatientController@addpatientinformationss')->name('addpatientinformationss');
Route::get('allpatientmessage','PatientController@allpatientmessage')->name('allpatientmessage');

//patient information-----

Route::post('/admin/patientinformation/store','PatientController@patientinformationstore')->name('patientinformationstore');


//home page----------
Route::get('homelogout','HomeController@index')->name('homelogout');


//test center

Route::get('diagnostic-admin/tests/','Admin\TestsController@testsindex');
Route::get('diagnostic-tests/tests/testscreate/{id}',  'Admin\TestsController@testscreate');

Route::post('diagnostic/tests/testsstore/{id}', 'Admin\TestsController@testsstore');
Route::get('diagnostic/tests/edit/{id}', ['uses'=>'Admin\TestsController@edit','as'=>'testedit','roles'=>['superadmin','supportadmin']]);
Route::get('diagnostic/tests/destroy/{id}', ['uses'=>'Admin\TestsController@destroy','as'=>'testdelete','roles'=>['superadmin','supportadmin']]);
Route::patch('diagnostic/tests/update/{id}', 'Admin\TestsController@update');



//Diagnostic center Tests

    Route::get('diagnostic/diagnostic-center-test/', ['uses'=>'Admin\DiagnosticCenterTestController@index','as'=>'diagonosticscenterindex','roles'=>['superadmin','supportadmin']]);
    Route::get('diagonosticscentertestcreate/{id}',  'Admin\DiagnosticCenterTestController@diagonosticscentertestcreate');
    Route::post('diagnostic/diagnostic-center-test/store', ['uses'=>'Admin\DiagnosticCenterTestController@store','as'=>'diagonosticscenterstore','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center-test/edit/{id}', ['uses'=>'Admin\DiagnosticCenterTestController@edit','as'=>'diagonosticscenteredit','roles'=>['superadmin','supportadmin']]);
    Route::patch('diagnostic/diagnostic-center-test/update/{id}', ['uses'=>'Admin\DiagnosticCenterTestController@update','as'=>'diagonosticscenterupdate','roles'=>['superadmin','supportadmin']]);
    Route::get('diagnostic/diagnostic-center-test/destroy/{id}',  ['uses'=>'Admin\DiagnosticCenterTestController@destroy','as'=>'diagonosticscenterupdate','roles'=>['superadmin','supportadmin']]);
    Route::post('order/store',['uses'=>'Admin\Diagnostictestordercontroller@store','as'=>'teststore','roles'=>['superadmin','supportadmin']]);
    Route::get('testorder/',['uses'=>'Admin\Diagnostictestordercontroller@index','as'=>'testindex','roles'=>['superadmin','supportadmin']]);
    Route::get('testorder/delete/{id}',['uses'=>'Admin\Diagnostictestordercontroller@destroy','as'=>'testdestroy','roles'=>['superadmin','supportadmin']]);

/** search and pdf */
Route::post('searchPDF','SearchPdfController@search')->name('searchPDF');
Route::post('searchPDFdoctor','SearchPdfController@searchPDFdoctor')->name('searchPDFdoctor');
Route::post('searchPDFmain','SearchPdfController@searchPDFmain')->name('searchPDFmain');
Route::get('/pdf/{id}','PdfController@getpdf');
